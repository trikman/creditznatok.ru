<?php
/*
Plugin Name: Вывод продуктов со статусом личное
Description: Вывод продуктов со статусом личное
Version: 0.1
Author: Systemo.biz
Author URI: http://systemo.biz/
 */

/**
 * Перехват WP_Query
 *
 * @param WP_Query $query
 *
 * @return WP_Query
 */
function view_private_posts_pre_get_posts($query)
{
    if ($query->query['post_type'] == 'offers' && $query->is_singular()) {
        // Изменение поста
        add_filter('posts_results', 'view_private_posts_posts_results', 10, 2);
    }

    return $query;
}

add_filter('pre_get_posts', 'view_private_posts_pre_get_posts'); // Перехват WP_Query

function view_private_posts_posts_results($posts)
{
    remove_filter('posts_results', 'view_private_posts_posts_results', 10, 2);

    if (empty($posts)) {
        return;
    }

    if ('private' == $posts[0]->post_status) {
        $posts[0]->post_status = 'publish';
    }

    return $posts;
}

