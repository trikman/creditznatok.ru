<?php
/**
 * Plugin Name: Creditznatok parser
 * Description: Creditznatok parser banks parser
 * Version: 1.0
 * Author: Profex team
 **/
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
define('CBP_PLUG_NAME', basename(dirname(__FILE__)));
define('CBP_DIR', WP_PLUGIN_DIR. DS. CBP_PLUG_NAME);
define('CBP_TPL_DIR', CBP_DIR. DS. 'tpl');
define('CBP_TEST_MODE', true);
define('CBP_PARENT_PAGE_ID', 1837);

//@set_time_limit( 0 );

class cbpFrame {
	private $_parser = null;
	private $_res = array();
	public function __construct() {
		add_action('admin_menu', array($this, 'initMenu'));
		add_action('wp_ajax_cbp_start_parse', array($this, 'startParse'));
		add_action('wp_ajax_cbp_parse', array($this, 'parse'));
	}
	public function initMenu() {
		add_menu_page(
			'Creditznatok parser', 
			'Creditznatok parser', 
			'manage_options', 
			'creditznatok-parser', 
			array($this, 'showAdminPage'));
	}
	public function showAdminPage() {
		require_once(CBP_TPL_DIR. DS. 'adminPage.php');
	}
	private function _checkNonce() {
		check_admin_referer('credoznat-none-secret', 'cr-nonce-f');
	}
	public function startParse() {
		$this->_checkNonce();
		$this->_initSession();
		$this->_initParser();
		$linksList = $this->_parser->getLinksList();
		if($linksList) {
			$this->_addRes('linksList', $linksList);
		} else
			$this->_addError( $this->_parser->getErrors() );
		$this->_finish();
	}
	public function parse() {
		$this->_checkNonce();
		$this->_initSession();
		$this->_initParser();
		if(!$this->_parser->parse($_POST['links'])) {
			$this->_addError( $this->_parser->getErrors() );
		}
		$this->_finish();
	}
	private function _initParser() {
		if(!$this->_parser) {
			require_once(CBP_DIR. DS. 'parser.php');
			$this->_parser = new cbpParser();
		}
	}
	private function _addError($error) {
		if(!isset($this->_res['errors'])) {
			$this->_res['errors'] = array();
		}
		if(is_array($error)) {
			$this->_res['errors'] = array_merge($this->_res['errors'], $error);
		} else {
			$this->_res['errors'][] = $error;
		}
	}
	private function _addRes($name, $val) {
		$this->_res[ $name ] = $val;
	}
	private function _finish() {
		exit( json_encode($this->_res) );
	}
	private function _initSession() {
		@set_time_limit( 0 ); // All operations bellow can take a large time
		session_start();
	}
}
$cbpFrame = new cbpFrame();

