<?php
	$nonce = wp_create_nonce('credoznat-none-secret');
?>
<script type="text/javascript">
	var cbpLinksList = []
	,	cbpLinksPerReq = 20
	,	cbrLastParseIter = 0;
	jQuery(document).ready(function(){
		jQuery('#parseForm').submit(function(){
			cbrLastParseIter = 0;
			clearParseLog();
			writeParseLog('Parsing items total links');
			jQuery.ajax({
				url: ajaxurl,
				data: {action: 'cbp_start_parse', 'cr-nonce-f': '<?php echo $nonce?>'},
				type: 'POST',
				dataType: 'json',
				success: function(res) {
					if(!res.errors) {
						cbpLinksList = res.linksList;
						writeParseLog('Found '+ cbpLinksList.length+ ' items for parse');
						parsePages();
					} else {
						writeParseLog( res.errors.join('<br />') );
					}
				}
			});
			return false;
		});
	});
	function writeParseLog(data) {
		jQuery('#parseLog').append( jQuery('<div class="parseLogRow" />').html(data) );
	}
	function clearParseLog() {
		jQuery('#parseLog').html('');
	}
	function parsePages() {
		var currParseArr = []
		,	currParseSize = cbrLastParseIter + cbpLinksPerReq;
		if(cbpLinksList.length + cbpLinksPerReq <= currParseSize) {
			writeParseLog('All items was parsed');
			return;
		}
		writeParseLog('Starting parse items from '+ cbrLastParseIter+ ' to '+ currParseSize);
		for(cbrLastParseIter; cbrLastParseIter < currParseSize; cbrLastParseIter++) {
			if(cbpLinksList[cbrLastParseIter])
				currParseArr.push( cbpLinksList[cbrLastParseIter] );
		}
		jQuery.ajax({
			url: ajaxurl,
			data: {action: 'cbp_parse', links: currParseArr, 'cr-nonce-f': '<?php echo $nonce?>'},
			type: 'POST',
			dataType: 'json',
			success: function(res) {
				if(!res.errors) {
					writeParseLog('Parsed another '+ currParseArr.length+ ' items');
					parsePages();
				} else {
					writeParseLog( res.errors.join('<br />') );
				}
			}
		});
	}
</script>
<form id="parseForm">
	
	<input type="submit" name="start" value="Start Parse!" class="button button-primary" />
	<div id="parseLog"></div>
</form>