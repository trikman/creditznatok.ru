<?php
class cbpParser {
	private $_baseUrl = '';
	private $_baseListUrl = '';
	private $_errors = array();
	private $_parentPageId = CBP_PARENT_PAGE_ID;
	private $_httpReq = null;
	private $_rekvizity = array();
	private $_about = array('active', 'licension', 'head_office', 'phones', 'offsite', 'online_bank');
	private $_lang = array();
	public function __construct() {
		$this->_baseUrl = 'http://www.sravni.ru';
		$this->_baseListUrl = $this->_baseUrl. '/banki/rating/';
	}
	public function getLinksList() {
		$html = $this->_getRequest( $this->_baseListUrl );
		if(!empty($html)) {
			$this->_connectQuery();
			$doc = phpQuery::newDocument($html);
			$paginationLinks = $this->_extractPagerLinks( $doc );
			$links = $this->_extractLinkFromDoc( $doc );
			foreach($paginationLinks as $pgLink) {
				$html = $this->_getRequest( $this->_baseUrl. $pgLink );
				if($html) {
					$doc = phpQuery::newDocument($html);
					$newLinks = $this->_extractLinkFromDoc( $doc );
					if(!empty($newLinks) && is_array($newLinks)) {
						$links = array_merge($links, $newLinks);
					}
				}
			}
			return $links;
		}
		return false;
	}
	private function _extractSlug($link) {
		$slugArr = array_map('trim', explode('/', $link));
		$slug = $slugArr[ count($slugArr) - 1 ];
		if(empty($slug))
			$slug = $slugArr[ count($slugArr) - 2 ];
		return $slug;
	}
	public function parse($linksList) {
		$this->_connectQuery();
		if( !class_exists( 'WP_Http' ) )
			include_once( ABSPATH . WPINC. '/class-http.php' );
		$this->_httpReq = new WP_Http();
		$this->_rekvizity = parse_ini_file(CBP_DIR. DS. 'rekvisity.ini');
		$this->_lang = parse_ini_file(CBP_DIR. DS. 'lang.ini');
		foreach($linksList as $link) {
			$link = trim($link);
			$fullLink = $this->_baseUrl. $link;
			$html = $this->_getRequest( $fullLink );
			if($html) {
				$doc = phpQuery::newDocument($html);
				$title = trim($doc->find('.banks-card__title__value')->html());
				$desc = trim($doc->find('.banks-card__title__description')->html());
				$slug = $this->_extractSlug($link);
				$pageId = $this->_addBaseBankPage($title, $desc, $link, $slug);
				if($pageId) {
					$featuredImgId = $this->_addFeaturedImg($pageId, $doc);
					if(!$featuredImgId && CBP_TEST_MODE) {
						return false;
					}
					$pageMeta = array();
					// Page meta adding
					//$doc->find('.ratings__item:first')->find('.ratings__item__title, a, br')->remove()->html();
					//$pageMeta['active'] = trim($doc->find('.ratings__item:first')->html());
					$pageMeta['active'] = isset($_SESSION['activi_bankov'][ $link ]) ? $_SESSION['activi_bankov'][ $link ] : 0;
					$cardInfoBlocks = $doc->find('.banks-card__info__item');
					$k = 0;
					foreach($cardInfoBlocks as $infoBlock) {
						$infoText = pq(pq($infoBlock)->find('.banks-card__info__item__text'));
						$value = trim($infoText->html());
						switch($k) {
							case 0: 
								$infoText->find('a')->remove();
								$value = trim(str_replace('|', '', $infoText->html()));
								$pageMeta['licension'] = $value;
								break;
							case 1: $pageMeta['head_office'] = $value; break;
							case 2: $pageMeta['phones'] = $value; break;
							case 3: 
								$value = trim(pq($infoText->find('a:first'))->html());
								$pageMeta['offsite'] = $value;
								break;
							case 4:
								$fullRedirectUrl = trim(pq($infoText->find('a:first'))->attr('href'));
								$urlData = parse_url($fullRedirectUrl);
								if(!empty($urlData['query'])) {
									$redirectParams = array();
									parse_str($urlData['query'], $redirectParams);
									if(!empty($redirectParams) && !empty($redirectParams['out'])) {
										$pageMeta['online_bank'] = trim($redirectParams['out']);
									}
								}
								break;
						}
						$k++;
					}
					$rekvizity = $this->_parseRekvizity($fullLink);
					if($rekvizity && is_array($rekvizity)) {
						$pageMeta = array_merge($pageMeta, $rekvizity);
					}
					$this->_addAllMeta($pageId, $pageMeta);
					$this->_setPageTpl($pageId, 'template-bank.php');
					
					// Add reviews page
					$reviewsId = $this->_addBaseBankPage($this->_lang['REVIEWS']. ' '. $title, '', $link. '-reviews', $slug. '-reviews', $pageId);
					if($reviewsId) {
						$this->_setPageTpl($reviewsId, 'template-reviews.php');
					}
					// Add rekvizity page
					$rekvizityId = $this->_addBaseBankPage($this->_lang['REKVIZITY']. ' '. $title, '', $link. '-rekvizity', $slug. '-rekvizity', $pageId);
					if($rekvizityId) {
						$this->_setPageTpl($rekvizityId, 'template-rekvizit.php');
					}
				} else {
					return false;
				}
			} else
				return false;
		}
		return true;
	}
	private function _setPageTpl($pageId, $tpl) {
		update_post_meta($pageId, '_wp_page_template', $tpl);
	}
	private function _addAllMeta($pageId, $pageMeta) {
		$allMetaKeys = array();
		// Didn't used array_merge() - just because _rekvizity is associative, and result will be 50/50 - associative and no, not really preaty
		foreach($this->_about as $key) $allMetaKeys[] = $key;
		foreach($this->_rekvizity as $key) $allMetaKeys[] = $key;
		foreach($allMetaKeys as $key) {
			if( empty($pageMeta[ $key ]) ) {
				delete_post_meta($pageId, $key);
			} else {
				if(in_array($key, array('iban'))) {
					$pageMeta[ $key ] = trim(preg_replace('/\s+/', ' ', $pageMeta[ $key ]));
				}
				update_post_meta($pageId, $key, $pageMeta[ $key ]); 
			}
		}
	}
	private function _parseRekvizity($fullLink) {
		$res = array();
		$html = $this->_getRequest( $fullLink. 'rekvizity/' );
		if(!empty($html)) {
			$doc = phpQuery::newDocument($html);
			$dataRows = pq(pq($doc->find('.content-block.m-bg.m-pad'))->find('table.text-table:first'))->find('tr');
			foreach($dataRows as $row) {
				$rowObj = pq($row);
				$rowLabel = trim($rowObj->find('th:first')->html());
				if(isset($this->_rekvizity[ $rowLabel ]) && !empty($this->_rekvizity[ $rowLabel ])) {
					$res[ $this->_rekvizity[ $rowLabel ] ] = trim($rowObj->find('td:first')->html());
				}
			}
		}
		return $res;
	}
	private function _addFeaturedImg($pageId, $doc) {
		$existAttachId = (int) get_post_thumbnail_id( $pageId );
		if($existAttachId)	// Image already exists - will not create new one
			return true;
		$imgSrc = trim($doc->find('.banks-card__logo__bank')->attr('src'));
		if(!empty($imgSrc)) {
			$imgSrcFull = $this->_baseUrl. $imgSrc;
			$img = $this->_httpReq->request( $imgSrcFull, array('timeout' => 25) );
			if(is_wp_error($img) && CBP_TEST_MODE) {
				$this->_addError('Can not upload image: ['. $img->get_error_message(). '] ['. $imgSrcFull. ']');
				return false;
			} elseif($img && isset($img['body']) && !empty($img['body'])) {
				$imgName = $img['headers']['content-disposition'];
				if(!empty($imgName)) {
					$imgName = explode(';', $imgName);
					if(isset($imgName[1]) && !empty($imgName[1])) {
						$imgName = explode('=', $imgName[1]);
						if(isset($imgName[1]) && !empty($imgName[1])) {
							$imgName = str_replace('"', '', $imgName[1]);
						}
					}
				}
				if(empty($imgName))
					$imgName = $pageId. '.jpg';
				if(!isset($img['headers']['last-modified']) || empty($img['headers']['last-modified'])) {
					$img['headers']['last-modified'] = date('d-m-Y H:i:s');
				}
				$uploaded = wp_upload_bits( $imgName, null, $img['body'], date('Y-m', strtotime( $img['headers']['last-modified'] ) ) );
				if($uploaded && $uploaded['url']) {
					$file = $uploaded['file'];
					$filename   = basename($uploaded['url']);
					// Check image file type
					$wp_filetype = wp_check_filetype( $filename, null );
					// Set attachment data
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title'     => sanitize_file_name( $filename ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);
					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file, $pageId );
					// Include image.php
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					// Define attachment metadata
					$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
					// Assign metadata to attachment
					wp_update_attachment_metadata( $attach_id, $attach_data );
					// And finally assign featured image to post
					set_post_thumbnail( $pageId, $attach_id );
					return $attach_id;
				} else
					$this->_addError('Can not do wp_upload_bits');
			}
		}
		// Just no image I guess
		return true;
	}
	private function _addBaseBankPage($title, $desc = '', $link = '', $slug = '', $parentId = 0) {
		$parentId = $parentId ? $parentId : $this->_parentPageId;
		global $wpdb;		
		// Check post by meta key
		$pageId = (int)$wpdb->get_var("select post_id from $wpdb->postmeta where meta_key = 'original_link' AND meta_value = '$link'", ARRAY_A );
		if($pageId)
			return $pageId;
		// Check by title
		$mypostids = $wpdb->get_col("select ID from $wpdb->posts where post_title = '". $title. "' AND post_parent = $parentId AND post_type = 'page' AND post_status = 'publish'");
		if(!empty($mypostids))
			return $mypostids[0];

		/*if($page && is_object($page) && $page->ID)
			return $page->ID;*/
		// Create page in other case
		//var_dump($slug);
		$pageId = wp_insert_post(array(
			'post_name' => $slug,
			'post_title' => $title,
			'post_content' => $desc,
			'post_status' => 'publish',
			'post_type' => 'page',
			'post_parent' => $parentId,
		));
		if($pageId) {
			add_post_meta($pageId, 'original_link', $link);
			return $pageId;
		} else {
			$this->_addError(is_wp_error($pageId) ? $pageId->get_error_message() : 'Can not create page with title ['. $title. '], url ['. $link. ']');
			return false;
		}
	}
	private function _extractLinkFromDoc($doc) {
		if(!isset($_SESSION['activi_bankov']))
			$_SESSION['activi_bankov'] = array();
		$links = array();
		$blocks = $doc->find('#allBanksRating .results__item');
		foreach ($blocks as $block) {
			$blockObj = pq($block);
			$link = trim($blockObj->find('.results__item__title a:first')->attr('href'));
			$links[] = $link;
			$activi = $blockObj->find('.results__item__rating__value')->html();
			$activi = (int) preg_replace('/[^0-9]/', '', $activi);
			$_SESSION['activi_bankov'][ $link ] = $activi;
		}
		return $links;
	}
	private function _extractPagerLinks($doc) {
		$links = array();
		$blocks = $doc->find('.pager__item:not(.m-slide)');
		foreach ($blocks as $block) {
			$link = trim(pq($block)->attr('href'));
			if(!empty($link))
				$links[] = $link;
		}
		return $links;
	}
	public function getErrors() {
		return $this->_errors;
	}
	private function _addError($error) {
		$this->_errors = $error;
	}
	private function _connectQuery() {
		require_once(CBP_DIR. DS. 'phpquery'. DS. 'phpQuery.php');
	}
	private function _getRequest($url) {
		if(CBP_TEST_MODE && isset($_SESSION['cbp_html_'. md5($url) ]))
			$html = $_SESSION['cbp_html_'. md5($url) ];
		else {
			$html = file_get_contents($url);
			if(empty($html)) {
				$res = wp_remote_get($url, array(
					'timeout'     => 50,
				));
				if(!empty($res) && !is_wp_error($res) && isset($res['body']) && !empty($res['body'])) {
					$html = $res['body'];
				}
			}
			if(empty($html)) {
				$error = '';
				if(empty($res)) {
					$error = 'Empty responce';
				} elseif(is_wp_error($res)) {
					$error = $res->get_error_message();
				} elseif(!isset($res['body']) || empty($res['body'])) {
					$error = 'Empty body in responce';
				} else {
					$error = 'Undefined server request error';
				}
				$this->_addError($error. ' ['. $url. ']');
				return false;
			}
			if(CBP_TEST_MODE) {
				$_SESSION['cbp_html_'. md5($url) ] = $html;
			}
		}
		return $html;
		
	}
}