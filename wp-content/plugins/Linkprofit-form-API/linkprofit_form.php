<?php
/**
 * Plugin Name:       Linkprofit-form-API
 * Plugin URI:        http://creditznatok.ru/wp-admin/options-general.php?page=linkprofit-admin-options
 * Description:       Форма заявки на займ. Данные заявки передаються по API в партнерскую сеть Linkprofit и также сохраняться в БД сайта.
 * Version:           1.0
 * Author:            Мельник Артём
 * Author URI:        http://qwib.ru/
 */

add_action('admin_menu', 'linkprofit_form');
function linkprofit_form() {
	add_options_page('Партнерская сеть Linkprofit', 'Партнерская сеть Linkprofit', 'manage_options', 'linkprofit-admin-options', 'linkprofit_admin_options_page');
}

function linkprofit_form_api_function() {
	$list_region = file_get_contents(plugins_url().'/Linkprofit-form-API/country_list_option.php');
	$list_day = file_get_contents(plugins_url().'/Linkprofit-form-API/list_day.php');
	$list_m = file_get_contents(plugins_url().'/Linkprofit-form-API/list_m.php');
	$list_year = file_get_contents(plugins_url().'/Linkprofit-form-API/list_year.php');
	
	$randCaptcha = rand(2000, 9000);

	$form_api = '';
	$form_api .= '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/Linkprofit-form-API/cust.css" />';
	$form_api .= '<script src="'.plugins_url().'/Linkprofit-form-API/jquery-2.1.3.min.js"></script>';
	$form_api .= '<script src="'.plugins_url().'/Linkprofit-form-API/jquery.maskedinput.min.js"></script>';
	$form_api .= '<script src="'.plugins_url().'/Linkprofit-form-API/cust.js"></script>';
	
	$form_api .= '<h3>Форма заявки на займ</h3>';
	$form_api .= '<form class="request_api_form" id="request_api_form">';
		$form_api .= '<input type="hidden" name="wmid" value="3332" />';
		$form_api .= '<input type="hidden" name="chan" value="creditznatok.ru" />';
		$form_api .= '<div class="step_1">';
		$form_api .= '<h3>1 шаг</h3>';
			$form_api .= '<p>Имя<br><input type="text" name="fname" class="input1" required /></p>';
			$form_api .= '<p>Фамилия<br><input type="text" name="lname" class="input1" required /></p>';
			$form_api .= '<p>Отчество<br><input type="text" name="mname" class="input1" required /></p>';
			$form_api .= '<p>Регион<br><select name="region" class="input1">'.$list_region.'</select></p>';
			$form_api .= '<p>Город проживания<br><input name="city" class="input1" required /></p>';
			$form_api .= '<p>Сумма займа(Кратно 1000 руб.)<br><input type="number" name="sum" class="input1" required step="1000" /></p>';
			$form_api .= '<p>Телефон<br><input type="text" name="phone" id="phone" class="input1" required /></p>';
			$form_api .= '<p>Email<br><input type="email" name="email" class="input1" required /></p>';
			$form_api .= '<a href="#" class="next_step next_step1" onclick="next_step1(); return false;">Далее</a>';
		$form_api .= '</div>';
		
		$form_api .= '<div class="step_2">';
		$form_api .= '<h3>2 шаг</h3>';
			$form_api .= '<p>Срок займа<br><input type="number" name="time" class="input2" step="1" /></p>';
			$form_api .= '<p>Дата рождения<br><select name="day" class="input2 day">'.$list_day.'</select><select name="mouth" class="input2 mouth">'.$list_m.'</select><select name="year" class="input2 year">'.$list_year.'</select></p>';
			
			$form_api .= '<p>Пол<br>'
				. '<select name="gender" class="input2">'
				. '<option value="0">Не выбран</option>'
				. '<option>Мужской</option>'
				. '<option>Женский</option>'
				. '</select>'
				. '</p>';
			$form_api .= '<a href="#" class="next_step next_step2" onclick="next_step2(); return false;">Далее</a>';
		$form_api .= '</div>';
		
		$form_api .= '<div class="step_3">';
		$form_api .= '<h3>3 шаг – паспортные данные</h3>';
			$form_api .= '<p>Адрес регистрации'
				. '<ul>'
				. '<li>Регион<br><select name="region2" class="input3">'.$list_region.'</select></li>'
				. '<li>Тип населенного пункта<br><input type="text" name="type_p" class="input3" /></li>'
				. '<li>Название населенного пункта<br><input type="text" name="city2" class="input3" /></li>'
				. '<li>Улица<br><input type="text" name="street" class="input3" /></li>'
				. '<li>Номер дома<br><input type="number" name="house" class="input3" /></li>'
				. '<li>Корпус/строение<br><input type="text" name="building" /></li>'
				. '<li>Номер квартиры<br><input type="text" name="apartment" class="input3" /></li>'
				. '</ul></p>';
			$form_api .= '<p>Серия паспорта<br><input type="text" name="pass_srs" id="pass_srs" class="input3" /></p>';
			$form_api .= '<p>Номер паспорта<br><input type="text" name="pass_num" id="pass_num" class="input3" /></p>';
			$form_api .= '<p>Дата выдачи паспорта<br><input type="text" name="pass_date" id="pass_date" class="input3" /></p>';
			$form_api .= '<p>Кем выдан паспорт<br><input type="text" name="pass_plc" id="pass_plc" class="input3" /></p>';
			$form_api .= '<a href="#" class="next_step next_step3" onclick="next_step3(); return false;">Далее</a>';
		$form_api .= '</div>';
		
		$form_api .= '<div class="step_4">';
		$form_api .= '<h3>3 шаг – дополнительная информация</h3>';
			$form_api .= '<div class="check_in">Совпадает с адресом регистрации <input type="checkbox" class="checkbadr checkb" name="checkIn" /></div>';
			$form_api .= '<div class="adrr_in">';
				$form_api .= '<p>Адрес проживания'
				. '<ul>'
				. '<li>Регион<br><select name="region3" class="input4">'.$list_region.'</select></li>'
				. '<li>Тип населенного пункта<br><input type="text" name="type_p3" class="input4" /></li>'
				. '<li>Название населенного пункта<br><input type="text" name="city3" class="input4" /></li>'
				. '<li>Улица<br><input type="text" name="street3" class="input4" /></li>'
				. '<li>Номер дома<br><input type="number" name="house3" class="input4" /></li>'
				. '<li>Корпус/строение<br><input type="text" name="building3" class="input4" /></li>'
				. '<li>Номер квартиры<br><input type="text" name="apartment3" class="input4" /></li>'
				. '</ul></p>';
			$form_api .= '</div>';
			$form_api .= '<div class="check_in_work">Не работаю <input type="checkbox" class="checkb checkbwor" name="checkInWork" /></div>';
			$form_api .= '<div class="work_in">';
				$form_api .= '<p>Наименование организации<br><input type="text" name="work_plc" id="work_plc" class="input4" /></p>';
				$form_api .= '<p>Заработная плата<br><input type="text" name="salary" id="salary" class="input4" /></p>';
			$form_api .= '</div>';
			$form_api .= '<div><input type="text" name="captcha" id="captcha" value="'.$randCaptcha.'" disabled /><input type="text" id="captchaval" value="" /><span class="er caper"></span></div>';
			$form_api .= '<a href="#" class="next_step submit_click" onclick="submit_click(); return false;">Готово</a>';
		$form_api .= '</div>';
	
	$form_api .= '</form>';
	
	echo $form_api;
}
add_shortcode( 'linkprofit_form_api_view', 'linkprofit_form_api_function' );

function linkprofit_admin_options_page(){
	$html = '';
	$html .= '<h1>Партнерская сеть Linkprofit</h1>';
	$html .= '<h3>Список заявок</h3>';
	$html .= do_shortcode('[linkprofit_form_api_view]');
	echo $html;
}
