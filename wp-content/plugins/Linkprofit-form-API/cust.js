jQuery(function($){
   $("#phone").mask("8(999) 999-9999");
   $("#pass_srs").mask("9999");
   $("#pass_num").mask("999999");
   $("#pass_date").mask("9999-99-99");
   
   $('.checkbwor').on('click', function(){
       $('.work_in').toggle();
   });
   
   $('.checkbadr').on('click', function(){
       $('.adrr_in').toggle();
   });
});

function next_step1(){
    var true_x = [];
    $(".input1").each(function (i) {
        var val = $(this).val();
        if(val == '' || val.length < 2 || val == '0'){
            $(this).parent().find('.er').remove();
            $(this).parent().append('<span class="er">Поле заполнено не коректно.</span>');
            true_x.push('false');
        }else{
            $(this).parent().find('.er').remove();
            true_x.push('true');
        }
    });
    
    var check = true_x.indexOf('false');
        if(check == -1){
            $('.step_1').hide(); 
            $('.step_2').fadeIn(1000);  
        }else{
            return false;
        }
}

function next_step2(){
    var true_x = [];
    $(".input2").each(function (i) {
        var val = $(this).val();
        if(val == '' || val.length < 1 || val == '0'){
            $(this).parent().find('.er').remove();
            $(this).parent().append('<span class="er">Поле заполнено не коректно.</span>');
            true_x.push('false');
        }else{
            $(this).parent().find('.er').remove();
            true_x.push('true');
        }
    });
    console.log(true_x);
    var check = true_x.indexOf('false');
        if(check == -1){
            $('.step_2').hide(); 
            $('.step_3').fadeIn(1000);  
        }else{
            return false;
        }
}

function next_step3(){
    var true_x = [];
    $(".input3").each(function (i) {
        var val = $(this).val();
        if(val == '' || val.length < 1 || val == '0'){
            $(this).parent().find('.er').remove();
            $(this).parent().append('<span class="er">Поле заполнено не коректно.</span>');
            true_x.push('false');
        }else{
            $(this).parent().find('.er').remove();
            true_x.push('true');
        }
    });
    
    var check = true_x.indexOf('false');
        if(check == -1){
            $('.step_3').hide(); 
            $('.step_4').fadeIn(1000);  
        }else{
            return false;
        }
}

function submit_click(){
     var true_x = [];
     var checkbadr = $(".checkbadr").prop("checked");
     var checkbwor = $(".checkbwor").prop("checked");
     
     if(!checkbadr){
        $(".adrr_in .input4").each(function (i) {
            var val = $(this).val();
            if(val == '' || val.length < 1 || val == '0'){
                $(this).parent().find('.er').remove();
                $(this).parent().append('<span class="er">Поле заполнено не коректно.</span>');
                true_x.push('false');
            }else{
                $(this).parent().find('.er').remove();
                true_x.push('true');
            }
        }); 
     }
     
     if(!checkbwor){
        $(".work_in .input4").each(function (i) {
            var val = $(this).val();
            if(val == '' || val.length < 1 || val == '0'){
                $(this).parent().find('.er').remove();
                $(this).parent().append('<span class="er">Поле заполнено не коректно.</span>');
                true_x.push('false');
            }else{
                $(this).parent().find('.er').remove();
                true_x.push('true');
            }
        }); 
     }
     
     if($('#captcha').val() !== $('#captchaval').val()){
         $('.caper').html('<span class="er">Неверный ответ</span>');
     }else{
         $('.caper').empty();
     }
    
    var check = true_x.indexOf('false');
        if(check == -1 && $('#captcha').val() == $('#captchaval').val()){
            var data_form = $('#request_api_form').serialize();
            
            $.ajax({
                type: 'POST',
                url: 'http://gtw.linkprofit.ru/cgi/send',
                data: data_form,
                success: function(html) {
                    console.log(html);
                }
            }); 
        }else{
            return false;
        }
}