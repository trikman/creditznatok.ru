��             +         �  '   �     �       9        K     P     `      f  )   �  x   �     *     E  1   R     �     �     �     �     �     �     �     �     �          #  !   2     T  �   b  =   �     0     5     >  �  K  a   �  !   <     ^  n   z     �  !   �     	  8   %	  Y   ^	  �   �	  +   �
  +   �
  S   �
     :  %   Q  0   w  <   �  *   �               6      L  0   m  "   �  9   �      �  "    q   ?  
   �      �     �                                                              	         
                                                                         A scheduled update has been executed at Change categories Change status Check the post types you wish to display the Scheduler on Date Delete (forced) Draft Enable email notification option Enable extra column on posttype edit page Enabling this option makes it possible to send an email notification to the post author on a scheduled change execution. Enter your settings below: Extra column Mark allowed meta fields to be shown as removable Notification Pending Post Status Scheduler Post Status Scheduler update Post Types and meta keys Private Public Regards Remove postmeta Schedule Status Change Scheduled date Send email notification on change Set status to Settings for adding extra column "Scheduled date" on edit page. This column will only be displayed on posttypes that are allowed for scheduling The post will have the following categories on scheduled time Time Trashbin on your post Project-Id-Version: post-status-scheduler
POT-Creation-Date: 2015-09-20 22:13+0300
PO-Revision-Date: 2015-09-30 21:56+0300
Last-Translator: Igor V Belousov <igor@belousovv.ru>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
 Запланированное обновление статуса было выполнено в Сменить категории Сменить статус Отметьте типы постов в которых вы хотите видеть планировщик Дата Удалено (навсегда) Черновик Включить опцию email-уведомлений Включить дополнительную колонку в списке постов Включение этой опции позволяет отправить уведомление по email автору поста после запланированного изменения статуса. Введите ваши настройки: Дополнительная колонка Отметьте мето-поля показываемые для удаления Уведомления Ожидает утверждения Планировщик смены статуса Планировщик обновления статусов Типы постов и мето-поля Личное Опубликовано С уважением Удалить мето-поле Планировщик смены статуса Дата смены статуса Отправить email при смене статуса Сменить статус на Настройки для добавления дополнительного столбца "Дата смены статуса" в списке постов. Этот столбец будет отображаться только в типах постов отмеченных выше. В запланированное время пост будет иметь следующие категории Время Удалено в корзину в посте 