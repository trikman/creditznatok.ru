jQuery( document ).ready( function( $ ) {

  $('#schedulerdate').datepicker({

    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    firstDay: 1,
    monthNames: ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрьr","Октябрь","Ноябрь","Декабрь"],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'], // For formatting
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'], // For formatting
    dayNamesShort: ['Вск', 'Пон', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'], // For formatting
    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], // Column headings for days starting at Sunday
    prevText: '<Пред',
    nextText: 'След>',

  });
  //$( ".selector" ).datepicker({ });
  $('#schedulertime').timepicker();

  $('#scheduler-use').change( function() {

    if( $(this).is(':checked') ) {
       
      $('#scheduler-settings').slideDown();

    } else {

      $('#scheduler-settings').slideUp();

    }

  });

  $('#scheduler-status').change( function() {
    if( $(this).is(':checked') ) {
       
       $('#scheduler-status-box').slideDown();

    } else {

       $('#scheduler-status-box').slideUp();

    }

    toggle_email_notification();
  });

  $('#scheduler-category').change( function() {
    if( $(this).is(':checked') ) {
       
       $('#scheduler-category-box').slideDown();

    } else {

       $('#scheduler-category-box').slideUp();

    }

    toggle_email_notification();
  });

  $('#scheduler-postmeta').change( function() {
    if( $(this).is(':checked') ) {
       
       $('#scheduler-postmeta-box').slideDown();

    } else {

       $('#scheduler-postmeta-box').slideUp();

    }

    toggle_email_notification();
  });

  toggle_email_notification();

  function toggle_email_notification() {

    if( $('#scheduler-status').is(':checked') || $('#scheduler-category').is(':checked') || $('#scheduler-postmeta').is(':checked') ) {

      $('#scheduler-email-notification').removeAttr('disabled');

    } else {

      /*$('#scheduler-email-notification').attr('disabled', 'disabled');
      $('#scheduler-email-notification').removeAttr('checked', 'checked');*/

    }

  }

});