<?php
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

/**
 * Single Posts Template
 *
 * Template name: Страница списка банков
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<?php get_template_part('loop-header'); ?>
    <div id="banks-list">
        <div class="row">
            <div class="col-md-3">
                <div class="cz-block-white">
                    <ul class="cz-ul">
                        <?php foreach (CreditznatokConfig::$bankRatingsData as $slug => $data) : ?>
                            <li>
                                <a href="/banki/<?php echo $slug; ?>/" class="border-bottom" title="<?php echo $data['title']; ?>"><?php echo $data['label']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cz-block-white">
                    <div class="rating-bank-title">Каталог банков</div>
                    <?php
                    $paged   = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $perPage = 10;
                    $query   = new WP_Query('post_type=banks&posts_per_page=' . $perPage . '&meta_key=reyting-aktivy&orderby=meta_value_num&order=DESC&paged=' . $paged);

                    while ($query->have_posts()) {
                        $query->the_post(); ?>
                        <?php $post_id      = get_the_ID();
                        $bank_sorted_fields = Creditznatok::get_bank_sorted_fields($post_id);
                        $post_number        = $perPage * ($paged - 1) + $query->current_post + 1; ?>
                        <div class="banks-wrapper">
                            <div class="row v-center-all">
                                <div class="col-xs-4 border">
                                    <a href="<?php the_permalink(); ?>" class="cz-bank-link">
                                        <?php echo get_the_post_thumbnail($post_id, 'large',
                                            ['alt' => get_the_title(), 'width' => '20px']); ?>
                                    </a>
                                </div>
                                <div class="col-xs-8 bank-fields-col">
                                    <div class="bank-fields">
                                        <a class="bank-list-title-link border-bottom" href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </div>
                                    <?php foreach ($bank_sorted_fields as $name => $value) { ?>
                                        <div class="bank-fields">
                                            <span class="bold"><?php echo CreditznatokConfig::$bankRatingMapping[$name]; ?></span>
                                            <?php echo Creditznatok::format_number($value); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                    <div class="pagination">
                        <?php
                        $paged = $query->query['paged'] ? $query->query['paged'] : 1;
                        $min   = (($paged - 1) * 10) + 1;
                        $max   = $paged * 10 <= $query->found_posts ? $paged * 10 : $query->found_posts; ?>
                        <div class="pagination-info">Показаны банки
                            <span class="bold"><?php echo $min; ?>-<?php echo $max; ?> из <?php echo $query->found_posts; ?></span>
                        </div>
                        <?php
                        $args = [
                            'base'      => get_site_url() . '/banki/%_%',
                            'format'    => 'page/%#%/',
                            'total'     => $query->max_num_pages,
                            'current'   => $paged,
                            'end_size'  => 2,
                            'mid_size'  => 2,
                            'prev_next' => false,
                            'type'      => 'plain',
                        ];
                        echo paginate_links($args); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="cz-banner">
                    <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>