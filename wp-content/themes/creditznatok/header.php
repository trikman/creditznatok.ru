<?php
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
$language_attributes = get_language_attributes();
?>
<!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php echo $language_attributes; ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php echo $language_attributes; ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php echo $language_attributes; ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php echo $language_attributes; ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php echo $language_attributes; ?>> <!--<![endif]-->
<head>
    <?php echo CreditznatokConfig::GoogleTagManagerHead; ?>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo Creditznatok::get_title(); ?></title>
    <?php echo Creditznatok::get_meta_description(); ?>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php echo CreditznatokConfig::GoogleTagManagerBody; ?>
<div class="container">
    <?php Creditznatok::get_title(); ?>
    <div class="header">
        <div class="row v-center">
            <div class="col-xs-9 col-md-3">
                <div class="row">
                    <div class="col-md-2 hidden-sm hidden-xs">
                    </div>
                    <div class="col-md-10 col-xs-12 logo-link-wrapper">
                        <div class="cz-mobile-nav-button" data-toggle="collapse" data-target="#navbar-collapse"></div>
                        <a class="logo-link" href="<?php echo home_url('/'); ?>">
                            <img class="desktop-logo" src="<?php echo get_template_directory_uri() . '/includes/css/new-style/images/logo-horizontal.png'; ?>" alt="<?php bloginfo('name'); ?>" />
                            <img class="mobile-logo" src="<?php echo get_template_directory_uri() . '/includes/css/new-style/images/mobile-logo.png'; ?>" alt="<?php bloginfo('name'); ?>" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs header-text">
                <div class="row">
                    <div class="col-xs-1">
                        <div class="header-divider"></div>
                    </div>
                    <div class="col-xs-5">
                        <span>Лучшие предложения от банков для физических и юридических лиц!</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 cities-wrapper">
                <div class="pull-right">
                    <?php get_template_part('template-parts/geoip-form'); ?>
                </div>
            </div>
        </div>
    </div>



    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle navbar-left collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Показать навигацию</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div id="navbar-collapse" class="collapse navbar-collapse ">
                <ul class="nav navbar-nav navbar-center">
                    <?php $menu_items = wp_get_nav_menu_items('Top'); ?>
                    <?php foreach ($menu_items as $menu_item) : ?>
                        <li>
                            <a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div id="wrapper" class="clearfix">