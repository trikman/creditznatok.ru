<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
} ?>

<?php if (post_password_required()) { ?>
    <p class="nocomments"><?php _e('This post is password protected. Enter the password to view any comments.',
            'responsive'); ?></p>

    <?php return;
} ?>

    <div class="h6" id="comments">
        <?php echo Creditznatok::get_comment_title(); ?> (<?php echo number_format_i18n(get_comments_number()); ?>)
    </div>

<?php if (have_comments()) : ?>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
        <div class="navigation">
            <div class="previous"><?php previous_comments_link(__('&#8249; Older comments', 'responsive')); ?></div>
            <!-- end of .previous -->
            <div class="next"><?php next_comments_link(__('Newer comments &#8250;', 'responsive', 0)); ?></div>
            <!-- end of .next -->
        </div><!-- end of.navigation -->
    <?php endif; ?>

    <div class="comment-list-wrapper">
        <?php wp_list_comments([
            'avatar_size' => 60,
            'type'        => 'comment',
            'callback'    => ['Creditznatok', 'comment_template'],
            'per_page'    => 9999
        ]); ?>
    </div>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
        <div class="navigation">
            <div class="previous"><?php previous_comments_link(__('&#8249; Older comments', 'responsive')); ?></div>
            <!-- end of .previous -->
            <div class="next"><?php next_comments_link(__('Newer comments &#8250;', 'responsive', 0)); ?></div>
            <!-- end of .next -->
        </div><!-- end of.navigation -->
    <?php endif; ?>

<?php else : ?>
    Никто пока ничего не написал, будьте первым
<?php endif; ?>

<?php
if ( ! empty($comments_by_type['pings'])) : // let's seperate pings/trackbacks from comments
    $count = count($comments_by_type['pings']);
    ($count !== 1) ? $txt = __('Pings&#47;Trackbacks', 'responsive') : $txt = __('Pings&#47;Trackbacks', 'responsive');
    ?>

    <h6 id="pings"><?php printf(__('%1$d %2$s for "%3$s"', 'responsive'), $count, $txt, get_the_title()) ?></h6>

    <ol class="commentlist">
        <?php wp_list_comments('type=pings&max_depth=<em>'); ?>
    </ol>


<?php endif; ?>