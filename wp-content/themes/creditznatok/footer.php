<?php
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
?>

<div id="footer" class="clearfix">
    <div class="row">
        <?php $footer_menu = Creditznatok::get_footer_menu(); ?>
        <?php foreach ($footer_menu as $parent_post => $child_posts) : ?>
            <div class="footer-menu-wrapper">
                <div class="footer-title">
                    <a href="<?php echo get_permalink($parent_post); ?>"><?php echo get_the_title($parent_post); ?></a>
                </div>
                <div class="footer-content">
                    <?php foreach ($child_posts as $child_post): ?>
                        <div class="footer-link">
                            <a href="<?php echo get_permalink($child_post); ?>"><?php echo Creditznatok::get_post_name(get_post($child_post)); ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="contacts-link-wrapper">
        <a href="<?php echo get_site_url(null, '/contacts/'); ?>">Контакты</a>
    </div>
</div><!-- end #footer -->
<?php wp_footer(); ?>
</div><!-- end of #wrapper -->
</div><!-- end of #container -->
</body>
</html>