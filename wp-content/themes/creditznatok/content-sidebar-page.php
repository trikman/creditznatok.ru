<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

get_header();

$post = get_post();
?>

<?php get_template_part('loop-header'); ?>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>
        <div class="row">
            <div class="col-md-8">
                <div id="content">
                    <?php CreditznatokFilterForm::get_filter_form(); ?>

                    <?php get_template_part('template-parts/seo_groups_childs'); ?>

                    <div class="post-content">

                        <?php if ($post->post_excerpt) : ?>
                            <div class="products-excerpt cz-block-white cz-list">
                                <?php echo do_shortcode($post->post_excerpt); ?>
                            </div>
                        <?php endif; ?>
                        
                        <?php echo CreditznatokShortcodes::products_shortcode(); ?>

                        <?php if ($post->post_content) : ?>
                            <div class="products-content cz-block-white cz-list">
                                <?php echo do_shortcode($post->post_content); ?>
                            </div>
                        <?php endif; ?>

                        <?php wp_link_pages(array(
                            'before' => '<div class="pagination">' . __('Pages:', 'responsive'),
                            'after'  => '</div>'
                        )); ?>

                        <?php comments_template('', true); ?>

                    </div>
                    <?php get_template_part('loop-nav'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <?php get_sidebar('right'); ?>
            </div>
        </div>
    <?php endwhile; ?>

<?php else : ?>
    <div class="row">
        <div class="col-md-8">
            <div id="content">
                <?php get_template_part('loop-no-posts'); ?>
            </div><!-- end of #content -->
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
