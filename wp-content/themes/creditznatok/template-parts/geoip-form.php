<?php if ( ! CreditznatokConfig::isGeoIpEnable) {
    return;
}

$city_name = CreditznatokGeoip::get_geo('real_loc') ? CreditznatokGeoip::get_geo('real_loc') : 'Выбрать город';
?>
    <div class="btn-group">
        <button id="cities-select" type="button" class="btn btn-default cities-select">
            <span class="you-city">Ваш город:&nbsp;&nbsp;&nbsp;</span><span class="city-name" title="Ваш город"><?php echo $city_name; ?><span class="caret"></span></span>
        </button>

        <div class="cz-popover-wrapper hidden">
            <div class="form-group geo">
                <input type="text" class="form-control" id="autocomplete" placeholder="Введите название города...">
                <span id="current-city" data-current-city="<?php echo $city_name; ?>" class="city-name" title="Ваш город"><?php echo $city_name; ?><span class="caret"></span></span>
            </div>
            <div class="popover-suggestions clear"></div>
        </div>
    </div>