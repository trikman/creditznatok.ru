<?php
$title                  = str_replace('Личное: ', '', get_the_title($post_id));
$home_url               = get_option('home_url');
//$icon_url               = get_template_directory_uri() . '/includes/img/post-info-icons';
$parent_service_type_id = Creditznatok::get_parent_service_type_id();
?>

<div class="product-info">
    <div class="row">
        <div class="col-md-3 text-center">

            <div class="row hidden-md hidden-lg">
                <div class="col-md-12 product-title">
                    <?php if ($post->post_type == 'offers'): ?>
                        <h1 class="single-offers-title"><?php echo $title; ?></h1>
                    <?php else : ?>
                        <a class="cz-link" href="<?php echo get_permalink($post_id); ?>"><?php echo $title; ?></a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="product-image-link">
                <a class="cz-link" href="<?php echo get_permalink($post_id); ?>">
                    <?php $image_url = get_the_post_thumbnail_url($post_id, 'full'); ?>
                    <img src="<?php echo $image_url; ?>" class="img-rounded img-responsive product-image" alt="<?php echo $title; ?>">
                </a>
            </div>

            <?php $review_id = Creditznatok::get_review_post($post_id); ?>
            <?php if ($review_id) : ?>
                <?php $comment_count = wp_count_comments($review_id); ?>
                <a href="<?php echo get_permalink($review_id); ?>" class="review-link hidden-xs hidden-sm">
                    <div class="review-link-wrap">
                        <i class="cz-icon cz-comments"></i><div class="review-link-text">Отзывы (<?php echo $comment_count->approved; ?>)</div>
                    </div>
                </a>
            <?php endif; ?>

            <?php if ('publish' == get_post_status($post_id)) {
                $text = get_post_meta($post_id, 'text', true);
                $text = $text ? $text : 'Оформить займ'; ?>
                <div class="credit-apply-button-wrapper hidden-xs hidden-sm">
                    <span class="btn cz-button btn-responsive credit_apply_button" data-url="<?php echo get_post_meta($post_id,
                        'link', true); ?>">
                        <?php echo $text; ?>
                    </span>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-9">

            <div class="row hidden-xs hidden-sm">
                <div class="col-md-12 product-title">
                    <?php if ($post->post_type == 'offers'): ?>
                        <h1 class="single-offers-title"><?php echo $title; ?></h1>
                    <?php else : ?>
                        <a class="cz-link" href="<?php echo get_permalink($post_id); ?>"><?php echo $title; ?></a>
                    <?php endif; ?>
                </div>
            </div>

            <?php $razmer_zayma_text = $parent_service_type_id == 6275 ? 'Стоимость годового обслуживания' : 'Сумма'; ?>
            <div class="product-param-wrapper">
                <div class="product-param-1 cz-left cz-ceil">
                    <i class="cz-icon cz-razmer-zayma"></i>
                </div>
                <div class="product-param-2 cz-left cz-ceil">
                    <?php echo $razmer_zayma_text; ?>
                </div>
                <div class="product-param-3 cz-left cz-ceil">
                    <?php CreditznatokShortcodes::display_razmer_zayma($post_id); ?>
                </div>
            </div>

            <div class="product-param-wrapper">
                <div class="product-param-1 cz-left cz-ceil">
                    <i class="cz-icon cz-percents"></i>
                </div>
                <div class="product-param-2 cz-left cz-ceil">
                    Процентная ставка
                </div>
                <div class="product-param-3 cz-left cz-ceil">
                    <?php CreditznatokShortcodes::display_percents($post_id, $parent_service_type_id); ?>
                </div>
            </div>

            <?php if ($parent_service_type_id != 41 && $parent_service_type_id != 132) : ?>
                <?php $doc = get_post_meta($post_id, 'doc', true); ?>
                <?php if ($doc) : ?>
                    <div class="product-param-wrapper">
                        <div class="product-param-1 cz-left cz-ceil">
                            <i class="cz-icon cz-docs"></i>
                        </div>
                        <div class="product-param-2 cz-left cz-ceil">
                            Требуемые документы
                        </div>
                        <div class="product-param-3 cz-left cz-ceil">
                            <?php echo $doc; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($parent_service_type_id == 41 || $parent_service_type_id == 132) : ?>
                <?php $proof_of_income = get_post_meta($post_id, 'proof_of_income', true); ?>
                <?php if ($proof_of_income) : ?>
                    <div class="product-param-wrapper">
                        <div class="product-param-1 cz-left cz-ceil">
                            <i class="cz-icon cz-docs"></i>
                        </div>
                        <div class="product-param-2 cz-left cz-ceil">
                            Подтверждение дохода
                        </div>
                        <div class="product-param-3 cz-left cz-ceil">
                            <?php echo $proof_of_income; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php $currency = get_post_meta($post_id, 'currency_new', true); ?>
            <?php if ($currency) : ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-currency"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        Валюта
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">
                        <?php CreditznatokShortcodes::display_currency($currency); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php $srok = CreditznatokShortcodes::display_srok($post_id, $parent_service_type_id); ?>
            <?php if ($srok): ?>
                <?php $srok_text = $parent_service_type_id == 135 ? 'Льготный период' : 'Период кредитования'; ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-calendar"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        <?php echo $srok_text; ?>
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">
                        <?php echo $srok; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($parent_service_type_id != 41) : ?>
                <?php $card_type = get_post_meta($post_id, 'card_type', true); ?>
                <?php if ($card_type) : ?>
                    <?php $card_name = $card_type == 'visa' ? 'Visa' : 'MasterCard'; ?>
                    <div class="product-param-wrapper">
                        <div class="product-param-1 cz-left cz-ceil">
                            <i class="cz-icon cz-card-type"></i>
                        </div>
                        <div class="product-param-2 cz-left cz-ceil">
                            Тип карты
                        </div>
                        <div class="product-param-3 cz-left cz-ceil">
                            <?php echo $card_name; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php $vozrast = get_post_meta($post_id, 'vozrast_min', true); ?>
            <?php if ($vozrast) : ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-age"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        Возраст
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">
                        <?php CreditznatokShortcodes::display_age($post_id); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php $time = CreditznatokShortcodes::display_time($post_id, $parent_service_type_id); ?>
            <?php if ($time) : ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-clock"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        Время одобрения
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">
                        <?php echo $time; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php $min_deposit = get_post_meta($post_id, 'min_deposit', true); ?>
            <?php if ($min_deposit || $min_deposit === '0') : ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-min-deposit"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        Минимальный взнос
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">
                        <?php echo $min_deposit . ' %'; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php $deposit_insurance = get_post_meta($post_id, 'deposit_insurance', true); ?>
            <?php if ($deposit_insurance) : ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-deposit-insurance"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        Страхование вклада
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">

                    </div>
                </div>
            <?php endif; ?>

            <?php $early_close = get_post_meta($post_id, 'early_close', true); ?>
            <?php if ($early_close) : ?>
                <?php $earle_title = $early_close == 'early_withdrawal' ? 'Досрочное снятие' : 'Досрочное расторжение' ?>
                <?php $earle_icon = $early_close == 'early_withdrawal' ? 'early-close' : 'card-type' ?>
                <div class="product-param-wrapper">
                    <div class="product-param-1 cz-left cz-ceil">
                        <i class="cz-icon cz-<?php echo $earle_icon; ?>"></i>
                    </div>
                    <div class="product-param-2 cz-left cz-ceil">
                        <?php echo $earle_title; ?>
                    </div>
                    <div class="product-param-3 cz-left cz-ceil">

                    </div>
                </div>
            <?php endif; ?>

            <?php if (135 == Creditznatok::get_parent_service_type_id()) {
                $way2get = get_post_meta($post_id, 'way2get');
                if ( ! empty($way2get)) { ?>
                    <div class="product-param-wrapper">
                        <div class="product-param-1 cz-left cz-ceil">
                            <i class="cz-icon cz-card-type"></i>
                        </div>
                        <div class="product-param-2 cz-left cz-ceil">
                            Способ получения
                        </div>
                        <div class="product-param-3 cz-left cz-ceil sprite-product-icons">
                            <?php $mapping = [
                                'На карту'           => 'cz-card',
                                'На банковский счет' => 'cz-bank',
                                'Через Contact'      => 'cz-contact',
                                'На Webmoney'        => 'cz-webmoney',
                                'На QIWI'            => 'cz-qiwi',
                                'На Яндекс Деньги'   => 'cz-yandex',
                                'Лидер'              => 'cz-leader',
                                'Золотая Корона'     => 'cz-golden-crown',
                                'Юнистрим'           => 'cz-unistream',
                                'На дом'             => 'cz-house',
                                'В офисе'            => 'cz-office',
                            ]; ?>
                            <?php foreach ($way2get as $way_name) { ?>
                                <i class="<?php echo $mapping[$way_name]; ?>" title="<?php echo $way_name; ?>"></i>
                            <?php } ?>
                        </div>
                    </div>
                <?php }
            } ?>

            <?php $review_id = Creditznatok::get_review_post($post_id); ?>
            <?php if ($review_id) : ?>
                <?php $comment_count = wp_count_comments($review_id); ?>
                <a href="<?php echo get_permalink($review_id); ?>" class="review-link hidden-md hidden-lg">
                    <div class="review-link-wrap">
                        <i class="cz-icon cz-comments"></i><div class="review-link-text">Отзывы (<?php echo $comment_count->approved; ?>)</div>
                    </div>
                </a>
            <?php endif; ?>

            <?php if ('publish' == get_post_status($post_id)) {
                $text = get_post_meta($post_id, 'text', true);
                $text = $text ? $text : 'Оформить займ'; ?>
                <div class="credit-apply-button-wrapper hidden-md hidden-lg">
                    <span class="btn cz-button btn-responsive credit_apply_button" data-url="<?php echo get_post_meta($post_id,
                        'link', true); ?>">
                        <?php echo $text; ?>
                    </span>
                </div>
            <?php } ?>

        </div>
    </div>
</div>