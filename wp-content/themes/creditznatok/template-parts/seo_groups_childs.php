<?php
/**
 * Вывод списка дочерних SEO-группировок под формой-фильтром для страниц SEO-группировок и дочерних SEO-группировок
 */

$post = get_post();

if ('seo_groups_child' != $post->post_type && 'seo_groups' != $post->post_type) {
    return;
}

$childs = null;

if ('seo_groups_child' == $post->post_type) {
    //$parents = Creditznatok::parent_posts('seo_groups', $post->ID);
    //$childs  = Creditznatok::child_posts('seo_groups_child', $parents[0]);
    $parents = Creditznatok::get_all_connected_posts('seo_groups', $post->ID);
    $childs  = Creditznatok::get_all_connected_posts('seo_groups_child', $parents[0]);

} elseif ('seo_groups' == $post->post_type) {
    //$childs = Creditznatok::child_posts('seo_groups_child', $post);
    $childs = Creditznatok::get_all_connected_posts('seo_groups_child', $post);
}

if ( ! empty($childs)) {
    ?>
    <div class="seo_groups_child_wrapper">
        <?php foreach ($childs as $child) { ?>
            <a href="<?php echo get_permalink($child->ID); ?>" class="seo_groups_child_content<?php if ($child->post_title == $post->post_title) {
                echo ' current_seo_groups_child';
            } ?>"><?php echo $child->post_title; ?></a>
        <?php } ?>
    </div>
    <?php
}