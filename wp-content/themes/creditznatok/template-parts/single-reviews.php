<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

get_header();

$post = get_post();
?>

<?php get_template_part('loop-header'); ?>
<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

        <div class="row">
            <div class="col-md-8">
                <div id="content">
                    <?php get_template_part('template-parts/seo_groups_childs'); ?>

                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php responsive_entry_top(); ?>

                        <div class="cz-block-white">
                            <?php echo do_shortcode($post->post_content); ?>
                            <?php comments_template('/comments-list.php', true); ?>
                        </div>

                        <?php get_template_part('comments-add-new'); ?>

                    </div>


                </div>
            </div>
            <div class="col-md-4">
                <?php get_sidebar('right'); ?>
            </div>
        </div>
        <?php
    endwhile;

else : ?>
    <div id="content" class="cp-content grid col-700">
        <?php get_template_part('loop-no-posts'); ?>
    </div><!-- end of #content -->
<?php endif; ?>

<?php get_footer(); ?>

<?php
return;
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

/**
 * Content/Sidebar Template
 *
 * Template Name:  Страница отзывов для товара
 *
 * @file           content-sidebar-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/content-right-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content" class="grid col-700">

    <?php get_template_part('loop-header'); ?>

    <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

            <?php responsive_entry_before(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php responsive_entry_top(); ?>

                <div class="post-entry">
                    <div class="bank-thumb">
                        <?php/*
						if ( has_post_thumbnail( get_the_ID() ) ) {
							echo get_the_post_thumbnail( get_the_ID(), 'large', array( 'alt' => get_the_title() ) );
						} elseif ( has_post_thumbnail( $post->post_parent ) ) {
							echo get_the_post_thumbnail( $post->post_parent, 'large', array( 'alt' => get_the_title() ) );
						} else {
							$parent       = get_post( $post->post_parent );
							$term         = array_pop( get_the_terms( $parent->ID, 'tax_services' ) );
							$service_post = '';//find_post( 'services', 'tax_services', $term->term_id );
//																echo "<!-- metko ".print_r($service_post,true)."-->";
							if ( ! empty( $service_post ) ) {
								$post_thumbnail = get_the_post_thumbnail( $service_post->ID, 'large', array( 'alt' => get_the_title() ) );
								if ( $post_thumbnail == '' ) {
									$term       = array_pop( get_the_terms( $service_post->ID, 'tax_banks' ) );
									$banks_post = find_post( 'banks', 'tax_banks', $term->term_id );
									if ( ! empty( $banks_post ) ) {
										echo get_the_post_thumbnail( $banks_post->ID, 'large', array( 'alt' => get_the_title() ) );
									}
								} else {
									echo $post_thumbnail;
								}
							}
						}*/
                        ?>
                        <?php if (get_post_meta($otziv_id, 'crfp-average-rating', 1)) {
                            echo $ratingHTML = '<div class="crfp-average-rating">Рейтинг пользователей<br> <div style="float:left;" class="crfp-rating crfp-rating-' . get_post_meta($otziv_id,
                                    'crfp-average-rating', 1) . '"></div><strong>' . get_post_meta($otziv_id,
                                    'crfp-average-rating', 1) . '</strong> по ' . get_post_meta($otziv_id,
                                    'crfp-total-ratings', 1) . ' отзывам.</div>';
                        } ?>
                        <div class="clear"></div>
                    </div>
                    <h1><?php the_title(); ?></h1>

                    <?php the_content(__('Read more &#8250;', 'responsive')); ?>

                    <?php wp_link_pages(array(
                        'before' => '<div class="pagination">' . __('Pages:', 'responsive'),
                        'after'  => '</div>'
                    )); ?>
                </div>
                <!-- end of .post-entry -->

                <?php get_template_part('post-data'); ?>

                <?php responsive_entry_bottom(); ?>
            </div><!-- end of #post-<?php the_ID(); ?> -->
            <?php responsive_entry_after(); ?>
            <?php responsive_comments_before(); ?>
            <?php comments_template('', true); ?>
            <?php responsive_comments_after(); ?>

            <?php
        endwhile;

        get_template_part('loop-nav');

    else :

        get_template_part('loop-no-posts');

    endif;
    ?>

</div><!-- end of #content -->

<?php get_sidebar('otzyvy'); ?>
<?php get_footer(); ?>
