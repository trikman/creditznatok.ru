<?php $lettersArray = get_option('mfo_letters');
$letters            = '';
foreach ($lettersArray as $array) {
    $letters .= $array['letter'];
}

$russianLetters = mb_ereg_replace('[0-9A-Za-z]+', '', $letters);
$englishLetters = preg_replace('/[0-9\p{Cyrillic}]/u', '', $letters);
$digits         = preg_replace('/[A-Za-z\p{Cyrillic}]+/u', '', $letters); ?>

<div class="cz-block-white mfo-by-letter">
    <div class="row">
        <div class="col-sm-3 mfo-letter-desc">
            Каталог МФО - подбор по алфавиту
        </div>
        <div class="col-sm-9 mfo-letter-wrapper">

            <?php if ($russianLetters): ?>
                <div class="letter-row">
                    <?php foreach (Creditznatok::mbStringToArray($russianLetters) as $letter) : ?>
                        <a class="mfo-single-letter" href="<?php echo site_url('mfo/search/' . $letter . '/'); ?>"><?php echo $letter; ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if ($englishLetters): ?>
                <div class="letter-row">
                    <?php foreach (str_split($englishLetters) as $letter) : ?>
                        <a class="mfo-single-letter" href="<?php echo site_url('mfo/search/' . $letter . '/'); ?>"><?php echo $letter; ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if ($digits): ?>
                <div class="letter-row">
                    <?php foreach (str_split($digits) as $letter) : ?>
                        <a class="mfo-single-letter" href="<?php echo site_url('mfo/search/' . $letter . '/'); ?>"><?php echo $letter; ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
