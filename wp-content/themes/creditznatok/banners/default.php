<?php
$outer_url = get_post_meta($this->banner->ID, 'outer_url', 1);
$inner_url = get_post_meta($this->banner->ID, 'inner_url', 1);
?>
    <div class="cz-banner-wrapper">
        <div class="cz-banner banner-gradient"<?php echo $outer_url ? ' data-outer_url="' . $outer_url . '"' : ''; ?><?php echo $inner_url ? 'data-inner_url="' . $inner_url . '"' : ''; ?>>
            <?php if ($outer_url) { ?>
                <a href="<?php echo $outer_url; ?>" target="_blank">
            <?php } elseif ($inner_url) { ?>
                <a href="<?php echo $inner_url; ?>">
            <?php } ?>
                    <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url($this->banner->ID, self::ImageSize); ?>">
            <?php if ($outer_url && $inner_url) { ?>
                </a>
            <?php } ?>
        </div>
    </div>