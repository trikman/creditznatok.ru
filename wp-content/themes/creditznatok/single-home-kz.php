<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

/**
 * Main Page Template
 *
 * Template name: Главная KZ
 *
 * @file           home-ru.php
 * @filesource     wp-content/themes/responsive/home-ru.php
 * @link           http://codex.wordpress.org/Templates
 */

get_header();
$images_url = get_template_directory_uri() . '/includes/css/new-style/images';
?>
    <div class="featured">
        <div class="row mp-navigation main-page-sprite">
            <div class="col-xs-6 mp-single-wrapper col-sm-2 col-sm-offset-3">
                <a class="mp-image-text" href="/kredity/">
                    <i class="credit"></i>
                    <div><span>Кредиты</span></div>
                </a>
            </div>
            <div class="col-xs-6 mp-single-wrapper col-sm-2">
                <a class="mp-image-text" href="/mikrozaimy/">
                    <i class="mikrozaimy"></i>
                    <div><span>Микрозаймы</span></div>
                </a>
            </div>
            <div class="col-xs-6 mp-single-wrapper col-sm-2">
                <a class="mp-image-text" href="/debetovye-karty/">
                    <i class="debetovye-karty"></i>
                    <div>
                        <span>Дебетовые карты</span>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <?php if ($post->post_excerpt) : ?>
        <div class="featured-desc cz-block">
            <?php echo $post->post_excerpt; ?>
        </div>
    <?php endif; ?>

    <div class="mp-row mp-first-row">
        <div class="row">
            <div class="col-md-4">
                <div class="cz-block bank-rating-widget">
                    <div class="row bank-rating-first-row">
                        <div class="col-xs-6">
                            <span class="rating-bank-title">Рейтинг банков</span>
                        </div>
                        <div class="col-xs-3">
                            <a href="/banki/" class="rating-mfo-title">Все банки</a>
                        </div>
                        <div class="col-xs-3">
                            <a href="/mfo/" class="rating-mfo-title">МФО</a>
                        </div>
                    </div>
                    <div class="bank-rating-second-row">
                        <div class="bank-rating-filled cz-left br-outer-first">
                            <span>Название банка</span>
                        </div>
                        <div class="bank-rating-filled cz-right br-outer-second">
                            <span>Активы (т. тг.)</span>
                        </div>
                    </div>
                    <div class="bank-rating-third-row">
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">1.</div>
                            <div class="cz-left br-inner-second cz-ceil">Казкоммерцбанк</div>
                            <div class="cz-left br-inner-third cz-ceil">5 045 802 814</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">2.</div>
                            <div class="cz-left br-inner-second cz-ceil" title="Народный банк Казахстана">Народный банк</div>
                            <div class="cz-left br-inner-third cz-ceil">4 294 527 060</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">3.</div>
                            <div class="cz-left br-inner-second cz-ceil">Цеснабанк</div>
                            <div class="cz-left br-inner-third cz-ceil">2 235 018</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">4.</div>
                            <div class="cz-left br-inner-second cz-ceil">Сбербанк России</div>
                            <div class="cz-left br-inner-third cz-ceil">1 496 169 464</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">5.</div>
                            <div class="cz-left br-inner-second cz-ceil">АТФБанк</div>
                            <div class="cz-left br-inner-third cz-ceil">1 308 514 934</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">6.</div>
                            <div class="cz-left br-inner-second cz-ceil">Банк ЦентрКредит</div>
                            <div class="cz-left br-inner-third cz-ceil">1 302 709 962</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">7.</div>
                            <div class="cz-left br-inner-second cz-ceil">ForteBank</div>
                            <div class="cz-left br-inner-third cz-ceil">1 202 039 858</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">8.</div>
                            <div class="cz-left br-inner-second cz-ceil">Kaspi Bank</div>
                            <div class="cz-left br-inner-third cz-ceil">1 192 099 302</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">9.</div>
                            <div class="cz-left br-inner-second cz-ceil">Bank RBK</div>
                            <div class="cz-left br-inner-third cz-ceil">1 020 897 255</div>
                        </div>
                        <div class="br-row">
                            <div class="cz-left br-inner-first cz-ceil">10.</div>
                            <div class="cz-left br-inner-second cz-ceil">Евразийский банк</div>
                            <div class="cz-left br-inner-third cz-ceil">984 853 336</div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="cz-block cz-posts-widget">
                    <div class="cz-posts-title">Статьи</div>
                    <ul>
                        <?php $menu_items = wp_get_nav_menu_items('Recent posts'); ?>
                        <?php foreach ($menu_items as $menu_item) : ?>
                            <li>
                                <a href="<?php echo $menu_item->url; ?>" class="cz-link"><?php echo $menu_item->title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="cz-banner">
                    <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cz-block credit-calculator-widget">
                    <div class="row v-center cc-title-row">
                        <div class="col-xs-1 cc-icon">&nbsp;</div>
                        <div class="col-xs-11 cc-title">Кредитный калькулятор</div>
                    </div>
                    <form method="POST" name="cz-calculator-form">
                        <div class="form-group">
                            <input type="text" name="cz-calculator-sum" class="form-control format-numeral" id="" placeholder="Размер кредита (руб.)">
                        </div>
                        <div class="form-group">
                            <input type="text" name="cz-calculator-percent" class="form-control format-numeral" id="" placeholder="Процент">
                        </div>
                        <div class="form-group">
                            <input type="text" name="cz-calculator-year" class="form-control format-numeral" id="" placeholder="Срок кредита (лет)">
                        </div>
                        <div class="cc-submit ">
                            <input class="btn btn-default" type="submit" value="Рассчитать">
                        </div>
                        <div class="cz-calculator-result"></div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="cz-block cz-news-widget">
                    <div class="cz-news-title-row">
                        <div class="cz-left cz-news-title">Новости</div>
                        <div class="cz-right cz-all-news-link">
                            <a href="/news/">Все новости</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="cz-news-content-row">
                        <?php $news = Creditznatok::get_news_for_main_page(); ?>
                        <div class="row">
                            <?php foreach ($news as $single_news) : ?>
                                <div class="col-md-4 cz-news-wrapper">
                                    <div class="cz-news-date">
                                        <?php echo Creditznatok::format_rus_date($single_news->post_date_gmt); ?>
                                    </div>
                                    <div class="cz-news-link">
                                        <a href="<?php echo get_permalink($single_news->ID); ?>"><?php echo $single_news->post_title; ?></a>
                                    </div>
                                    <div class="cz-news-excerpt">
                                        <?php echo Creditznatok::get_post_excerpt($single_news); ?>
                                    </div>
                                </div>
                                <div class="cz-news-divider"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($post->post_content) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="cz-block mp-desc-wrapper">
                    <?php echo $post->post_content; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
<?php get_footer(); ?>