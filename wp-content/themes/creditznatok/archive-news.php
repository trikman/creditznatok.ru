<?php
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
?>

<?php get_header(); ?>

<?php get_template_part('loop-header'); ?>

<?php if (have_posts()) : ?>

    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white news-archive">
                <h1 class="h1-news-title">Новости</h1>

                <?php while (have_posts()) : the_post(); ?>
                    <?php $post = get_post(); ?>

                    <div class="news-wrapper">
                        <div class="news-date">
                            <?php echo Creditznatok::format_rus_date($post->post_date_gmt); ?>
                        </div>
                        <div class="news-link">
                            <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>
                        </div>
                        <div class="news-excerpt">
                            <?php echo Creditznatok::get_post_excerpt($post, 360); ?>
                        </div>
                    </div>
                    <div class="news-divider"></div>

                <?php endwhile; ?>

                <div class="pagination">
                    <?php global $wp_query;
                    $paged = $wp_query->query['paged'] ? $wp_query->query['paged'] : 1;
                    $min   = (($paged - 1) * 10) + 1;
                    $max   = $paged * 10 <= $wp_query->found_posts ? $paged * 10 : $wp_query->found_posts; ?>
                    <div class="pagination-info">Показаны новости
                        <span class="bold"><?php echo $min; ?>-<?php echo $max; ?> из <?php echo $wp_query->found_posts; ?></span>
                    </div>
                    <?php
                    $args = [
                        'base'      => get_site_url() . '/news/%_%',
                        'format'    => 'page/%#%/',
                        'total'     => $wp_query->max_num_pages,
                        'current'   => $paged,
                        'end_size'  => 2,
                        'mid_size'  => 2,
                        'prev_next' => false,
                        'type'      => 'plain',
                    ];
                    echo paginate_links($args); ?>
                </div>

            </div>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php else : ?>
    <div class="row">
        <div class="col-md-8">
            <div id="content">
                <?php get_template_part('loop-no-posts'); ?>
            </div><!-- end of #content -->
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>