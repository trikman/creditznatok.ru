<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 * Template name: Заявка на займ
 * 
 */

get_header(); ?>

<div id="content" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">
<?php echo do_shortcode('[linkprofit_form_api_view]');?>
</div><!-- end of #content -->

<?php get_sidebar('banki'); ?>
<?php get_footer(); ?>