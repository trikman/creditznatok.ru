<?php
// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

get_header(); ?>
<?php get_template_part('loop-header'); ?>
    <div id="banks-list">
        <div class="row">
            <div class="col-md-8">
                <?php get_template_part('template-parts/search-mfo-by-letter'); ?>

                <div class="cz-block-white">
                    <div class="rating-bank-title">Каталог МФО</div>
                    <?php
                    $query = Creditznatok::get_archive_mfo_query();
                    while ($query->have_posts()) {
                        $query->the_post();
                        $post_id    = get_the_ID();
                        $reg_number = get_post_meta($post_id, 'reg_no', true); ?>

                        <div class="banks-wrapper mfo-wrapper">
                            <div class="row v-center-all">
                                <div class="col-xs-4 border">
                                    <a href="<?php the_permalink(); ?>" class="cz-bank-link">
                                        <?php echo get_the_post_thumbnail($post_id, 'large',
                                            ['alt' => get_the_title(), 'width' => '20px']); ?>
                                    </a>
                                </div>
                                <div class="col-xs-8 bank-fields-col">
                                    <div class="bank-fields">
                                        <a class="bank-list-title-link border-bottom" href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </div>
                                    <?php if ($reg_number) { ?>
                                        <div class="bank-fields">
                                            <span class="no-break-word"><?php echo isset(CreditznatokConfig::$regNumber) ? CreditznatokConfig::$regNumber : 'Регистрационный номер' ?>:</span>
                                            <br>
                                            <span class="bold"><?php echo $reg_number ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>

                    <div class="pagination">
                        <?php
                        $paged = $query->query['paged'] ? $query->query['paged'] : 1;
                        $min   = (($paged - 1) * 40) + 1;
                        $max   = $paged * 40 <= $query->found_posts ? $paged * 40 : $query->found_posts; ?>
                        <div class="pagination-info">Показаны МФО
                            <span class="bold"><?php echo $min; ?>-<?php echo $max; ?> из <?php echo $query->found_posts; ?></span>
                        </div>
                        <?php
                        $args = [
                            'base'      => get_site_url() . '/mfo/%_%',
                            'format'    => 'page/%#%/',
                            'total'     => $query->max_num_pages,
                            'current'   => $paged,
                            'end_size'  => 2,
                            'mid_size'  => 2,
                            'prev_next' => false,
                            'type'      => 'plain',
                        ];
                        echo paginate_links($args); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cz-banner">
                    <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>