<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
$post = get_post();

$reg_number_title = isset(CreditznatokConfig::$regNumber) ? CreditznatokConfig::$regNumber : 'Регистрационный номер';
$mapping          = [
    'reg_no'          => $reg_number_title,
    'legal_name'      => 'Юридическое наименование',
    'legal_address'   => 'Юридический адрес',
    'rating_expert'   => 'Рейтинг РА "Эксперт"',
    'foundation_date' => 'Дата внесения в гос. реестр',
    'ogrn'            => 'ОГРН',
    'registry_number' => '№ п/п в реестре Территориальных филиалов НБРК',
    'kz_mikrozaimy'   => 'Микрозайм',
];

get_header(); ?>

<?php get_template_part('loop-header'); ?>


    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white single-banks">

                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <?php $image_url = get_the_post_thumbnail_url(get_post(), 'full'); ?>
                        <img src="<?php echo $image_url; ?>" class="img-rounded img-responsive product-image" alt="<?php the_title(); ?>">
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <h1 class="h1-title"><?php the_title(); ?></h1>
                        <div class="bank-info-wrapper">
                            <?php foreach ($mapping as $meta_name => $text) : ?>
                                <?php $meta_value = get_post_meta(get_the_ID(), $meta_name, 1); ?>
                                <?php if ($meta_value): ?>
                                    <?php if ($text == 'Микрозайм') {
                                        $meta_value = '<a href="'.get_permalink($meta_value).'">'.get_the_title($meta_value).'</a>';
                                    } ?>
                                    <div class="single-bank-info">
                                        <div class="row v-center">
                                            <div class="col-xs-6 bank-param-name"><?php echo $text; ?></div>
                                            <div class="col-xs-6 bank-param-value"><?php echo $meta_value; ?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>                            

                            <?php
                            $args = [
                                'post_type'      => 'mfo',
                                'orderby'        => 'meta_value',
                                'meta_key'       => 'city_name',
                                'order'          => 'ASC',
                                'post_parent'    => $post_id,
                                'posts_per_page' => -1,
                            ];

                            $child_posts       = Creditznatok::get_posts('mfo', $args);
                            if ( ! empty ($child_posts)) : ?>
                                <div class="cz-divider"></div>
                                <h4>Адреса МФО:</h4>
                                <?php
                                foreach ($child_posts as $post) :
                                    $city_id = get_post_meta($post->ID, 'city', true);
                                    $city_post = get_post($city_id);
                                    $city      = $city_post->post_title;
                                    $address   = get_post_meta($post->ID, 'address', true);
                                    ?>
                                    <div class="single-bank-info">
                                        <div class="row v-center">
                                            <div class="col-xs-6 bank-param-name"><?php echo $city; ?></div>
                                            <div class="col-xs-6 bank-param-value"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $address; ?></a></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($post->post_content): ?>
                <div class="cz-block-white">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>