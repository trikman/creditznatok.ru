<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
$post = get_post();
get_header(); ?>

<?php get_template_part('loop-header'); ?>

<?php if (have_posts()) : ?>

    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white single-banks">

                <?php while (have_posts()) :
                    the_post(); ?>
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <?php $image_url = get_the_post_thumbnail_url(get_post(), 'full'); ?>
                            <img src="<?php echo $image_url; ?>" class="img-rounded img-responsive product-image" alt="<?php the_title(); ?>">
                        </div>
                        <div class="col-sm-8 col-xs-12">
                            <h1 class="h1-title"><?php the_title(); ?></h1>

                            <?php
                            $bank_children = get_pages('child_of=' . get_the_ID());
                            foreach ($bank_children as $ch) {
                                if (mb_strpos($ch->post_title, 'Реквизиты', 0, 'UTF-8') === 0) {
                                    $rekvizit_id = $ch->ID;
                                }

                                if (mb_strpos($ch->post_title, 'Отзывы', 0, 'UTF-8') === 0) {
                                    $otziv_id = $ch->ID;
                                }
                            }
                            ?>

                            <div class="bank-links">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active">
                                        <a href="#">О банке</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo Creditznatok::get_bank_data_link(); ?>">Реквизиты</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo Creditznatok::get_bank_reviews_link(); ?>">Отзывы</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bank-info-wrapper">
                                <?php foreach (CreditznatokConfig::$banks_fields_mapping as $text => $meta_name) : ?>
                                    <?php $meta_value = get_post_meta(get_the_ID(), $meta_name, 1); ?>
                                    <?php if ($meta_value): ?>
                                        <div class="single-bank-info">
                                            <div class="row v-center">
                                                <div class="col-xs-6 bank-param-name"><?php echo $text; ?></div>
                                                <div class="col-xs-6 bank-param-value"><?php echo $meta_value; ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>

                <?php if (have_comments()) : ?>
                    <div class="cz-divider"></div>
                    <?php comments_template('/comments-list.php', true); ?>
                <?php endif; ?>

            </div>

            <?php get_template_part('comments-add-new'); ?>

            <?php if ($post->post_content): ?>
                <div class="cz-block-white">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php else : ?>
    <div class="row">
        <div class="col-md-8">
            <div id="content">
                <?php get_template_part('loop-no-posts'); ?>
            </div><!-- end of #content -->
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>