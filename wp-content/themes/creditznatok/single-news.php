<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

get_header(); ?>

<?php get_template_part('loop-header'); ?>

<?php if (have_posts()) : ?>

    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white single-news">
                <h1 class="h1-title"><?php the_title(); ?></h1>

                <?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>

                <div class="cz-divider"></div>
                <?php comments_template('/comments-list.php', true); ?>

            </div>
            <?php get_template_part('comments-add-new'); ?>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php else : ?>
    <div class="row">
        <div class="col-md-8">
            <div id="content">
                <?php get_template_part('loop-no-posts'); ?>
            </div><!-- end of #content -->
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>