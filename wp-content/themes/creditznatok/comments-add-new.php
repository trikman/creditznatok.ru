<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

if ( ! comments_open()) {
    return;
}

$add_title = Creditznatok::get_comment_type();
$commenter = wp_get_current_commenter();

$fields = [
    'author' => '<div class="form-group"><label for="usr">Ваше имя: <span class="required">*</span></label>
<input class="form-control" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" required ></div>',
    'email'  => '<p class="comment-form-email"><label for="email">' . __('E-mail',
            'responsive') . '</label> ' . ($req ? '<span class="required">*</span>' : '') .
                '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" /></p>',
    'url'    => '<p class="comment-form-url"><label for="url">' . __('Website', 'responsive') . '</label>' .
                '<input id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></p>',
];

$args = [
    'id_form'            => 'commentform',
    'id_submit'          => 'submit',
    'title_reply_before' => '',
    'title_reply_after'  => '',
    'title_reply'        => __('', 'multi'),
    'title_reply_to'     => __('Оставить ответ к %s', 'multi'),
    'cancel_reply_link'  => __('Отменить ответ', 'multi'),
    'label_submit'       => __('Отправить', 'multi'),

    'comment_field' => '<div class="form-group"><label for="comment">Ваш ' . $add_title . ': <span class="required">*</span></label>
<textarea class="form-control" rows="5" id="comment" name="comment"></textarea></div>',

    'must_log_in' => '<p class="must-log-in">' .
                     sprintf(
                         __('Вы должны <a href="%s">войти</a> что бы оставлять отзывы.', 'multi'),
                         wp_login_url(apply_filters('the_permalink', get_permalink()))
                     ) . '</p>',

    'logged_in_as' => '<p class="logged-in-as">' .
                      sprintf(
                          __('Вы ввошли как <a href="%1$s">%2$s</a>. <a href="%3$s" title="Выйти">Выйти?</a>',
                              'multi'),
                          admin_url('profile.php'),
                          $user_identity,
                          wp_logout_url(apply_filters('the_permalink', get_permalink()))
                      ) . '</p>',

    'submit_button' => '<input name="submit" type="submit" id="submit" class="submit btn btn-default cz-button" value="Отправить" tabindex="6">',

    'comment_notes_before' => '<div class="h3">Оставить ' . $add_title . '</div>',
    'comment_notes_after'  => '',
    'fields'               => apply_filters('comment_form_default_fields', $fields),
];
?>
<div id="add-comments" class="cz-block-white">
    <?php
    $defaults = ['fields' => apply_filters('comment_form_default_fields', $fields)];
    comment_form($args);
    ?>
</div>

