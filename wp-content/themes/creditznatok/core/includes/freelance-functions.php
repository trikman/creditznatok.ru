<?php

function post_connection_types() {
	p2p_register_connection_type( array(
		'name'     => 'posts_to_posts',
		'from'     => 'page',
		'to'       => 'page',
		'sortable' => true
	) );
}

add_action( 'p2p_init', 'post_connection_types' );
add_action( 'init', 'create_reviews_page_init' );

// Meta fields for pages
//add_action( 'admin_init', 'ga_west_page_extra_fields', 1 );
add_action( 'add_meta_boxes', 'ga_west_page_extra_fields', 1 );

function create_reviews_page_init() {
	//pe($_POST);
	if ( isset( $_POST['create_for'] ) && $_POST['create_for'] ) {
		/* Создание страницы*/
		if ( $_POST['type'] == 'offer_reviews' ) {
			$taxonomy = 'tax_offers';
		} else if ( $_POST['type'] == 'service_reviews' ) {
			$taxonomy = 'tax_services';
		} else {
			die ( 'Неизвестная таксономия' );
		}

		$page_id = wp_insert_post( array(
				'post_title'     => $_POST['name'],
				'post_type'      => $_POST['type'],
				'post_status'    => 'publish',
				'comment_status' => 'open'
			) );
		if ( $page_id && ! is_object( $page_id ) ) {
			on_post_save( $page_id );                                            // создаем ему таксономию
			$term = get_term_by_post( get_post( $_POST['create_for'] ) );
			wp_set_object_terms( $page_id, $term->slug, $taxonomy, true );    // привязываем к родителю

			die ( "<a href='" . admin_url() . "/post.php?post={$page_id}&action=edit'>Страница отзывов создана</a>" );
		} else {
			die ( 'Ошибка при создании страницы' );
		}
		exit;
	}
}

function ga_west_page_extra_fields() {
	add_meta_box( 'extra_fields', 'Дополнительные параметры', 'ga_west_extra_fields_box_func', 'offers', 'normal', 'high' );
	add_meta_box( 'extra_fields', 'Дополнительные параметры', 'ga_west_extra_fields_box_func_services', 'services', 'normal', 'high' );
}

function ga_west_extra_fields_box_func_services( $post ) {
	?>
	<div class="reviews">


		<table border=0>

			<tr>
				<td><b>Страница отзывов об услуге банка</b></td>
				<td> &mdash;
					<?
					$shownew = true;
					if ( $children = child_posts( $post, 'service_reviews' ) ) {
						echo "<a target='_blank' href='" . admin_url() . "/post.php?post={$children[0]->ID}&action=edit'>{$children[0]->post_title}</a>";

					} else {
						?>
						<div class="service_reviews_name">
							<input type="text" id="service_reviews_name"><a href="javascript:;" onClick="createPage('service_reviews','<? echo $post->ID; ?>');">Создать страницу отзывов</a>
						</div>

					<?
					}
					?>
				</td>

			</tr>
		</table>

	</div>

	<script>
		function createPage(type, for_ID) {
			if (jQuery("#" + type + "_name").val() == '') alert('Задайте имя страницы!'); else
				jQuery.ajax({
					type   : "POST",
					url    : '<?echo admin_url();?>/',
					data   : 'create_for=' + for_ID + '&type=' + type + '&name=' + jQuery("#" + type + "_name").val(),
					success: function (data) {
						jQuery("." + type + "_name").html(data);
					}
				});
			return false;
		}
	</script>
<?
}

function ga_west_extra_fields_box_func( $post ) {
	?>
	<style type="text/css">
		.tab-options {
			font-size: 13px;
			line-height: 1.5;
			margin: 1em 0;
		}

		.option-header {
			font-size: 17px;
		}

		.tab-options input:not([type="checkbox"]) {
			width: 100%;
		}

		.option-wrapper {
			border: 1px solid gray;
			padding: 10px;
			margin-bottom: 10px;
		}
		/*------------------------------------------------*/
		<?php if(current_user_can('editor')): ?>
		.post-php.post-type-offers #postcustom {
			display: none !important;
		}

		<?php endif; ?>
		.phone-opt, {
			font-size: 13px;
			line-height: 1.5;
			margin: 1em 0;
		}

		.phone-opt input {
			width: 100%;
			margin-bottom: 10px;
		}

		#rt_tabs li {
			margin-top: 15px;
			display: inline-block;
			margin-right: 10px;
		}

		#rt_tabs li a {
			padding: 5px 15px;
			color: #000;
			border-bottom: 2px solid #455fac;
		}

		#rt_tabs li a:hover {
			background: #d9d9d9;
		}

		#rt_tabs li.ui-state-active a {
			background: #455fac;
			color: #fff;
		}
	</style>
	<?php wp_enqueue_script( 'jquery-ui-tabs' ); ?>
	<script type="text/javascript">
		jQuery(document).ready(function () {
			jQuery("#rt_tabs").tabs();
		});
	</script>

	<div class="reviews">


		<table border=0>
			<tr>
				<td><b>Страница отзывов о продукте </b></td>
				<td> &mdash;
					<?
					if ( $children = child_posts( $post, 'offer_reviews' ) ) {
						echo "<a target='_blank' href='" . admin_url() . "/post.php?post={$children[0]->ID}&action=edit'>{$children[0]->post_title}</a>";
					} else {
						?>
						<div class="offer_reviews_name">
							<input type="text" id="offer_reviews_name"><a href="javascript:;" onClick="createPage('offer_reviews','<? echo $post->ID; ?>');">Создать страницу отзывов</a>
						</div>
					<?

					}
					?>
				</td>
			</tr>
			<tr>
				<td><b>Страница отзывов об услуге банка</b></td>
				<td> &mdash;
					<?
					$shownew = true;
					if ( $parents = parent_posts( $post->ID, 'services' ) ) {
						if ( $children = child_posts( $parents[0], 'service_reviews' ) ) {
							echo "<a target='_blank' href='" . admin_url() . "/post.php?post={$children[0]->ID}&action=edit'>{$children[0]->post_title}</a>";
							$shownew = false;
						}
					}

					if ( $shownew && $parents[0] ) {
						?>
						<div class="service_reviews_name">
							<input type="text" id="service_reviews_name"><a href="javascript:;" onClick="createPage('service_reviews','<? echo $parents[0]->ID; ?>');">Создать страницу отзывов</a>
						</div>

					<?

					} else if ( ! $parents[0] ) {
						echo "Услуга банка еще не создана.";
					}
					?>
				</td>

			</tr>
		</table>

	</div>

	<script>
		function createPage(type, for_ID) {
			if (jQuery("#" + type + "_name").val() == '') alert('Задайте имя страницы!'); else
				jQuery.ajax({
					type   : "POST",
					url    : '<?echo admin_url();?>/',
					data   : 'create_for=' + for_ID + '&type=' + type + '&name=' + jQuery("#" + type + "_name").val(),
					success: function (data) {
						jQuery("." + type + "_name").html(data);
					}
				});
			return false;
		}
	</script>


	<?php $post_service_type = get_post_service_type();	?>
	<div id="rt_tabs">
		<ul>
			<li><a href="#banks">Инфо для банка</a></li>
			<li><a href="#cards">Инфо по картам</a></li>
		</ul>

		<div id="cards">
			<div class="tab-options">
				<div class="option-wrapper">
					<div class="option-header">
						<?php if ( $post_service_type == 'debetovye-karty' ) {
							echo _e( 'Стоимость годового обслуживания', 'multi' );
						}else {
							echo _e( 'Размер займа', 'multi' );
						}
						?>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'Старое значение', 'multi' ); ?><br>
							<input type="text" name="extra[razmer_zayma]" value="<?php echo get_post_meta( $post->ID, 'razmer_zayma', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'min', 'multi' ); ?><br>
							<input type="text" name="extra[razmer_zayma_min]" value="<?php echo get_post_meta( $post->ID, 'razmer_zayma_min', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'max', 'multi' ); ?><br>
							<input type="text" name="extra[razmer_zayma_max]" value="<?php echo get_post_meta( $post->ID, 'razmer_zayma_max', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'Точное значение', 'multi' ); ?>
							<input type="hidden" name="extra[razmer_zayma_single]" value="false" />
							<input type="checkbox" name="extra[razmer_zayma_single]" value="true" <?php checked( get_post_meta( $post->ID, 'razmer_zayma_single', true ), 'true' ); ?> />
						</label>
					</div>
				</div>

				<div class="option-wrapper">
					<div class="option-header">
						<?php echo _e( 'Количество %', 'multi' ); ?>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'Старое значение', 'multi' ); ?><br>
							<input type="text" name="extra[percents]" value="<?php echo get_post_meta( $post->ID, 'percents', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'min', 'multi' ); ?><br>
							<input type="text" name="extra[percents_min]" value="<?php echo get_post_meta( $post->ID, 'percents_min', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'max', 'multi' ); ?><br>
							<input type="text" name="extra[percents_max]" value="<?php echo get_post_meta( $post->ID, 'percents_max', true ); ?>" />
						</label>
					</div>
					<div class="option-single-value">
						<label>
							<?php echo _e( 'Точное значение', 'multi' ); ?>
							<input type="hidden" name="extra[percents_single]" value="false" />
							<input type="checkbox" name="extra[percents_single]" value="true" <?php checked( get_post_meta( $post->ID, 'percents_single', true ), 'true' ); ?> />
						</label>
					</div>
				</div>

				<?php if ( $post_service_type == 'kredity' || $post_service_type == 'creditnye-karty' || $post_service_type == 'mikrozaimy' || $post_service_type == 'credit-dlya-business' || $post_service_type == 'ipoteka'  || $post_service_type == 'avtokredity') { ?>
					<div class="option-wrapper">
						<label><?php echo _e( 'Документ', 'multi' ); ?><br>
							<input type="text" name="extra[doc]" value="<?php echo get_post_meta( $post->ID, 'doc', true ); ?>" />
						</label>
					</div>
				<?php } ?>

				<?php if ( $post_service_type != 'debetovye-karty' ) { ?>
					<div class="option-wrapper">
						<div class="option-header">
							<?php if ( $post_service_type == 'kredity' || $post_service_type == 'avtokredity' || $post_service_type == 'credit-dlya-business' ) {
								echo _e( 'Срок (месяцев)', 'multi' );
							} elseif ( $post_service_type == 'vklady' || $post_service_type == 'mikrozaimy' || $post_service_type == 'creditnye-karty' ) {
								echo _e( 'Срок (дней)', 'multi' );
							} elseif ( $post_service_type == 'ipoteka' ) {
								echo _e( 'Срок (лет)', 'multi' );
							} else {
								echo _e( 'Срок', 'multi' );
							} ?>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Старое значение', 'multi' ); ?><br>
								<input type="text" name="extra[srok]" value="<?php echo get_post_meta( $post->ID, 'srok', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'min', 'multi' ); ?><br>
								<input type="text" name="extra[srok_min]" value="<?php echo get_post_meta( $post->ID, 'srok_min', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'max', 'multi' ); ?><br>
								<input type="text" name="extra[srok_max]" value="<?php echo get_post_meta( $post->ID, 'srok_max', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Точное значение', 'multi' ); ?>
								<input type="hidden" name="extra[srok_single]" value="false" />
								<input type="checkbox" name="extra[srok_single]" value="true" <?php checked( get_post_meta( $post->ID, 'srok_single', true ), 'true' ); ?> />
							</label>
						</div>
					</div>
				<?php } ?>

				<?php if ( $post_service_type == 'kredity' || $post_service_type == 'creditnye-karty' || $post_service_type == 'mikrozaimy' || $post_service_type == 'credit-dlya-business' || $post_service_type == 'ipoteka'  || $post_service_type == 'avtokredity') { ?>
					<div class="option-wrapper">
						<div class="option-header">
							<?php echo _e( 'Возраст (лет)', 'multi' ); ?>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Старое значение', 'multi' ); ?><br>
								<input type="text" name="extra[vozrast]" value="<?php echo get_post_meta( $post->ID, 'vozrast', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'min', 'multi' ); ?><br>
								<input type="text" name="extra[vozrast_min]" value="<?php echo get_post_meta( $post->ID, 'vozrast_min', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'max', 'multi' ); ?><br>
								<input type="text" name="extra[vozrast_max]" value="<?php echo get_post_meta( $post->ID, 'vozrast_max', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Точное значение', 'multi' ); ?>
								<input type="hidden" name="extra[vozrast_single]" value="" />
								<input type="checkbox" name="extra[vozrast_single]" value="true" <?php checked( get_post_meta( $post->ID, 'vozrast_single', true ), 'true' ); ?> />
							</label>
						</div>
					</div>
				<?php } ?>

				<?php if ( $post_service_type == 'kredity' || $post_service_type == 'creditnye-karty' || $post_service_type == 'mikrozaimy' || $post_service_type == 'credit-dlya-business' || $post_service_type == 'ipoteka'  || $post_service_type == 'avtokredity') { ?>
					<div class="option-wrapper">
						<div class="option-header">
							<?php if ( $post_service_type == 'mikrozaimy' ) {
								echo _e( 'Время одобрения (минут)', 'multi' );
							} elseif ( $post_service_type == 'kredity' || $post_service_type == 'avtokredity' || $post_service_type == 'ipoteka' || $post_service_type == 'credit-dlya-business' || $post_service_type == 'creditnye-karty' ) {
								echo _e( 'Время одобрения (дней)', 'multi' );
							} else {
								echo _e( 'Время одобрения', 'multi' );
							} ?>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Старое значение', 'multi' ); ?><br>
								<input type="text" name="extra[time]" value="<?php echo get_post_meta( $post->ID, 'time', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'min', 'multi' ); ?><br>
								<input type="text" name="extra[time_min]" value="<?php echo get_post_meta( $post->ID, 'time_min', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'max', 'multi' ); ?><br>
								<input type="text" name="extra[time_max]" value="<?php echo get_post_meta( $post->ID, 'time_max', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Точное значение', 'multi' ); ?>
								<input type="hidden" name="extra[time_single]" value="false" />
								<input type="checkbox" name="extra[time_single]" value="true" <?php checked( get_post_meta( $post->ID, 'time_single', true ), 'true' ); ?> />
							</label>
						</div>
					</div>
				<?php } ?>

				<?php if ( $post_service_type == 'vklady' || $post_service_type == 'debetovye-karty' || $post_service_type == 'credit-dlya-business' ) { ?>
					<div class="option-wrapper">
						<div class="option-header">
							<?php echo _e( 'Валюта', 'multi' ); ?>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Старое значение', 'multi' ); ?><br>
								<input type="text" name="extra[currency]" value="<?php echo get_post_meta( $post->ID, 'currency', true ); ?>" />
							</label>
						</div>
						<div class="option-single-value">
							<label>
								<?php echo _e( 'Новое значение', 'multi' ); ?><br>
								<?php $current_currency = get_post_meta( $post->ID, 'currency_new', true ); ?>
								<select name="extra[currency_new]">
									<option value="">Выберите валюту</option>
									<option <?php selected( $current_currency ,CreditznatokConfig::Currency ); ?> value="<?php echo CreditznatokConfig::Currency?>"><?php echo CreditznatokConfig::CurrencySingle; ?></option>
									<option <?php selected( $current_currency ,'usd' ); ?> value="usd">Доллар</option>
									<option <?php selected( $current_currency ,'eur' ); ?> value="eur">Евро</option>
								</select>
							</label>
						</div>
					</div>
				<?php } ?>

				<?php
				global $current_user;
				$user_roles = $current_user->roles;
				$user_role  = array_shift( $user_roles );
				if ( 'editor' == $user_role ) {
					?>
					<input type="hidden" name="extra[link]" value="<?php echo get_post_meta( $post->ID, 'link', true ); ?>" />
				<?php
				} else {
					?>
					<div class="option-wrapper">
						<label><?php echo _e( 'Ссылка на оформление', 'multi' ); ?><br />
							<input type="text" name="extra[link]" value="<?php echo get_post_meta( $post->ID, 'link', true ); ?>" />
						</label>
					</div>
				<?php } ?>

				<div class="option-wrapper">
					<label><?php echo _e( 'Текст для кнопки', 'multi' ); ?><br />
						<input type="text" name="extra[text]" value="<?php echo get_post_meta( $post->ID, 'text', true ); ?>" />
					</label>
				</div>

				<?php if ( $post_service_type == 'avtokredity' || $post_service_type == 'ipoteka' ) { ?>
					<div class="option-wrapper">
						<label><?php echo _e( 'Минимальный взнос', 'multi' ); ?><br />
							<input type="text" name="extra[min_deposit]" value="<?php echo get_post_meta( $post->ID, 'min_deposit', true ); ?>" />
						</label>
					</div>
				<?php } ?>

				<?php if ($post_service_type == 'creditnye-karty' || $post_service_type == 'debetovye-karty') : ?>
					<div class="option-wrapper">
						<label>
							<?php echo _e( 'Тип карты', 'multi' ); ?><br>
							<?php $card_type = get_post_meta( $post->ID, 'card_type', true ); ?>
							<select name="extra[card_type]">
								<option value="">Выберите валюту</option>
								<option <?php selected( $card_type, 'visa' ); ?> value="visa">Visa</option>
								<option <?php selected( $card_type, 'master_card' ); ?> value="master_card">MasterCard</option>
							</select>
						</label>
					</div>
				<?php endif; ?>

				<?php if ( $post_service_type == 'vklady' ) { ?>
					<div class="option-wrapper">
						<label>
							<?php echo _e( 'Страхование вклада', 'multi' ); ?>
							<input type="hidden" name="extra[deposit_insurance]" value="" />
							<input type="checkbox" name="extra[deposit_insurance]" value="true" <?php checked( get_post_meta( $post->ID, 'deposit_insurance', true ), 'true' ); ?> />
						</label>
					</div>

					<div class="option-wrapper">
						<label>
							<?php echo _e( 'Досрочное закрытие', 'multi' ); ?><br>
							<?php $early_close = get_post_meta( $post->ID, 'early_close', true ); ?>
							<select name="extra[early_close]">
								<option value="">Выберите тип</option>
								<option <?php selected( $early_close, 'early_withdrawal' ); ?> value="early_withdrawal">Досрочное расторжение</option>
								<!--<option <?php selected( $early_close, 'early_dissolution' ); ?> value="early_dissolution">Досрочное расторжение</option>-->
							</select>
						</label>
					</div>
				<?php } ?>

				<?php if ( $post_service_type == 'avtokredity' ) { ?>
					<div class="option-wrapper">
						<div class="option-single-value">
							<?php echo _e( 'Вид ТС', 'multi' ); ?>
							<div>
								<label>
									<input type="hidden" name="extra[vehicle_type_light]" value="" />
									<input type="checkbox" name="extra[vehicle_type_light]" value="true" <?php checked( get_post_meta( $post->ID, 'vehicle_type_light', true ), 'true' ); ?> />
									Легковой транспорт
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[vehicle_type_moto]" value="" />
									<input type="checkbox" name="extra[vehicle_type_moto]" value="true" <?php checked( get_post_meta( $post->ID, 'vehicle_type_moto', true ), 'true' ); ?> />
									Мототранспорт
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[vehicle_type_commercial]" value="" />
									<input type="checkbox" name="extra[vehicle_type_commercial]" value="true" <?php checked( get_post_meta( $post->ID, 'vehicle_type_commercial', true ), 'true' ); ?> />
									Коммерческий транспорт
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[vehicle_type_water]" value="" />
									<input type="checkbox" name="extra[vehicle_type_water]" value="true" <?php checked( get_post_meta( $post->ID, 'vehicle_type_water', true ), 'true' ); ?> />
									Водный транспорт
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[vehicle_type_motorcycle]" value="" />
									<input type="checkbox" name="extra[vehicle_type_motorcycle]" value="true" <?php checked( get_post_meta( $post->ID, 'vehicle_type_motorcycle', true ), 'true' ); ?> />
									Мотоцикл
								</label>
							</div>
						</div>
					</div>

					<div class="option-wrapper">
						<div class="option-single-value">
							<?php echo _e( 'Тип ТС', 'multi' ); ?>
							<div>
								<label>
									<input type="hidden" name="extra[manufacturer_foreign_new]" value="" />
									<input type="checkbox" name="extra[manufacturer_foreign_new]" value="true" <?php checked( get_post_meta( $post->ID, 'manufacturer_foreign_new', true ), 'true' ); ?> />
									Иностранный новый
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[manufacturer_native_new]" value="" />
									<input type="checkbox" name="extra[manufacturer_native_new]" value="true" <?php checked( get_post_meta( $post->ID, 'manufacturer_native_new', true ), 'true' ); ?> />
									Отечественный новый
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[manufacturer_foreign_used]" value="" />
									<input type="checkbox" name="extra[manufacturer_foreign_used]" value="true" <?php checked( get_post_meta( $post->ID, 'manufacturer_foreign_used', true ), 'true' ); ?> />
									Иностранный подержанный
								</label>
							</div>
							<div>
								<label>
									<input type="hidden" name="extra[manufacturer_native_used]" value="" />
									<input type="checkbox" name="extra[manufacturer_native_used]" value="true" <?php checked( get_post_meta( $post->ID, 'manufacturer_native_used', true ), 'true' ); ?> />
									Отечественный подержанный
								</label>
							</div>
						</div>
					</div>
				<?php } ?>

			</div>

		</div>
		<div id="banks">
			<h3>О банке</h3>

			<label><?php echo _e( 'Актив', 'multi' ); ?><br />
				<input type="text" name="extra[active]" value="<?php echo get_post_meta( $post->ID, 'active', 1 ); ?>" /></label><br>

			<p class="phone-opt">
				<label><?php echo _e( 'Номер лицензии банка', 'multi' ); ?><br />
					<input type="text" name="extra[licension]" value="<?php echo get_post_meta( $post->ID, 'licension', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'Головной офис', 'multi' ); ?><br />
					<input type="text" name="extra[head_office]" value="<?php echo get_post_meta( $post->ID, 'head_office', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'Номера телефонов горячей линии', 'multi' ); ?><br />
					<input type="text" name="extra[phones]" value="<?php echo get_post_meta( $post->ID, 'phones', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'Официальный сайт Сбербанка России', 'multi' ); ?><br />
					<input type="text" name="extra[offsite]" value="<?php echo get_post_meta( $post->ID, 'offsite', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'Интернет банк', 'multi' ); ?><br />
					<input type="text" name="extra[online_bank]" value="<?php echo get_post_meta( $post->ID, 'online_bank', 1 ); ?>" /></label><br>
			</p>

			<h3>Реквизиты</h3>

			<p class="phone-opt">
				<label><?php echo _e( 'ОГРН', 'multi' ); ?><br />
					<input type="text" name="extra[ogrn]" value="<?php echo get_post_meta( $post->ID, 'ogrn', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'ИНН', 'multi' ); ?><br />
					<input type="text" name="extra[inn]" value="<?php echo get_post_meta( $post->ID, 'inn', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'КПП', 'multi' ); ?><br />
					<input type="text" name="extra[kpp]" value="<?php echo get_post_meta( $post->ID, 'kpp', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'ОКПО', 'multi' ); ?><br />
					<input type="text" name="extra[okpo]" value="<?php echo get_post_meta( $post->ID, 'okpo', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'БИК', 'multi' ); ?><br />
					<input type="text" name="extra[bik]" value="<?php echo get_post_meta( $post->ID, 'bik', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'SWIFT', 'multi' ); ?><br />
					<input type="text" name="extra[swift]" value="<?php echo get_post_meta( $post->ID, 'swift', 1 ); ?>" /></label><br>

				<label><?php echo _e( 'IBAN ', 'multi' ); ?><br />
					<input type="text" name="extra[iban]" value="<?php echo get_post_meta( $post->ID, 'iban', 1 ); ?>" /></label><br>
			</p>
		</div>
	</div>
	<input type="hidden" name="need_check" value="review" />
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>" />
	<div style="clear:both;"></div>

<?php
}

add_action( 'save_post', 'ga_west_page_extra_fields_update', 0 );

function ga_west_page_extra_fields_update( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return false;
	}

	if ( ! isset( $_POST['extra'] ) ) {
		return false;
	}

	if ( ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ ) ) {
		return false;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return false;
	}

/*
	$need_check = array_map( 'trim', explode( ',', $_POST['need_check'] ) );
	foreach ( $need_check as $val ) {
		$_POST['extra'][ $val ] = $_POST['extra'][ $val ];
	}
*/

	$_POST['extra'] = array_map( 'trim', $_POST['extra'] );

	//echo '<pre>';print_r($_POST['extra']);echo '</pre>';

	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) && 0 === $value ) {
			delete_post_meta( $post_id, $key );
		} else {
			update_post_meta( $post_id, $key, $value );
		}
	}

	return $post_id;
}

// [products]
function products_shortcode( $atts, $content = null ) {
	global $post;
	global $product_posts;
	global $product_posts_by_type;

	if ( count( $_POST ) ) {
		$workpost = get_post( $product_posts[ $product_posts_by_type[ get_product_type() ] ]['ID'] );
	} else {
		$workpost = get_queried_object();
	}

	if ( 'services' == $post->post_type ) {
		$services_post_type = true;
	} else {
		$services_post_type = false;
	}

	$posts = child_posts( $workpost, 'offers' );

	if ( count( $posts ) ) {
		$all_html   = array();

		if ( 'microfinance' == get_product_type() ) {
			$view_icons = true;
		} else {
			$view_icons = false;
		}

		$home_url = get_option( 'home_url' );

		foreach ( $posts as $post ) {
			if ( $post->post_status != 'private' && ( $post->post_status == 'publish' || $post->post_status == 'inactive1' || ( $services_post_type && $post->post_status == 'private' ) ) ) {

				$html = '';

				// greensid2 фильтрация
				if ( ! greensid2_filter_stack() ) {
					continue;
				}

				$otziv_id = '';
				// получение отзыва

				$child = child_posts( $post, 'offer_reviews' );
				if ( count( $child ) ) {
					$otziv_id = $child[0]->ID;
					$olink    = " о " . $post->post_title;
				}

				if ( $parent = parent_posts( $post->ID, 'services' ) ) {
					$child = child_posts( $parent[0], 'service_reviews' );
					if ( count( $child ) ) {
						$otziv_id = $child[0]->ID;
						$olink    = " о " . $parent[0]->post_title;
					}
				}

				if ( isset ( $parent ) && $parent && is_array( $parent ) && is_object( $parent[0] ) ) {
					$parent_id = $parent[0]->ID;
				} else {
					$parent_id = null;
				}

				$ratingHTML = '<a href="' . get_permalink( $parent_id ) . 'otzyvy/" style="float:left;">Отзывы </a>';
				if ( get_post_meta( $otziv_id, 'crfp-average-rating', 1 ) ) {
					$ratingHTML .= '<div class="crfp-average-rating" style="float:left;"><div class="crfp-rating crfp-rating-' . get_post_meta( $otziv_id, 'crfp-average-rating', 1 ) . '"></div></div>';
				}

				$tmp_title  = str_replace( 'Личное: ', '', get_the_title() );
				$post_id = get_the_ID();

				$html .= '<div class="clearfix"></div>';
				$html .= '<div class="product-info">';
				$html .= '<div class="credit"><a href="' . get_permalink() . '">' . $tmp_title . '</a></div>';
				$html .= '<div class="credit_image">';
				$html .= '<a href="' . get_permalink() . '">';
				$html .= get_the_post_thumbnail( $post_id, 'full', array( 'alt' => $tmp_title ) );
				$html .= '</a></div>';
				$html .= '<div class="credit_param">';
				$html .= '<table class="credit_param">';
				$html .= '<tbody>';

				$html .= '<tr>';

				// параметр 1
				$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_money.png" alt="Размер кредита" width="25" height="25" /> ';
				$html .= get_post_meta( $post_id, 'razmer_zayma', 1 ) . '</td>';

				// параметр 2
				$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_calculator.png" alt="Процентная ставка" width="25" height="25" /> ';
				$html .= get_post_meta( $post_id, 'percents', 1 ) . '</td>';
				$html .= '</tr><tr>';

				// параметр 3
				$tmp_post_meta = get_post_meta( $post_id, 'doc', 1 );
				if ( $tmp_post_meta ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_docs.png" alt="Требуемые документы" width="25" height="25" /> ';
					$html .= $tmp_post_meta . '</td>';
				}

				$tmp_post_meta = get_post_meta( $post_id, 'currency', 1 );
				if ( $tmp_post_meta ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/global.png" alt="Валюта" width="25" height="25" /> ';
					$html .= $tmp_post_meta . '</td>';
				}
				// параметр 4
				$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_calendar.png" alt="Период кредитования" width="25" height="25" /> ';
				$html .= get_post_meta( $post_id, 'srok', 1 ) . '</td>';

				$html .= '</tr>';

				$html .= '<tr>';

				$tmp_post_meta = get_post_meta( $post_id, 'vozrast', 1 );
				if ( $tmp_post_meta ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_profle.png" alt="Возраст" width="25" height="25" /> ';
					$html .= $tmp_post_meta . '</td>';
				}

				$tmp_post_meta = get_post_meta( $post_id, 'time', 1 );
				if ( $tmp_post_meta ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/uploads/2014/07/mono_clock.png" alt="Время одобрения" width="25" height="25" /> ';
					$html .= $tmp_post_meta . '</td>';
				}

				$html .= '</tr>';

				$html .= '<tr>';

				$min_deposit = get_post_meta( $post_id, 'min_deposit', true );
				if ( $min_deposit ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/min_deposit.png" alt="Минимальный депозит" width="25" height="25" /> ';
					$html .= $min_deposit . '</td>';
				}
				
				$card_type = get_post_meta( $post_id, 'card_type', true );
				if ( $card_type ) {
					if ( $card_type == 'visa' ) {
						$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/card_type.jpg" alt="Visa" width="25" height="25" /> Visa</td>';
					} elseif ( $card_type == 'master_card' ) {
						$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/card_type.jpg" alt="MasterCard" width="25" height="25" /> MasterCard</td>';
					}
				}

				$html .= '</tr>';

				$html .= '<tr>';

				$deposit_insurance = get_post_meta( $post_id, 'deposit_insurance', true );
				if ( $deposit_insurance ) {
					$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/deposit_insurance.png" alt="Страхование вклада" width="25" height="25" /> Да</td>';
				}

				$early_close = get_post_meta( $post_id, 'early_close', true );
				if ( $early_close ) {
					if ( $early_close == 'early_withdrawal' ) {
						$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/early_close.jpg" alt="Досрочное снятие" width="25" height="25" /> Досрочное снятие</td>';
					} elseif ( $early_close == 'early_dissolution' ) {
						$html .= '<td><img class="size-full wp-image-84" src="' . $home_url . '/wp-content/themes/responsive/includes/img/icons/card_type.jpg" alt="Досрочное расторжение" width="25" height="25" /> Досрочное расторжение</td>';
					}
				}

				$html .= '</tr>';

				$html .= '</tbody></table>';
				if ( $view_icons ) {
					$wey2get = get_post_meta( $post_id, 'way2get' );
					if ( ! empty( $wey2get ) ) {
						$html .= '<div class="worf-div"><strong>Способ получения</strong><br>';
						foreach ( $wey2get as $value ) {
							switch ( $value ) {
								case "На карту":
									$html .= '<i class="wofr wofr-card" title="На карту"></i> ';
									break;
								case "На банковский счет":
									$html .= '<i class="wofr wofr-bank" title="На банковский счет"></i> ';
									break;
								case "Через Contact":
									$html .= '<i class="wofr wofr-contact" title="Через Contact"></i> ';
									break;
								case "На Webmoney":
									$html .= '<i class="wofr wofr-webmoney" title="На Webmoney"></i> ';
									break;
								case "На QIWI":
									$html .= '<i class="wofr wofr-kiwi" title="На QIWI"></i> ';
									break;
								case "На Яндекс Деньги":
									$html .= '<i class="wofr wofr-yandex" title="На Яндекс Деньги"></i> ';
									break;
								case "Лидер":
									$html .= '<i class="wofr wofr-leader" title="Лидер"></i>  ';
									break;
								case "Золотая Корона":
									$html .= '<i class="wofr wofr-golden-crown" title="Золотая Корона"></i> ';
									break;
								case "Юнистрим":
									$html .= '<i class="wofr wofr-unistream" title="Юнистрим"></i> ';
									break;
								case "На дом":
									$html .= '<i class="wofr wofr-house" title="На дом"></i> ';
									break;
								case "В офисе":
									$html .= '<i class="wofr wofr-office" title="В офисе"></i> ';
									break;
							}
						}
						$html .= '</div>';
					}

				}
				$html .= '</div>';

				$tmp_post_meta = get_post_meta( $post_id, 'text', 1 );
				if ( $tmp_post_meta ) {
					$text = $tmp_post_meta;
				} else {
					$text = 'Оформить займ';
				}

				if ( ! ( $services_post_type && $post->post_status == 'private' ) ) {
					$html .= '<div class="credit_apply_button" onClick="javascript:window.open(\'' . get_post_meta( $post_id, 'link', 1 ) . '\'); yaCounter26138283.reachGoal(\'goToBankButton\'); return true;">' . $text . '</div>';
				}
				$html .= '<div class="clearfix"></div>' . $ratingHTML . '<div class="clearfix"></div></div>';

				$all_html[ greensid2_sort() ] = $html; // greensid2 сортировка
			}
		}
		wp_reset_postdata();
	}

	if ( ! count( $all_html ) ) {
		return "<div class='clearfix'></div><BR><div>К сожалению, по Вашему запросу ничего не найдено. Попробуйте упростить поиск.</div><BR>";
	}

	// Greensid2 сортировка
	global $cb_sort_params;
	if ( $cb_sort_params[ $_POST['product'] ][ $_POST['sort'] ]['order'] == 'ASC' ) {
		ksort( $all_html );
	} else {
		krsort( $all_html );
	}

	return implode( '', $all_html );
}

add_shortcode( 'products', 'products_shortcode' );

// [product]
function product_shortcode( $atts, $content = null ) {

	global $post;

	$html     = '';
	$args     = Array(
		'post_type'       => 'page',
		'name'            => 'otzyvy',
		'post_parent__in' => array( get_the_ID(), wp_get_post_parent_id( get_the_ID() ) )
	);
	$banks    = get_posts( $args );
	$otziv_id = '';

	if (isset($banks[0]) && is_object($banks[0])) {
		$otziv_id = $banks[0]->ID;
	}

	//$ratingHTML = '<a href="'.get_permalink($parent[0]->ID).'otzyvy/" style="float:left;">Отзывы</a>';
	$ratingHTML = '<a href="../otzyvy/" style="float:left;">Отзывы</a>';
	if ( get_post_meta( $otziv_id, 'crfp-average-rating', 1 ) ) {
		$ratingHTML .= '<div class="crfp-average-rating" style="float:left;"><div class="crfp-rating crfp-rating-' . get_post_meta( $otziv_id, 'crfp-average-rating', 1 ) . '"></div></div>';
	}

	$html .= '<div class="clearfix"></div>';
	$html .= '<div class="product-info">';
	$html .= '<div class="credit">' . get_the_title() . '</div>';
	$html .= '<div class="credit_image">';
	$html .= get_the_post_thumbnail( get_the_ID(), 'full', array( 'alt' => get_the_title() ) );
	$html .= '</div>';
	$html .= '<div class="credit_param">';
	$html .= '<table class="credit_param">';
	$html .= '<tbody>';

	$html .= '<tr>';

	// параметр 1
	$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_money.png" alt="Размер кредита" width="25" height="25" /> ';
	$html .= get_post_meta( get_the_ID(), 'razmer_zayma', 1 ) . '</td>';

	// параметр 2
	$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_calculator.png" alt="Процентная ставка" width="25" height="25" /> ';
	$html .= get_post_meta( get_the_ID(), 'percents', 1 ) . '</td>';
	$html .= '</tr><tr>';

	// параметр 3
	if ( get_post_meta( get_the_ID(), 'doc', 1 ) ) {
		$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_docs.png" alt="Требуемые документы" width="25" height="25" /> ';
		$html .= get_post_meta( get_the_ID(), 'doc', 1 ) . '</td>';
	}

	if ( get_post_meta( get_the_ID(), 'currency', 1 ) ) {
		$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/global.png" alt="Валюта" width="25" height="25" /> ';
		$html .= get_post_meta( get_the_ID(), 'currency', 1 ) . '</td>';
	}
	// параметр 4
	$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_calendar.png" alt="Период кредитования" width="25" height="25" /> ';
	$html .= get_post_meta( get_the_ID(), 'srok', 1 ) . '</td>';

	$html .= '</tr>';

	$html .= '<tr>';

	if ( get_post_meta( get_the_ID(), 'vozrast', 1 ) ) {
		$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_profle.png" alt="Возраст" width="25" height="25" /> ';
		$html .= get_post_meta( get_the_ID(), 'vozrast', 1 ) . '</td>';
	}

	if ( get_post_meta( get_the_ID(), 'time', 1 ) ) {
		$html .= '<td><img class="size-full wp-image-84" src="' . get_option( 'home_url' ) . '/wp-content/uploads/2014/07/mono_clock.png" alt="Время одобрения" width="25" height="25" /> ';
		$html .= get_post_meta( get_the_ID(), 'time', 1 ) . '</td>';
	}

	$html .= '</tr>';

	$html .= '</tbody></table></div>';

	if ( get_post_meta( get_the_ID(), 'text', 1 ) ) {
		$text = get_post_meta( get_the_ID(), 'text', 1 );
	} else {
		$text = 'Оформить займ';
	}

	if ( $post->post_status !== 'private' ) {
		$html .= '<div class="credit_apply_button" onClick="javascript:window.open(\'' . get_post_meta( get_the_ID(), 'link', 1 ) . '\'); yaCounter26138283.reachGoal(\'goToBankButton\'); return true;">' . $text . '</div>';
	}

	$html .= '<div class="clearfix"></div>' . $ratingHTML . '<div class="clearfix"></div></div>';

	return $html;
}

add_shortcode( 'product', 'product_shortcode' );

function remove_comment_fields( $fields ) {
	/*$fields['email'] = '<p class="comment-form-email"><label for="email">' . __( 'Email (Не обязательно)', 'twentythirteen' ) . '</label> ' .
	                   ( $req ? '<span class="required">*</span>' : '' ) .
	                   '<input id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) .
	                   '" size="30"' . $aria_req . ' /></p>';*/
	unset( $fields['email'] );
	unset( $fields['url'] );

	return $fields;
}

add_filter( 'comment_form_default_fields', 'remove_comment_fields' );

function mytheme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	extract( $args, EXTR_SKIP );

	if ( 'div' == $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
	<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
		<?php if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
		} ?>
		<?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
		<?php
		$rating = get_comment_meta( get_comment_ID(), 'crfp-rating', true );
		if ( $rating == '' ) {
			$rating = 0;
		}
		echo '<div class="rating" style="display:inline-block;"><div class="crfp-rating crfp-rating-' . $rating . '">' . $rating . '</div></div>';
		?>
	</div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
		<br />
	<?php endif; ?>

	<div class="comment-meta commentmetadata">
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
			/* translators: 1: date, 2: time */
			printf( __( '%1$s at %2$s' ), get_comment_date(), get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
		?>
	</div>

	<?php comment_text(); ?>

	<div class="reply">
		<?php comment_reply_link( array_merge( $args, array(
					'add_below' => $add_below,
					'depth'     => $depth,
					'max_depth' => $args['max_depth']
				) ) ); ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
		</div>
	<?php endif; ?>
<?php
}

add_filter( 'comments_open', 'my_comments_open', 10, 2 );

function my_comments_open( $open, $post_id ) {

	$post        = get_post( $post_id );
	$post_parent = get_post( $post->post_parent );

	if ( 'page' == $post->post_type && $post_parent->post_parent == 1837 ) {
		$open = true;
	}

	return $open;
}

?>