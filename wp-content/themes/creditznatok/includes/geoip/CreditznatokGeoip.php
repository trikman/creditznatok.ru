<?php

define('COORDINATES_FORMAT', 'WGS84');
define('MAJOR_AXIS', 6378137.0); //meters
define('MINOR_AXIS', 6356752.3142); //meters
define('MAJOR_AXIS_POW_2', pow(MAJOR_AXIS, 2)); //meters
define('MINOR_AXIS_POW_2', pow(MINOR_AXIS, 2)); //meters

class CreditznatokGeoip
{
    function __construct()
    {
        add_action('add_meta_boxes', [$this, 'geoip_init']);
        add_action('save_post', [$this, 'metabox_save_hook']);
    }

    function geoip_init()
    {
        if ( ! CreditznatokConfig::isGeoIpEnable) {
            return;
        }

        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

        if ($post_id) {
            add_meta_box('geoip', 'GeoIP-параметры', [$this, 'geoip_showup'], 'offers', 'side', 'default');
            add_meta_box('geoip', 'GeoIP-параметры', [$this, 'geoip_showup'], 'services', 'side', 'default');
        }
    }

    function geoip_showup($force_meta = null)
    {

        $current_user = wp_get_current_user();

        if ( ! current_user_can('manage_options') && ! in_array('editor_with_geoip', (array)$current_user->roles)) {
            echo "Недостаточно прав";

            return;
        }

        $post = get_post();
        include('cities.php');

        if ( ! is_object($force_meta)) {
            $geo_cities = $force_meta;
        } else {

            $geoip_cities = wp_get_post_terms($post->ID, 'geoip_taxonomy');
            $geo_cities   = array();

            foreach ($geoip_cities as $city_term) {
                $geo_cities[$city_term->name] = $city_term->term_id;
            }
        }
        ?>
        <input class="button button-primary button-large" id="show-hide-all" value="Свернуть/развернуть все" type="button">
        <script>
            jQuery(document).ready(function ($) {
                $('#show-hide-all').click(function () {
                    var currentItem = $('.region-accordion');

                    if (currentItem.hasClass('div-show')){
                        currentItem.removeClass('div-show');
                        currentItem.addClass('div-hide');
                    } else {
                        currentItem.removeClass('div-hide');
                        currentItem.addClass('div-show');
                    }
                });
            });
        </script>
        <br><br>

        <input type="checkbox" class="geoip-check" name="geoip[Показывать во всех регионах]" <?php echo isset($geo_cities['Показывать во всех регионах']) ? 'checked' : ''; ?>>Показывать во всех регионах
        <BR>

        <input type="checkbox" onClick="_check_all(this,'geoip-check');"> Все <BR>
        <?php
        foreach ($cities_by_region as $region => $cities) {

            $region = str_replace('__', 'Москва и СПб', $region);
            $reg    = md5($region);
            ?>
            <input type="checkbox" onClick="_check_all(this,'check-<?php echo $reg; ?>');" id="checkbox-<?php echo $reg; ?>" class="geoip-check">
            <a href='javascript:;' onClick="_show_hide('div-<?php echo $reg; ?>');"><?php echo $region; ?></a> <BR>
            <div style="overflow:hidden;" class="region-accordion div-hide" id="div-<?php echo $reg; ?>">
                <?php

                $checked = 0;
                foreach ($cities as $city => $one) {
                    if ($city) {
                        ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="checkbox" onClick="_check_intermed('<?php echo $reg; ?>');" class="geoip-check check-<?php echo $reg; ?>" name="geoip[<?php echo htmlspecialchars($city); ?>]" <?php if (isset($geo_cities[$city])) {
                            echo 'checked';
                            $checked++;
                        } ?>> <?php echo $city; ?><BR>
                        <?php
                    }
                }

                if ($checked == count($cities)) {
                    $script .= 'jQuery("#checkbox-' . $reg . '").prop("checked", true);';
                } else if ($checked) {
                    $script .= 'jQuery("#checkbox-' . $reg . '").prop("indeterminate", true);';
                }
                ?>
            </div>
            <?php
        }
        ?>
        <style>
            .div-show {
                height: auto;
            }

            .div-hide {
                height: 1px;
            }
        </style>

        <script>
            <?php echo $script;?>
            function _check_all(_this, _class) {

                if (jQuery(_this).prop("checked")) {
                    jQuery('.' + _class).prop('checked', true);

                } else {
                    jQuery('.' + _class).prop('checked', false);
                }
            }

            function _show_hide(_id) {
                if (jQuery('#' + _id).hasClass('div-show')) {
                    jQuery('#' + _id).removeClass('div-show');
                    jQuery('#' + _id).addClass('div-hide');
                } else {
                    jQuery('#' + _id).removeClass('div-hide');
                    jQuery('#' + _id).addClass('div-show');
                }
            }

            function _check_intermed(reg) {
                var checked = 0;
                var all = 0;

                jQuery(".check-" + reg).each(function () {
                    if (jQuery(this).prop("checked")) {
                        checked = checked + 1;
                    }
                    all = all + 1;
                });
                if (checked == 0) {
                    jQuery('#checkbox-' + reg).prop('indeterminate', false);
                    jQuery('#checkbox-' + reg).prop('checked', false);
                } else if (checked < all) {
                    jQuery('#checkbox-' + reg).prop('checked', false);
                    jQuery('#checkbox-' + reg).prop('indeterminate', true);
                } else {
                    jQuery('#checkbox-' + reg).prop('indeterminate', false);
                    jQuery('#checkbox-' + reg).prop('checked', true);
                }
            }
        </script>
        <?php
    }

    function metabox_save_hook()
    {
        $current_user = wp_get_current_user();
        if (in_array('seo', (array)$current_user->roles)) {
            return;
        }

        global $post;
        // Тут ловим сохраненные параметры в $_POST

        if ($post->post_type == 'services' || $post->post_type == 'offers') {

            if (isset($_POST['geoip'])) {
                $post_terms = array();

                foreach ($_POST['geoip'] as $city => $one) {
                    $term = term_exists($city, 'geoip_taxonomy');

                    if ($term) {
                        $term = $term['term_id'];

                    } else {
                        $new_term = wp_insert_term($city, 'geoip_taxonomy');
                        $term     = $new_term['term_id'];
                    }

                    $post_terms[] = (int)$term;
                }

                wp_set_object_terms($post->ID, $post_terms, 'geoip_taxonomy');
            } else {
                wp_set_object_terms($post->ID, null, 'geoip_taxonomy');
            }
        }
    }

    //get arrays with gps coordinates, returns earth terrestrial distance between 2 points
    public static function get_distance_between_2_points($gps_1, $gps_2, $decart = false)
    {

        if ( ! $decart) {
            $true_angle_1 = self::get_true_angle($gps_1);
            $true_angle_2 = self::get_true_angle($gps_2);

            $point_radius_1 = self::get_point_radius($gps_1, $true_angle_1);
            $point_radius_2 = self::get_point_radius($gps_2, $true_angle_2);

            $earth_point_1_x = $point_radius_1 * cos(deg2rad($true_angle_1));
            $earth_point_1_y = $point_radius_1 * sin(deg2rad($true_angle_1));

            $earth_point_2_x = $point_radius_2 * cos(deg2rad($true_angle_2));
            $earth_point_2_y = $point_radius_2 * sin(deg2rad($true_angle_2));

            $x = self::get_distance_between_2_points(array(
                'lat' => $earth_point_1_x,
                'lng' => $earth_point_1_y
            ), array('lat' => $earth_point_2_x, 'lng' => $earth_point_2_y), true);
            $y = pi() * (($earth_point_1_x + $earth_point_2_x) / 360) * ($gps_1['lng'] - $gps_2['lng']);


            return sqrt(pow($x, 2) + pow($y, 2));
        } else {
            return sqrt(pow(($gps_1['lat'] - $gps_2['lat']), 2) + pow(($gps_1['lng'] - $gps_2['lng']), 2));
        }
    }

    //returns degree's decimal measure, getting degree, minute and second
    function get_decimal_degree($deg = 0, $min = 0, $sec = 0)
    {
        return ($deg < 0) ? (-1 * (abs($deg) + (abs($min) / 60) + (abs($sec) / 3600))) : (abs($deg) + (abs($min) / 60) + (abs($sec) / 3600));
    }

    // get point, returns true angle
    function get_true_angle($gps)
    {
        return atan(((MINOR_AXIS_POW_2 / MAJOR_AXIS_POW_2) * tan(deg2rad($gps['lat'])))) * 180 / pi();
    }

    //get point and true angle, returns radius of small circle (radius between meridians)
    static function get_point_radius($gps, $true_angle)
    {
        return (1 / sqrt((pow(cos(deg2rad($true_angle)), 2) / MAJOR_AXIS_POW_2) + (pow(sin(deg2rad($true_angle)),
                        2) / MINOR_AXIS_POW_2))) + $gps['point_elevation'];
    }

    function check_lat($lat)
    {
        if ($lat >= 0 && $lat <= 90) {
            return 'north';
        } else if ($lat >= -90 && $lat <= 0) {
            return 'south';
        }

        return false;
    }

    function check_lng($lng)
    {
        if ($lng >= 0 && $lng <= 180) {
            return 'east';
        } else if ($lng >= -180 && $lng <= 0) {
            return 'west';
        }

        return false;

    }

    public static function get_geo($what = 'loc')
    {
        $ini = [];

        $geoip_options = get_option('geoip_options');
        foreach ($geoip_options['cities'] as $city => $one) {
            if ($city) {
                $ini[($city)] = $city;
            }
        }

        if (isset($_COOKIE['geo'])) {

            // получаем по указанному real_loc
            if (isset($ini[($_COOKIE['geo'])])) {
                return $_COOKIE['geo'];
            } // город есть в списке, просто возвращаем
            else {
                if ($what == 'real_loc') {
                    return $_COOKIE['geo'];
                } // то же самое
                else {
                    /* Возврат ближайшего города */
                    return self::get_closest_city($_COOKIE['geo']);
                }
            }

        } else {
            // получаем по айпи
            $ip  = 0;
            $geo = self::do_geoip($ip, $ini);

            return $geo[$what]; // это для вывода в интерфейсе
        }
    }

    public static function do_geoip($ip, $ini)
    {

        /* Тут пример формата INI */
        /*
        $ini = array(
            'город 1'=>'Город 1',
            'город 2'=>'Город 2',
        );
        */

        require_once("ipgeobase.php");
        $gb = new IPGeoBase();

        $data                    = $gb->getRecord($_SERVER['REMOTE_ADDR']);
        $data['point_elevation'] = 0;

        $c = iconv('windows-1251', 'utf-8', $data['city']);

        if (isset($ini[$c])) {

            $result = array('loc' => $ini[$c], 'real_loc' => $ini[$c]);
        } else {

            // ищем ближайший
            // 1. Подгружаем координаты городов
            $result = array(
                'loc'      => self::get_closest_city($data),
                'real_loc' => iconv('windows-1251', 'utf-8', $data['city'])
            );
        }

        return $result;
    }

    /**
     * Получение ближайшего города по географическому расстоянию
     *
     * @data Либо строковое название города, либо его координаты array('lat'=>123,'lng'=>123,'point_elevation'=>123)
     * @return Строковое имя
     */
    function get_closest_city($data)
    {
        $geo_data_from_file = self::get_geo_data_from_file();

        $all_crd       = $geo_data_from_file['all_crd'];
        $needed_cities = $geo_data_from_file ['needed_cities'];

        if ( ! is_array($data)) {
            $data = $all_crd[($data)];
            if ( ! $data) {
                return null;
            }
        }

        //Вычисляем расстояние от GeoIP-шного до наших и берем минималку
        $dist = array();
        foreach ($needed_cities as $c => $crd) {
            $dist[self::get_distance_between_2_points($crd, $data)] = $c;
        }

        ksort($dist);
        $dist = array_values($dist);

        if (count($dist)) {
            return $dist[0];
        } else {
            return null;
        }

    }

    public static function get_geo_data_from_file()
    {
        $result        = array();
        $needed_cities = array();
        $geoip_options = get_option('geoip_options');

        foreach ($geoip_options['cities'] as $city => $one) {
            if ($city) {
                $needed_cities[($city)] = 1;
            }
        }

        $t = file(__DIR__ . '/cities.txt');

        foreach ($t as $line) {
            $line = explode("\t", $line);
            $c    = (iconv('windows-1251', 'utf-8', $line[1]));
            $crd  = array(
                'lat'             => trim($line[4]),
                'lng'             => trim($line[5]),
                'point_elevation' => 0
            );

            if (isset($needed_cities[$c])) {
                $needed_cities[$c] = $crd;
            }

            $all_crd[$c]        = $crd;
            $result ['all_crd'] = $all_crd;
        }

        $result ['needed_cities'] = $needed_cities;

        return $result;
    }

    public static function geoip_options_cities_sort($goc)
    {
        foreach ($goc as $city => $one) {
            if ($city == 'Москва') {
                $goc[$city] = '_0';
            } else if ($city == 'Санкт-Петербург') {
                $goc[$city] = '_1';
            } else {
                $goc[$city] = $city;
            }
        }
        asort($goc);

        return $goc;
    }

    public static function get_list_of_all_cities()
    {

        $result = array();
        include('cities.php');

        foreach ($cities_by_region as $region => $cities) {
            foreach ($cities as $city => $one) {
                if ($city) {
                    $result[] = $city;
                }
            }
        }
        sort($result);

        return $result;
    }

    public static function get_list_of_all_cities_for_form()
    {

        $all_cities = self::get_list_of_all_cities();

        if (CreditznatokConfig::CurrentCountry == 'Россия') {
            if (($key = array_search('Москва', $all_cities)) !== false) {
                unset($all_cities[$key]);
            }

            if (($key = array_search('Санкт-Петербург', $all_cities)) !== false) {
                unset($all_cities[$key]);
            }
            array_unshift($all_cities, 'Москва', 'Санкт-Петербург');
        }

        return $all_cities;
    }

}

$creditznatok_geoip = new CreditznatokGeoip();