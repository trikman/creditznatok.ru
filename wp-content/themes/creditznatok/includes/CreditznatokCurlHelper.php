<?php

class CreditznatokCurlHelper
{

    const UserAgent = 'Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.2.15 Version/10.00';
    const HttpOk = 200;
    const HttpPartialInfo = 203;
    const HttpInternalError = 500;

    /**
     * Curl Options
     */
    public static $Options = [
        CURLOPT_RETURNTRANSFER => true,
        //, CURLOPT_VERBOSE      => true,
        CURLOPT_HEADER         => false,
        CURLOPT_TIMEOUT        => 300
    ];

    /**
     * @var string
     */
    protected static $Encode;


    /**
     * Get Url by curl_exec
     *
     * @static
     *
     * @param  string $searchUrl
     * @param  string $post
     *
     * @return mixed|null
     */
    public static function GetUrl($searchUrl, $post = null)
    {

        $result = null;

        $ch = curl_init();

        curl_setopt_array($ch, self::CurlGetOptions($searchUrl));

        //$output = curl_exec( $ch );
        //curl_setopt( $ch, CURLOPT_URL,            $searchUrl  );
        $output = curl_exec($ch);
        curl_close($ch);

        if (empty($output)) {
            return $result;
        }

        return $output;
    }


    /**
     * Running single curl
     * @static
     *
     * @param       $url
     * @param       $data
     * @param array $options
     *
     * @return bool|mixed
     * @throws CurlException
     */
    public static function SingleCurl($url, $data = null, $options = array())
    {
        $refugee = curl_init();
        if (is_array($options)) {
            $options += self::$Options;
        } else {
            $options = self::$Options;
        }

        if ( ! empty($data)) {
            if ( ! empty($options[CURLOPT_POST])) {
                $options[CURLOPT_POSTFIELDS] = http_build_query($data);
            } else {
                $url .= '?' . http_build_query($data);
            }
        }

        $options[CURLOPT_URL]        = $url;
        $options[CURLOPT_HTTPHEADER] = array('Expect:');

        curl_setopt_array($refugee, $options);
        $output = curl_exec($refugee);
        $info   = curl_getinfo($refugee);

        curl_close($refugee);
        if ($info['http_code'] == self::HttpOk) {
            return $output;
        } else {
            return false;
            //throw new CurlException(CurlException::CurlRequestFail, $url, $info);
        }
    }


    /**
     * Get Urls by curl_multi_exec
     *
     * @static
     *
     * @param  array  $urls
     * @param  string $post
     *
     * @return array
     */
    public static function GetUrls($urls, $post = null)
    {

        $curl   = array();
        $result = array();
        $mh     = curl_multi_init();

        foreach ($urls as $id => $url) {
            $curl[$id] = curl_init();
            curl_setopt_array($curl[$id], self::CurlGetOptions($url));
            curl_multi_add_handle($mh, $curl[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        foreach ($curl as $id => $c) {
            $result[$id] = curl_multi_getcontent($c);
            curl_multi_remove_handle($mh, $c);
        }

        curl_multi_close($mh);

        return $result;
    }


    /**
     * Curl get options array
     *
     * @static
     *
     * @param  string $url
     * @param  string $post
     *
     * @return array
     */
    protected static function CurlGetOptions($url, $post = null)
    {

        $file = Site::GetRealPath("temp://parse-cookies");

        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT      => self::UserAgent,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_COOKIEJAR      => $file,
            CURLOPT_TIMEOUT        => 10
        ];

        if ( ! empty($post)) {
            $options[CURLOPT_POST]       = true;
            $options[CURLOPT_POSTFIELDS] = $post;

        }

        return $options;
    }


    /**
     * Get Page Encoding
     *
     * @static
     *
     * @param  string $content
     *
     * @return bool|string
     */
    public static function GetPageEncode($content)
    {

        if (empty($content)) {
            return false;
        }

        if ( ! empty(self::$Encode)) {
            return self::$Encode;
        }

        $cp1251_flag = preg_match("#<meta[^<>]*charset=['\"]?(windows-1251|cp-1251|cp1251)['\"]?[^<>]*>#sim", $content);
        $koi8r_flag  = preg_match("#<meta[^<>]*charset=['\"]?(koi8r|koi8-r)['\"]?[^<>]*>#sim", $content);
        $utf8_flag   = preg_match("#<meta[^<>]*charset=['\"]?(utf-8|utf8)['\"]?[^<>]*>#sim", $content);

        switch ($cp1251_flag & $koi8r_flag & $utf8_flag) {
            case 2:
                self::$Encode = 'CP1251';
                break;
            case 4:
                self::$Encode = 'KOI8-R';
                break;
            case 6:
                ;
                break;
            case 0:
                preg_match("#<meta[^<>]*charset=['\"]?([^<>\"'\s]+)['\"]?[^<>]*>#sim", $content, $m);
                self::$Encode = empty($m[0]) || empty($m[1]) ? null : $m[1];
                break;
        }

        return self::$Encode;
    }
}