<?php

class CreditznatokShortcodes
{

    function __construct()
    {
        //add_shortcode('products', [$this, 'products_shortcode']);
        //add_shortcode('product', [$this, 'product_shortcode']);
        add_shortcode('date', [$this, 'date_shortcode']);
    }

    public static function date_shortcode($atts)
    {
        $format = isset($atts['format']) ? $atts['format'] : null;

        return Creditznatok::format_rus_date('', $format);
    }

    public static function display_time($post_id, $parent_post_id)
    {

        //ID постов типа услуг
        //41    кредитные карты
        //132   кредиты
        //135   микрозаймы
        //137   вклады
        //139   ипотека
        //1265  кредиты для бизнеса
        //5284  автокредиты
        //6275  дебетовые карты

        $plural = false;
        $result = false;

        switch ($parent_post_id) {
            case ($parent_post_id == 41 || $parent_post_id == 132 || $parent_post_id == 1265 || $parent_post_id == 139 || $parent_post_id == 5284):
                $plural    = array('день', 'дня', 'дней');
                $plural_to = array('дня', 'дней', 'дней');
                break;
            case ($parent_post_id == 135):
                $plural    = array('минута', 'минуты', 'минут');
                $plural_to = array('минуты', 'минут', 'минут');
                break;
        }

        if ( ! $plural) {
            return $result;
        }

        $time_min    = get_post_meta($post_id, 'time_min', true);
        $time_single = get_post_meta($post_id, 'time_single', true);

        if ($time_single === 'true') {
            $result = $time_min . ' ' . self::plural_form($time_min, $plural);
        } else {
            $time_max = get_post_meta($post_id, 'time_max', true);

            if ($time_min === '0' && $time_max !== '999999999') {
                $result = 'До ' . $time_max . ' ' . self::plural_form($time_max, $plural_to);;
            } else if ($time_min === '0' && $time_max === '999999999') {
                $result = false;// 'От ' . 1 . ' ' . $this->plural_form( 1, $plural_to );
            } else if ($time_max === '999999999') {
                $result = 'От ' . $time_min . ' ' . self::plural_form($time_min, $plural_to);
            } else {
                $result = $time_min . ' &mdash; ' . $time_max . ' ' . self::plural_form($time_max, $plural);;
            }
        }

        return $result;

    }

    public static function plural_form($number, $values)
    {

        $plural = $number % 10 == 1 && $number % 100 != 11 ? $values[0] : ($number % 10 >= 2 && $number % 10 <= 4 && ($number % 100 < 10 || $number % 100 >= 20) ? $values[1] : $values[2]);

        return $plural;
    }

    public static function display_age($post_id)
    {

        $plural = array('год', 'года', 'лет');

        $vozrast_min    = get_post_meta($post_id, 'vozrast_min', true);
        $vozrast_single = get_post_meta($post_id, 'vozrast_single', true);

        if ($vozrast_single === 'true') {
            echo $vozrast_min . ' ' . self::plural_form($vozrast_min, $plural);
        } else {
            $vozrast_max = get_post_meta($post_id, 'vozrast_max', true);

            if ($vozrast_min === '0' && $vozrast_max !== '999999999') {
                echo 'До ' . $vozrast_max . ' ' . self::plural_form($vozrast_max, $plural);;
            } else if ($vozrast_min === '0' && $vozrast_max !== '999999999') {
                //echo 'От ' . $vozrast_min . $this->plural_form( $vozrast_min, $plural );
            } else if ($vozrast_max === '999999999') {
                echo 'От ' . $vozrast_min . ' ' . self::plural_form($vozrast_min, $plural);
            } else {
                echo $vozrast_min . ' &mdash; ' . $vozrast_max . ' ' . self::plural_form($vozrast_max, $plural);;
            }
        }
    }

    public static function display_srok($post_id, $parent_post_id)
    {

        //ID постов типа услуг
        //41    кредитные карты
        //132   кредиты
        //135   микрозаймы
        //137   вклады
        //139   ипотека
        //1265  кредиты для бизнеса
        //5284  автокредиты
        //6275  дебетовые карты

        $plural = false;
        $result = false;

        switch ($parent_post_id) {
            case ($parent_post_id == 41 || $parent_post_id == 135 || $parent_post_id == 137):
                $plural    = array('день', 'дня', 'дней');
                $plural_to = array('дня', 'дней', 'дней');
                break;
            case ($parent_post_id == 132 || $parent_post_id == 5284 || $parent_post_id == 1265):
                //$plural    = array( 'месяц', 'месяца', 'месяцев' );
                //$plural_to = array( 'месяца', 'месяцев', 'месяцев' );
                $plural    = array('мес.', 'мес.', 'мес.');
                $plural_to = array('мес.', 'мес.', 'мес.');

                break;
            case ($parent_post_id == 139):
                $plural    = array('год', 'года', 'лет');
                $plural_to = array('года', 'лет', 'лет');
                break;
        }

        if ( ! $plural) {
            return;
        }

        $srok_min    = get_post_meta($post_id, 'srok_min', true);
        $srok_single = get_post_meta($post_id, 'srok_single', true);

        if ($srok_single === 'true') {
            $result = $srok_min . ' ' . self::plural_form($srok_min, $plural);
        } else {
            $srok_max = get_post_meta($post_id, 'srok_max', true);

            if ($srok_min === '0' && $srok_max !== '999999999') {
                $result = 'До ' . $srok_max . ' ' . self::plural_form($srok_max, $plural_to);
            } else if ($srok_min === '0' && $srok_max === '999999999') {
                $result = false;//'От ' . $srok_min . $this->plural_form( $srok_min, $plural_to );
            } else if ($srok_max === '999999999') {
                $result = 'От ' . $srok_min . ' ' . self::plural_form($srok_min, $plural_to);
            } else {
                $result = $srok_min . ' &mdash; ' . $srok_max . ' ' . self::plural_form($srok_max, $plural);;
            }
        }

        return $result;

    }

    public static function display_currency($currency)
    {
        switch ($currency) {
            case CreditznatokConfig::Currency:
                echo CreditznatokConfig::CurrencyMultiply;
                break;
            case 'usd':
                echo 'Доллары';
                break;
            case 'eur':
                echo 'Евро';
                break;
        }
    }

    public static function display_percents($post_id, $parent_post_id)
    {

        //ID постов типа услуг
        //41    кредитные карты
        //132   кредиты
        //135   микрозаймы
        //137   вклады
        //139   ипотека
        //1265  кредиты для бизнеса
        //5284  автокредиты
        //6275  дебетовые карты

        $display_percents = ' %';

        switch ($parent_post_id) {
            case ($parent_post_id == 135):
                $display_percents = ' % в день';
                break;
        }

        $percents_min    = get_post_meta($post_id, 'percents_min', true);
        $percents_single = get_post_meta($post_id, 'percents_single', true);

        if ($percents_single === 'true') {
            echo $percents_min . $display_percents;
        } else {
            $percents_max = get_post_meta($post_id, 'percents_max', true);

            if ($percents_min === '0' && $percents_max !== '999999999') {
                echo 'До ' . $percents_max . $display_percents;
            } else if ($percents_min === '0' && $percents_max === '999999999') {
                echo '';//'От ' . 1 . $display_percents;
            } else if ($percents_max === '999999999') {
                echo 'От ' . $percents_min . $display_percents;
            } else {
                echo $percents_min . ' &mdash; ' . $percents_max . $display_percents;
            }
        }

    }

    public static function display_razmer_zayma($post_id)
    {
        $display_currency = ' ' . CreditznatokConfig::CurrencyName;
        $currency         = get_post_meta($post_id, 'currency_new', true);

        if ($currency) {
            switch ($currency) {
                case CreditznatokConfig::Currency:
                    $display_currency = ' ' . CreditznatokConfig::CurrencyName;
                    break;
                case 'usd':
                    $display_currency = ' $';
                    break;
                case 'eur':
                    $display_currency = ' €';
                    break;
            }
        }

        $razmer_zayma_min    = get_post_meta($post_id, 'razmer_zayma_min', true);
        $razmer_zayma_single = get_post_meta($post_id, 'razmer_zayma_single', true);

        if ($razmer_zayma_min == 'Неограничен') {
            echo 'Неограничен';

            return;
        }

        if ($razmer_zayma_single === 'true') {
            $razmer_zayma_min = '<span class="no-break-word">' . number_format($razmer_zayma_min, 0, '',
                    ' ') . '</span>';
            echo $razmer_zayma_min . $display_currency;
        } else {
            $razmer_zayma_max = get_post_meta($post_id, 'razmer_zayma_max', true);

            if ($razmer_zayma_min === '0' && $razmer_zayma_max !== '999999999') {

                $razmer_zayma_max = '<span class="no-break-word">' . number_format($razmer_zayma_max, 0, '',
                        ' ') . '</span>';
                echo 'До ' . $razmer_zayma_max . $display_currency;

            } else if ($razmer_zayma_min === '0' && $razmer_zayma_max === '999999999') {

                echo 'От ' . 1 . $display_currency;

            } else if ($razmer_zayma_max === '999999999') {

                $razmer_zayma_min = '<span class="no-break-word">' . number_format($razmer_zayma_min, 0, '',
                        ' ') . '</span>';
                echo 'От ' . $razmer_zayma_min . $display_currency;

            } else {
                $razmer_zayma_min = '<span class="no-break-word">' . number_format($razmer_zayma_min, 0, '',
                        ' ') . '</span>';
                $razmer_zayma_max = '<span class="no-break-word">' . number_format($razmer_zayma_max, 0, '',
                        ' ') . '</span>';
                echo $razmer_zayma_min . ' &mdash; ' . $razmer_zayma_max . $display_currency;
            }
        }

    }

    //Шорткод продуктов, для разных типов постов, выводит списком продукты подходящие под запрос.
    //TODO: ОПТИМИЗАЦИЯ ВЫВОДА ПАРАМЕТРОВ, ОПТИМИЗАЦИЯ ОБРАБОТКИ ПАРАМЕТРОВ ЗАПРОСА WP_QUERY
    public static function products_shortcode()
    {

        $posts = CreditznatokSearchResults::get_filtered_search_results();

        ob_start();
        if (count($posts)) { ?>
            <div class="products cz-block-white">
                <?php Creditznatok::print_h1(); ?>

                <?php foreach ($posts as $post_id) {
                    ?>

                    <div class="clearfix"></div>

                    <?php include(locate_template('template-parts/single-product-info.php')); ?>
                    <?php
                } ?>
            </div>
            <? wp_reset_postdata();
        } else { ?>
            <div class="not-found cz-block-white">
                К сожалению, по Вашему запросу ничего не найдено.<br>
                Попробуйте изменить параметры запроса.
            </div>
        <?php }
        $result = ob_get_clean();

        return $result;
    }

    public static function product_shortcode()
    {
        $post    = get_post();
        $post_id = $post->ID;
        ob_start();
        ?>
        <div class="clearfix"></div>
        <div class="cz-block-white">
            <?php include(locate_template('template-parts/single-product-info.php')); ?>
            <?php Creditznatok::display_tabs(); ?>
        </div>
        <?php
        $result = ob_get_clean();

        return $result;
    }

}

$creditznatok_shortcode = new CreditznatokShortcodes();