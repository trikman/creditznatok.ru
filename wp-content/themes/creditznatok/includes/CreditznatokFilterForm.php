<?php
include_once('CreditznatokFilterFormUtility.php');

class CreditznatokFilterForm
{

    /**
     * Функция вывода формы по шаблону
     */
    public static function get_filter_form()
    {
        $post                = get_post();
        $parent_service_id   = Creditznatok::get_parent_service_type_id();
        $parent_service_type = Creditznatok::get_parent_service_type();
        $form_fields         = CreditznatokFilterFormUtility::$form_fields;
        $form_tpl            = self::get_filter_form_by_type();

        if ( ! isset ($form_fields[$parent_service_type])) {
            return;
        }

        $post_type_array = array(
            'seo_groups',
            'seo_groups_child',
            'service_types',
            'services'
        );

        if (in_array($post->post_type, $post_type_array)) {
            $action = '';
        } else {
            $action = get_permalink($parent_service_id);
        }

        ob_start();
        ?>

        <div class="row">
            <div class="col-md-12">
                <form method="POST" class="filters" action="<?php echo $action; ?>">
                    <div class="filters-form">
                        <?php if (isset($form_tpl[$parent_service_type])) { ?>
                            <input type="hidden" name="product" value="<?php echo $parent_service_type; ?>">
                            <?php echo $form_tpl[$parent_service_type](); ?>
                        <?php } ?>
                    </div>
                    <?php /*if ($post->post_type == 'service_types' || $post->post_type == 'services' || $post->post_type == 'seo_groups' || $post->post_type == 'seo_groups_child') { ?>
                        <div class="filters-sorting">
                            <?php self::print_sort(); ?>
                        </div>
                    <?php }*/ ?>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- форматирование полей -->
        <?php

        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }

    /**
     * Шаблоны форм фильтров по типам продуктов
     */
    private static function get_filter_form_by_type()
    {
        $form_tpl = [
            'creditnye_karty' => function () {
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('razmer_zayma_from'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php //self::print_element('proof_of_income'); ?>
                        <?php self::print_element('currency_new'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                    <div class="col-md-4">
                        <?php //self::print_element('proof_of_income'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('cashback'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('free_service'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('without_income_verification'); ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>

                <?php
            },
            'kredity'         => function () {
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('razmer_zayma'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('srok'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('currency_new'); ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('without_garantors'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('without_collateral'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('without_income_verification'); ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>


                <?php
            },
            'mikrozaimy'      => function () { // для продукта "микрозаймы"
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('razmer_zayma'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('srok'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('way2get'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                    <div class="col-md-4">
                        <?php //self::print_element('currency_new'); ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>
                <?php
            },
            'ipoteka'         => function () {
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('credit_sum'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('srok'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('min_deposit'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>
                <?php
            },
            'avtokredity'     => function () {
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('razmer_zayma'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('srok'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('doc'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('min_deposit'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('vehicle_type'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('manufacturer_type'); ?>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('currency_new'); ?>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>
                <?php
            },
            'vklady'          => function () {
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('razmer_zayma'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('banks'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php if (CreditznatokConfig::isGeoIpEnable) {
                            self::print_element('city_filter');
                        } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('currency_new'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('srok'); ?>
                    </div>
                    <div class="col-md-4 form-checkbox">
                        <?php self::print_element('preferential_dissolution'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php self::print_element('capitalization'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('depositing'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php self::print_element('partial_withdrawal'); ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="form-submit-button">
                    <?php self::print_submit(); ?>
                </div>

                <?php self::print_sort(); ?>
                <?php
            },
        ];

        return $form_tpl;
    }

    private static function print_submit()
    { ?>
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                <input type="submit" value="Подобрать" class="btn btn-block cz-button">
            </div>
        </div>
        <?php
    }

    private static function print_element($name)
    {
        $form_fields         = CreditznatokFilterFormUtility::$form_fields;
        $parent_service_type = Creditznatok::get_parent_service_type();
        $element_type        = $form_fields[$parent_service_type][$name]['type'];
        $element_title       = $form_fields[$parent_service_type][$name]['title'];

        if ($element_type == 'checkbox') {
            self::elem_content($name); ?>
            <label for='checkbox_<?php echo $name; ?>'><?php echo $element_title; ?></label>
        <?php } else { ?>
            <label for="<?php echo $name; ?>"><?php echo $element_title; ?></label>
            <div><?php self::elem_content($name); ?></div>
        <?php }
    }

    private static function elem_content($name)
    {
        $form_fields         = CreditznatokFilterFormUtility::$form_fields;
        $parent_service_type = Creditznatok::get_parent_service_type();
        $element_type        = $form_fields[$parent_service_type][$name]['type'];

        if (isset($form_fields[$parent_service_type][$name]['values'])) {
            $values = $form_fields[$parent_service_type][$name]['values'];
        }

        $default_selected = isset($form_fields[$parent_service_type][$name]['selected']) ? $form_fields[$parent_service_type][$name]['selected'] : [];

        $value_from_POST = null;
        $have_post_value = false;

        if (isset ($_POST[$name]) && $_POST[$name]) {
            $have_post_value = true;

            if ( ! is_array($_POST[$name])) {
                $value_from_POST = stripslashes($_POST[$name]);
            } else {
                $value_from_POST = $_POST[$name];
            }
        }

        switch ($element_type) {
            case 'select':
                ?>
                <select class="form-control selectpicker" name="<?php echo $name; ?>" id="select_<?php echo $name; ?>">
                    <option value=""> -</option>
                    <?php foreach ($values as $title => $value) {
                        $products_posts = Creditznatok::check_products_posts_by_meta_exists($name, $value);
                        if ($products_posts) { ?>
                            <option name='<?php echo $title; ?>' value='<?php echo $value; ?>' <?php selected($value_from_POST,
                                $value); ?> >
                                <?php echo $title; ?>
                            </option>
                        <?php }
                    } ?>
                </select>
                <?php break;
            case 'text': ?>
                <div class="form-group">
                    <input type="text" name="<?php echo $name; ?>" class="form-control format-numeral" id="text_<?php echo $name; ?>" value="<?php echo $value_from_POST; ?>">
                </div>
                <?php break;
            case 'checkbox': ?>
                <input type="checkbox" name="<?php echo $name; ?>" id="checkbox_<?php echo $name; ?>" <?php checked($have_post_value); ?>>
                <?php break;
            case 'banks':
                $banks = Creditznatok::get_all_banks(true);
                if ($parent_service_type == 'mikrozaimy') { ?>
                    <div class="cz-multi-select">
                        <select class="form-control selectpicker" name="<?php echo $name; ?>[]" id="banks_<?php echo $name; ?>" multiple="multiple">
                            <?php foreach ($banks as $bank_id) : ?>
                                <?php if ((is_array($value_from_POST) && in_array($bank_id,
                                            $value_from_POST)) || (empty($value_from_POST))
                                ) {
                                    $selected = true;
                                } else {
                                    $selected = false;
                                } ?>
                                <option <?php selected($selected); ?> value='<?php echo $bank_id; ?>'><?php echo get_the_title($bank_id); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php } else { ?>
                    <div class="cz-multi-select">
                        <select class="form-control selectpicker" name="<?php echo $name; ?>[]" id="banks_<?php echo $name; ?>" multiple="multiple">
                            <?php foreach ($banks as $bank) : ?>
                                <?php if ((is_array($value_from_POST) && in_array($bank->ID,
                                            $value_from_POST)) || (empty($value_from_POST))
                                ) {
                                    $selected = true;
                                } else {
                                    $selected = false;
                                } ?>
                                <option <?php selected($selected); ?> value='<?php echo $bank->ID; ?>'><?php echo $bank->post_title; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php
                }
                break;
            case 'cities':
                $all_cities = CreditznatokGeoip::get_list_of_all_cities_for_form();
                ?>
                <div class="cp-city-chosen">
                    <select class="form-control selectpicker" data-live-search="true" name="<?php echo $name; ?>" id="select_<?php echo $name; ?>">
                        <option value=''> -</option>
                        <?php foreach ($all_cities as $city) { ?>
                            <option <?php selected($city,
                                $value_from_POST); ?> value="<?php echo $city; ?>"><?php echo $city; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php break;
            case 'multiselect': ?>
                <div class="cz-multi-select">
                    <select class="form-control selectpicker" name="<?php echo $name; ?>[]" id="select_<?php echo $name; ?>" multiple="multiple">
                        <?php
                        foreach ($values as $name => $value) {
                            $selected = '';
                            if (is_array($value_from_POST) && in_array($value, $value_from_POST)) {
                                $selected = 'selected';
                            } elseif ( ! $value_from_POST && in_array($name, $default_selected)) {
                                $selected = 'selected';
                            } ?>
                            <option <?php echo $selected; ?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php break;
            default:
                break;
        }
    }


    /**
     * Функции для вывода сортировки
     */
    public static function print_sort()
    {
        $post = get_post();

        if ($post->post_type == 'service_types' ||
            $post->post_type == 'services' ||
            $post->post_type == 'seo_groups' ||
            $post->post_type == 'seo_groups_child'
        ) {
            $type               = Creditznatok::get_parent_service_type();
            $sorting_parameters = CreditznatokFilterFormUtility::$sorting_parameters;
            ?>
            <div class="row sort-row">
                <div class="filters-sorting">
                    <div class="col-md-12">
                        <label for="sorting-select">
                            <span class="form-sort-span">Сортировать по:</span>
                        </label>
                        <select class="form-group selectpicker" data-style="btn-default btn-sm" name="sort" id="sorting-select">
                            <?php $sort_from_post = isset($_POST['sort']) ? $_POST['sort'] : ''; ?>
                            <?php foreach ($sorting_parameters[$type] as $name => $values) : ?>
                                <option <?php selected($sort_from_post,
                                    $name); ?> value='<?php echo $name; ?>'><?php echo $values['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <?php
        }
    }

}