<?php
$creditznatokLoadConfigs = new CreditznatokLoadConfigs();

class CreditznatokLoadConfigs
{
    const config_name = 'CreditznatokConfig.php';
    const tabs_utility_name = 'CreditznatokTabsUtility.php';

    public function __construct()
    {
        $host = $_SERVER['HTTP_HOST'];

        switch ($host) {
            case 'creditznatok.ru':
                $path = 'ru';
                break;
            case 'creditznatok.com.ua':
                $path = 'com.ua';
                break;
            case 'creditznatok.kz':
                $path = 'kz';
                break;
            default:
                $path = 'test';
        }

        include_once($path . '/' . self::config_name);
        include_once($path . '/' . self::tabs_utility_name);
    }
}