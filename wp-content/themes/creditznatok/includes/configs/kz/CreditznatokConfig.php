<?php

class CreditznatokConfig
{
    const CurrentCountry = 'Казахстан';

    const Currency = 'kzt';
    const CurrencyName = 'тг.';
    const CurrencyNameShort = 'тг.';
    const CurrencyIn = 'В тенге';
    const CurrencySingle = 'Тенге';
    const CurrencyMultiply = 'Тенге';

    const isGeoIpEnable = false;

    const GoogleTagManagerHead = <<<TXT
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NT733K9');</script>
<!-- End Google Tag Manager -->
TXT;

    const GoogleTagManagerBody = <<<TXT
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NT733K9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
TXT;

    public static $bankRatingMapping = [
        'reyting-aktivy'  => 'Объемы активов',
        'reyting-pribyl'  => 'Объемы прибыли',
        'reyting-kredity' => 'Объемы кредитов',
        'reyting-vklady'  => 'Объемы вкладов',
    ];

    public static $regNumber = 'Регистрационный номер';

    public static $posts_relations = [
        'bank_data'        => [
            'banks',
        ],
        'bank_reviews'     => [
            'banks',
        ],
        'services'         => [
            'service_types',
            'banks',
        ],
        'service_reviews'  => [
            'services',
        ],
        'seo_groups'       => [
            'service_types',
        ],
        'seo_groups_child' => [
            'seo_groups',
            'service_types',
        ],
        'offers'           => [
            'services',
            'service_types',
            'banks',
            'seo_groups',
            'seo_groups_child'
        ],
        'offer_reviews'    => [
            'offers',
        ],
    ];
    public static $service_types_in_url_mapping = [
        'creditnye-karty'      => 41,
        'kredity'              => 132,
        'mikrozaimy'           => 135,
        'vklady'               => 137,
        'debetovye-karty'      => 6275,
        'avtokredity'          => 5284,
        'ipoteka'              => 139,
        'credit-dlya-business' => 1265,
    ];
    public static $service_types_mapping = [
        'debetovye_karty'      => 6275,
        'creditnye_karty'      => 41,
        'kredity'              => 132,
        'mikrozaimy'           => 135,
        'vklady'               => 137,
        'ipoteka'              => 139,
        'credit_dlya_business' => 1265,
        'avtokredity'          => 5284,
    ];
    public static $service_types_for_url_mapping = [
        41   => 'creditnye-karty',
        132  => 'kredity',
        135  => 'mikrozaimy',
        137  => 'vklady',
        6275 => 'debetovye-karty',
        5284 => 'avtokredity',
        139  => 'ipoteka',
        1265 => 'credit-dlya-business',
    ];

    public static $banks_fields_mapping = [
        'Наименование банка'                                     => 'name',
        'Место нахождения банка'                                 => 'location',
        'Сведения о государственной регистрации/перерегистрации' => 'state_registration',
        'Номер лицензии'                                         => 'license_number',
        'Дата выдачи лицензии'                                   => 'date_of_licensing',
    ];

    public static $subscribe_form_mapping = [
        41   => 5,
        132  => 6,
        135  => 7,
        137  => 4,
        6275 => 8,
        5284 => 2,
        139  => 3,
        1265 => 1,
    ];

    public static $banks_data_fields_mapping = [
        'БИК'   => 'bik',
        'БИН'   => 'bin',
        'SWIFT' => 'swift',
    ];

    public static $bankRatingsData = [
        'reyting-aktivy' => [
            'title' => 'Рейтинг банков по объему активов',
            'label' => 'Рейтинг по объему активов'
        ],
        'reyting-pribyl' => [
            'title' => 'Рейтинг банков по объему прибыли',
            'label' => 'Рейтинг по объему прибыли'
        ],
        /*'reyting-kredity' => [
            'title' => 'Рейтинг банков по объему кредитов',
            'label' => 'Рейтинг по объему кредитов'
        ],
        'reyting-vklady' => [
            'title' => 'Рейтинг банков по объему вкладов',
            'label' => 'Рейтинг по объему вкладов'
        ],*/
    ];
}