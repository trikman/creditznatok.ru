<?php

class CreditznatokTabsUtility
{
    public static $mikrozaimy = [
        'Способ получения' => [
            'На карту' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'На банковский счет' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'Через Contact' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'На Webmoney' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'На QIWI' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'На Яндекс Деньги' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'Лидер' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'Золотая Корона' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'Юнистрим' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'На дом' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
            'В офисе' => [
                'type' => 'way2get',
                'name' => 'way2get',
            ],
        ],
        'Процентные ставки' => [
            'Процентные ставки' => [
                'type' => 'table',
                'name' => 'extf_table',
            ],
        ],
    ];

    public static $creditnye_karty = [
        'Дополнительные опции' => [
            'Сashback' => [
                'type' => 'checkbox',
                'name' => 'cashback',
            ],
            'Бесплатное обслуживание' => [
                'type' => 'checkbox',
                'name' => 'free_service',
            ],
        ],
        'Обслуживание карты' => [
            'Тип карты' => [
                'type' => 'string',
                'name' => 'card_type',
            ],
            'Технологические особенности' => [
                'type' => 'string',
                'name' => 'tech_features',
            ],
            'Использование собственных средств' => [
                'type' => 'checkbox',
                'name' => 'use_of_own_funds',
            ],
            'Начисление процентов на остаток средств на счете' => [
                'type' => 'checkbox',
                'name' => 'calc_interest_on_balance',
            ],
            'Выпуск и обслуживание' => [
                'type' => 'string',
                'name' => 'issue_and_maintenance',
            ],
            'Снятие наличных в банкоматах банка' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_in_atm',
            ],
            'Снятие наличных в ПВН банка' => [
                'type' => 'string',
                'name' => 'withdrawal_cash_pvn',
            ],
            'Снятие наличных в банкоматах других банков' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_other_atm',
            ],
            'Снятие наличных в ПВН других банков' => [
                'type' => 'string',
                'name' => 'withdrawal_cash_other_pvn',
            ],
            'Лимиты по операциям' => [
                'type' => 'string',
                'name' => 'limits',
            ],
            'Дополнительная информация' => [
                'type' => 'string',
                'name' => 'other_info',
            ],
        ],
        'Условия кредитования' => [
            'Размер кредитного лимита' => [
                'type' => 'string',
                'name' => 'credit_limits',
            ],
            'Льготный период' => [
                'type' => 'string',
                'name' => 'grace_period',
            ],
            'Проценты в течение льготного периода' => [
                'type' => 'string',
                'name' => 'interest_grace_period',
            ],
            'Проценты за кредит' => [
                'type' => 'string',
                'name' => 'interest_for_credit',
            ],
            'Погашение кредита' => [
                'type' => 'string',
                'name' => 'loan_repayment',
            ],
        ],
        'Требования и документы' => [
            'Возраст' => [
                'type' => 'string',
                'name' => 'age',
            ],
            'Подтверждение дохода' => [
                'type' => 'string',
                'name' => 'proof_of_income',
            ],
            'Регистрация' => [
                'type' => 'string',
                'name' => 'registration',
            ],
            'Стаж работы на последнем месте' => [
                'type' => 'string',
                'name' => 'experience',
            ],
            'Общий стаж работы на последнем месте' => [
                'type' => 'string',
                'name' => 'all_experience',
            ],
        ],
    ];

    public static $kredity = [
        'Условия кредита' => [
            'Без поручителей' => [
                'type' => 'checkbox',
                'name' => 'without_garantors',
            ],
            'Без залога' => [
                'type' => 'checkbox',
                'name' => 'without_collateral',
            ],
            'Сумма' => [
                'type' => 'string',
                'name' => 'sum',
            ],
            'Валюта' => [
                'type' => 'string',
                'name' => 'currency',
            ],
            'Срок' => [
                'type' => 'string',
                'name' => 'time',
            ],
            'Ставка' => [
                'type' => 'table',
                'name' => 'extf_table',
            ],
            'Принятие решения' => [
                'type' => 'string',
                'name' => 'action'
            ],
            'Срок действия решения' => [
                'type' => 'string',
                'name' => 'term_of_decision',
            ],
            'Погашение кредита' => [
                'type' => 'string',
                'name' => 'repayment',
            ],
            'Досрочное погашение' => [
                'type' => 'string',
                'name' => 'early_repayment',
            ],
            'Комиссия за выдачу кредита' => [
                'type' => 'string',
                'name' => 'commission',
            ],
            'Пеня за возникновение просроченной задолженности' => [
                'type' => 'string',
                'name' => 'fine',
            ],
            'Обеспечение' => [
                'type' => 'string',
                'name' => 'provide',
            ],
            'Способ выдачи' => [
                'type' => 'string',
                'name' => 'method_of_issue',
            ],
            'Дополнительные условия' => [
                'type' => 'string',
                'name' => 'additional_conditions',
            ],
        ],

        'Требования к заемщику' => [
            'Возраст' => [
                'type' => 'string',
                'name' => 'age',
            ],
            'Возраст для определенной суммы' => [
                'type' => 'string',
                'name' => 'age_for_sum',
            ],
            'Гражданство РФ' => [
                'type' => 'checkbox',
                'name' => 'russian_citizenship',
            ],
            'Непрерывный стаж на последнем месте работы' => [
                'type' => 'string',
                'name' => 'uninterrupted_length_of_service',
            ],
            'Непрерывный стаж на последнем месте работы для определенной суммы' => [
                'type' => 'string',
                'name' => 'uninterrupted_length_of_service_for_sum',
            ],
            'Отсутствие просроченной задолженности по кредитам' => [
                'type' => 'checkbox',
                'name' => 'without_arrears',
            ],
            'Дополнительные условия' => [
                'type' => 'string',
                'name' => 'additional_conditions',
            ],
        ],

        'Пакет документов' => [
            'Паспорт РФ' => [
                'type' => 'checkbox',
                'name' => 'passport',
            ],
            'Пенсионное удостоверение' => [
                'type' => 'checkbox',
                'name' => 'pension_certificate',
            ],
            'Полис медицинского страхования' => [
                'type' => 'checkbox',
                'name' => 'health_insurance',
            ],
            'Водительское удостоверение' => [
                'type' => 'checkbox',
                'name' => 'driving_license',
            ],
            'Военный билет' => [
                'type' => 'checkbox',
                'name' => 'military_id',
            ],
            'Военное удостоверение' => [
                'type' => 'checkbox',
                'name' => 'military_license',
            ],
            'Страховое пенсионное свидетельство' => [
                'type' => 'checkbox',
                'name' => 'pension_insurance_certificate',
            ],
            'Заграничный паспорт' => [
                'type' => 'checkbox',
                'name' => 'international_passport',
            ],
            'Для суммы от ХХХ ХХХ ' . CreditznatokConfig::CurrencyName => [
                'type' => 'select',
                'name' => 'sum_from_xxx_xxx',
                'value' => [
                    'Справка 2 НДФЛ' => 'reference_2_ndfl',
                    'Справка по форме банка' => 'reference_bank_form',
                ],
            ],
            'Для суммы от X ХХХ ХХХ ' . CreditznatokConfig::CurrencyName => [
                'type' => 'select',
                'name' => 'sum_from_xxx_xxx_xxx',
                'value' => [
                    'Справка 2 НДФЛ' => 'reference_2_ndfl',
                    'Справка по форме банка' => 'reference_bank_form',
                ],
            ]
        ],

        'Способы погашения' => [
            'В отделении банка' => [
                'type' => 'checkbox',
                'name' => 'bank_office',
            ],
            'Через банкомат' => [
                'type' => 'checkbox',
                'name' => 'atm',
            ],
            'В интернет-банке' => [
                'type' => 'checkbox',
                'name' => 'internet_bank',
            ],
            'В любом банке безналичным переводом' => [
                'type' => 'checkbox',
                'name' => 'wire_transfer',
            ],
            'В терминале QIWI' => [
                'type' => 'checkbox',
                'name' => 'qiwi_terminal',
            ],
            'Отделения ФГУП $quot;Почта России$quot;' => [
                'type' => 'checkbox',
                'name' => 'office_russian_post',
            ],
        ],
    ];

    public static $vklady = [
        'Условия вклада' => [
            'Валюта' => [
                'type' => 'string',
                'name' => 'currency',
            ],
            'Сумма' => [
                'type' => 'string',
                'name' => 'sum',
            ],
            'Срок' => [
                'type' => 'string',
                'name' => 'time',
            ],
            'Выплата процентов' => [
                'type' => 'string',
                'name' => 'interest_payment',
            ],
            'Капитализация' => [
                'type' => 'checkbox',
                'name' => 'capitalization',
            ],
            'Автоматическая пролонгация' => [
                'type' => 'checkbox',
                'name' => 'automatic_prolongation',
            ],
            'Пополнение' => [
                'type' => 'checkbox',
                'name' => 'depositing',
            ],
            'Частичное снятие' => [
                'type' => 'checkbox',
                'name' => 'partial_withdrawal',
            ],
            'Досрочное расторжение' => [
                'type' => 'string',
                'name' => 'early_cancellation',
            ],
            'Частичное досрочное снятие' => [
                'type' => 'string',
                'name' => 'partial_early_removal',
            ],
            'Льготное расторжение' => [
                'type' => 'checkbox',
                'name' => 'preferential_dissolution',
            ],
            'Дополнительные условия' => [
                'type' => 'string',
                'name' => 'additional_conditions',
            ],
            'Процентные ставки' => [
                'type' => 'table',
                'name' => 'extf_table',
            ],
        ],

    ];

    public static $ipoteka = [
        'Условия кредита' => [
            'Сумма' => [
                'type' => 'string',
                'name' => 'credit_sum',
            ],
            'Первоначальный взнос' => [
                'type' => 'string',
                'name' => 'initial_contribution',
            ],
            'Валюта' => [
                'type' => 'string',
                'name' => 'currency',
            ],
            'Срок' => [
                'type' => 'string',
                'name' => 'time',
            ],
            'Ставка' => [
                'type' => 'string',
                'name' => 'rate',
            ],
            'Тип жилья' => [
                'type' => 'string',
                'name' => 'housing_type',
            ],
            'Только аккредитованные объекты' => [
                'type' => 'checkbox',
                'name' => 'objects_only_accredited',
            ],
            'Принятие решения' => [
                'type' => 'string',
                'name' => 'decision_making',
            ],
            'Срок действия решения' => [
                'type' => 'string',
                'name' => 'period_validity_decision',
            ],
            'Погашение кредита' => [
                'type' => 'string',
                'name' => 'credit_repayment',
            ],
            'Досрочное погашение' => [
                'type' => 'string',
                'name' => 'early_repayment',
            ],
            'Комиссия за выдачу кредита' => [
                'type' => 'string',
                'name' => 'issuance_commission',
            ],
            'Пеня за возникновение просроченной задолженности' => [
                'type' => 'string',
                'name' => 'arrears_penalty',
            ],
            'Обеспечение' => [
                'type' => 'string',
                'name' => 'providing',
            ],
            'Дополнительные условия' => [
                'type' => 'string',
                'name' => 'additional_conditions',
            ],
        ],

        'Требования к заемщику' => [
            'Возраст' => [
                'type' => 'string',
                'name' => 'age',
            ],
            'Гражданство РФ' => [
                'type' => 'checkbox',
                'name' => 'russian_citizenship',
            ],
            'Регистрация' => [
                'type' => 'string',
                'name' => 'registration',
            ],
            'Непрерывный стаж на последнем месте работы' => [
                'type' => 'string',
                'name' => 'uninterrupted_length_of_service',
            ],
            'Общий стаж' => [
                'type' => 'string',
                'name' => 'general_experience',
            ],
            'Отсутствие просроченной задолженности по кредитам' => [
                'type' => 'checkbox',
                'name' => 'without_arrears',
            ],
            'Дополнительные условия' => [
                'type' => 'string',
                'name' => 'additional_conditions',
            ],
            'ИП или владелец бизнеса' => [
                'type' => 'checkbox',
                'name' => 'business_owner',
            ],
        ],

        'Документы по заемщику' => [
            'Заявление-анкета' => [
                'type' => 'checkbox',
                'name' => 'application_questionnaire',
            ],
            'Паспорт РФ' => [
                'type' => 'checkbox',
                'name' => 'russian_passport',
            ],
            'Военный билет' => [
                'type' => 'checkbox',
                'name' => 'military_id',
            ],
            'Свидетельство ИНН' => [
                'type' => 'string',
                'name' => 'inn_certificate',
            ],
            'Брачный договор' => [
                'type' => 'string',
                'name' => 'marriage contract',
            ],
            'Документы об образовании' => [
                'type' => 'string',
                'name' => 'education_documents',
            ],
            'Копия трудовой книжки' => [
                'type' => 'checkbox',
                'name' => 'service_record_copy',
            ],
            'Справка 2 НДФЛ' => [
                'type' => 'checkbox',
                'name' => 'reference_2_ndfl',
            ],
            'Справка по форме банка' => [
                'type' => 'checkbox',
                'name' => 'reference_bank_form',
            ],
            'Документы подтверждающие регулярные доходы' => [
                'type' => 'string',
                'name' => 'regular_income_documents',
            ],
            'Документы подтверждающие наличие недвижимости' => [
                'type' => 'string',
                'name' => 'real_estate_documents',
            ],
            'Документы подтверждающие наличие ценных бумаг' => [
                'type' => 'string',
                'name' => 'securities_documents',
            ],
            'Выписки по текущим банковским счетам' => [
                'type' => 'string',
                'name' => 'bank_accounts_statements',
            ],
        ],

        'Документы на жилое помещение' => [
            'Правоустанавливающие документы на Жилое помещение' => [
                'type' => 'checkbox',
                'name' => 'premises_documents',
            ],
            'Выписка из технического/кадастрового паспорта' => [
                'type' => 'checkbox',
                'name' => 'cadastral_data',
            ],
            'Выписка из Домовой книги' => [
                'type' => 'checkbox',
                'name' => 'house_register',
            ],
            'Копии паспортов (свидетельств о рождении] продавцов' => [
                'type' => 'checkbox',
                'name' => 'sellers_passports_copies',
            ],
            'Согласие супруга правообладателя Жилого помещения на совершение сделки' => [
                'type' => 'checkbox',
                'name' => 'owner_spouse_consent',
            ],
            'Справка об отсутствии задолженности по коммунальным платежам' => [
                'type' => 'checkbox',
                'name' => 'without_debt_on_utility_payments',
            ],
            'Выписка из Единого государственного реестра' => [
                'type' => 'checkbox',
                'name' => 'unified_state_register',
            ],
            'Оригинал отчета об оценке Жилого помещения' => [
                'type' => 'checkbox',
                'name' => 'premises_assessment',
            ],
        ],

        'Способы погашения' => [
            'В отделении банка' => [
                'type' => 'checkbox',
                'name' => 'bank_office',
            ],
            'Через банкомат' => [
                'type' => 'checkbox',
                'name' => 'atm',
            ],
            'В интернет-банке' => [
                'type' => 'checkbox',
                'name' => 'internet_bank',
            ],
            'В любом банке безналичным переводом' => [
                'type' => 'checkbox',
                'name' => 'wire_transfer',
            ],
            'В терминале QIWI' => [
                'type' => 'checkbox',
                'name' => 'qiwi_terminal',
            ],
            'Отделения ФГУП "Почта России"' => [
                'type' => 'checkbox',
                'name' => 'office_russian_post',
            ],
        ],

    ];

    public static $avtokredity = [
        'Условия кредита' => [
            'Продавец' => [
                'type' => 'string',
                'name' => 'seller',
            ],
            'Вид транспортного средства' => [
                'type' => 'string',
                'name' => 'kind_of_vehicle',
            ],
            'Тип транспортного средства' => [
                'type'  => 'string',
                'name'  => 'type_of_vehicle',
            ],
            'Возраст транспортного средства' => [
                'type' => 'string',
                'name' => 'age_of_vehicle',
            ],
            'Срок кредита' => [
                'type' => 'string',
                'name' => 'credit_term',
            ],
            'Валюта' => [
                'type'  => 'string',
                'name'  => 'currency',
                'class' => 'currency-text'
            ],
            'Сумма кредита' => [
                'type' => 'string',
                'name' => 'amount_of_credit',
            ],
            'Минимальный первоначальный взнос' => [
                'type' => 'string',
                'name' => 'initial_deposit',
            ],
            'Cтавка по кредиту' => [
                'type' => 'string',
                'name' => 'rate_on_loan',
            ],
            'Комиссии' => [
                'type' => 'string',
                'name' => 'commission',
            ],
            'Мораторий на досрочное погашение' => [
                'type' => 'string',
                'name' => 'prepayment',
            ],
            'Условия досрочного погашения' => [
                'type' => 'string',
                'name' => 'terms_of_early_repayment',
            ],
            'Штраф за досрочное погашение' => [
                'type' => 'string',
                'name' => 'penalty_for_early_repayment',
            ],
            'Необходимость страхования' => [
                'type' => 'string',
                'name' => 'need_for_insurance',
            ],
            'Возраст заёмщика' => [
                'type' => 'string',
                'name' => 'borrower_age',
            ],
            'Подтверждение дохода' => [
                'type' => 'string',
                'name' => 'proof_of_income',
            ],
            'Регистрация по месту получения кредита' => [
                'type' => 'string',
                'name' => 'place_receipt_credit',
            ],
            'Стаж работы на последнем месте' => [
                'type' => 'string',
                'name' => 'work_experience',
            ],
            'Общий стаж работы' => [
                'type' => 'string',
                'name' => 'total_work_experience',
            ],
            'Дополнительная информация' => [
                'type' => 'string',
                'name' => 'info',
            ],
        ],
    ];

    public static $credit_dlya_business = [
        'Условия кредита' => [
            'Цель кредита' => [
                'type' => 'string',
                'name' => 'purpose_of_loan',
            ],
            'Срок кредита' => [
                'type' => 'string',
                'name' => 'credit_term',
            ],
            'Валюта' => [
                'type'  => 'string',
                'name'  => 'currency',
                'class' => 'currency-text'
            ],
            'Сумма кредита' => [
                'type' => 'string',
                'name' => 'amount_of_credit',
            ],
            'Cтавка по кредиту' => [
                'type' => 'string',
                'name' => 'rate_on_loan',
            ],
            'Комиссии' => [
                'type' => 'string',
                'name' => 'commission',
            ],
            'Пени за просрочку платежа по кредиту' => [
                'type' => 'string',
                'name' => 'penalties',
            ],
            'Срок рассмотрения заявки' => [
                'type' => 'string',
                'name' => 'term_for_consideration',
            ],
            'Обеспечение' => [
                'type' => 'string',
                'name' => 'security',
            ],
            'Форма выдачи' => [
                'type' => 'string',
                'name' => 'form_of_issue',
            ],
            'Режим погашения' => [
                'type' => 'string',
                'name' => 'redemption_mode',
            ],
            'Условия досрочного погашения' => [
                'type' => 'string',
                'name' => 'early_repayment',
            ],
            'Срок существования бизнеса' => [
                'type' => 'string',
                'name' => 'business_existence',
            ],
            'Требования к заемщику' => [
                'type' => 'string',
                'name' => 'borrower_requirements',
            ],
        ],
    ];
    
    public static $debetovye_karty = [
        'Обслуживание карты' => [
            'Тип карты' => [
                'type' => 'string',
                'name' => 'card_type',
            ],
            'Технологические особенности' => [
                'type' => 'string',
                'name' => 'tech_features',
            ],
            'Cash Back' => [
                'type' => 'checkbox',
                'name' => 'cashback',
            ],
            'Начисление процентов на остаток средств на счете' => [
                'type' => 'string',
                'name' => 'interest_on_balance',
            ],
            'Выпуск и обслуживание' => [
                'type' => 'string',
                'name' => 'issue_and_maintenance',
            ],
            'Снятие наличных в банкоматах банка' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_in_atm',
            ],
            'Снятие наличных в ПВН банка' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_in_pvn',
            ],
            'Снятие наличных в банкоматах других банков' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_in_others_atm',
            ],
            'Снятие наличных в ПВН других банков' => [
                'type' => 'string',
                'name' => 'cash_withdrawal_in_others_pvn',
            ],
            'Лимиты по операциям' => [
                'type' => 'string',
                'name' => 'limits',
            ],   
            'Дополнительная информация' => [
                'type' => 'string',
                'name' => 'other_info',
            ],         
        ],
        'Требования к держателю карты' => [
            'Возраст' => [
                'type' => 'string',
                'name' => 'age',
            ],
            'Регистрация' => [
                'type' => 'string',
                'name' => 'registration',
            ],
        ],
    ];
}