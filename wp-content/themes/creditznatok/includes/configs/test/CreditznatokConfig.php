<?php

class CreditznatokConfig
{
    const isTestSite = true;

    const CurrentCountry = 'Россия';

    const Currency = 'rur';
    const CurrencyName = 'руб.';
    const CurrencyNameShort = 'р.';
    const CurrencyIn = 'В рублях';
    const CurrencySingle = 'Рубль';
    const CurrencyMultiply = 'Рубли';

    const isGeoIpEnable = true;

    const GoogleTagManagerHead = <<<TXT
TXT;

    const GoogleTagManagerBody = <<<TXT
TXT;

    public static $regNumber = 'Регистрационный номер';

    public static $bankRatingMapping = [
        'reyting-aktivy'  => 'Активы',
        'reyting-pribyl'  => 'Прибыль',
        'reyting-kredity' => 'Кредиты',
        'reyting-vklady'  => 'Вклады',
    ];

    public static $posts_relations = [
        'bank_data'        => [
            'banks',
        ],
        'bank_reviews'     => [
            'banks',
        ],
        'services'         => [
            'service_types',
            'banks',
        ],
        'service_reviews'  => [
            'services',
        ],
        'seo_groups'       => [
            'service_types',
        ],
        'seo_groups_child' => [
            'seo_groups',
            'service_types',
        ],
        'offers'           => [
            'services',
            'service_types',
            'banks',
            'seo_groups',
            'seo_groups_child'
        ],
        'offer_reviews'    => [
            'offers',
        ],
    ];

    public static $service_types_in_url_mapping  = [
        'creditnye-karty'      => 41,
        'kredity'              => 132,
        'mikrozaimy'           => 135,
        'vklady'               => 137,
        'debetovye-karty'      => 6275,
        'avtokredity'          => 5284,
        'ipoteka'              => 139,
        'credit-dlya-business' => 1265,
    ];

    public static $subscribe_form_mapping = [
        41   => 5,
        132  => 6,
        135  => 7,
        137  => 4,
        6275 => 8,
        5284 => 2,
        139  => 3,
        1265 => 1,
    ];

    public static $service_types_mapping = [
        'debetovye_karty'      => 6275,
        'creditnye_karty'      => 41,
        'kredity'              => 132,
        'mikrozaimy'           => 135,
        'vklady'               => 137,
        'ipoteka'              => 139,
        'credit_dlya_business' => 1265,
        'avtokredity'          => 5284,
    ];

    public static $service_types_for_url_mapping = [
        41   => 'creditnye-karty',
        132  => 'kredity',
        135  => 'mikrozaimy',
        137  => 'vklady',
        6275 => 'debetovye-karty',
        5284 => 'avtokredity',
        139  => 'ipoteka',
        1265 => 'credit-dlya-business',
    ];

    public static $banks_fields_mapping = [
        'Номер лицензии банка'           => 'licension',
        'Головной офис'                  => 'head_office',
        'Номера телефонов горячей линии' => 'phones',
        'Официальный сайт'               => 'offsite',
        'Интернет банк'                  => 'online_bank',
    ];

    public static $banks_data_fields_mapping = [
        'ОГРН'  => 'ogrn',
        'ИНН'   => 'inn',
        'КПП'   => 'kpp',
        'ОКПО'  => 'okpo',
        'БИК'   => 'bik',
        'SWIFT' => 'swift',
        'IBAN'  => 'iban',
    ];

    /**
     * 12 должно делиться на число элементов массива без остатка
     * 12 - число элементов сетки bootstrap
     */
    public static $footer_mapping = [
        [
            41   => 'creditnye-karty',
            6275 => 'debetovye-karty',
        ],
        [
            132  => 'kredity',
            5284 => 'avtokredity',
        ],
        [
            135 => 'mikrozaimy',
            139 => 'ipoteka',
        ],
        [
            137  => 'vklady',
            1265 => 'credit-dlya-business',
        ]
    ];

    public static $bankRatingsData = [
        'reyting-aktivy' => [
            'title' => 'Рейтинг банков по объему активов',
            'label' => 'Рейтинг по объему активов'
        ],
        'reyting-pribyl' => [
            'title' => 'Рейтинг банков по объему прибыли',
            'label' => 'Рейтинг по объему прибыли'
        ],
        'reyting-kredity' => [
            'title' => 'Рейтинг банков по объему кредитов',
            'label' => 'Рейтинг по объему кредитов'
        ],
        'reyting-vklady' => [
            'title' => 'Рейтинг банков по объему вкладов',
            'label' => 'Рейтинг по объему вкладов'
        ],
    ];

}