<?php
$creditznatok_rewrite_rules = new CreditznatokRewriteRules();

class CreditznatokRewriteRules
{
    private static $url_parts = null;

    function __construct()
    {
        $this->crediznatok_rewrite_permalinks();

        add_filter('post_type_link', array($this, 'rewrite_post_link'), 10, 2);
    }

    public static function rewrite_post_link($post_link, $post)
    {
        if ($post->post_status == 'draft') {
            return $post_link;
        }

        if ($post->post_type == 'offers') {
            $parent_post_id = CreditznatokSqlUtility::get_connected_service_type($post)->ID;
            $parent_post    = get_post($parent_post_id);

            if ('mikrozaimy' == $parent_post->post_name) {
                $post_link = "/{$parent_post->post_name}/{$post->post_name}/";
            } else {
                $parent_service_id = CreditznatokSqlUtility::get_connected_child('services', $post)->ID;
                $parent_service    = get_post($parent_service_id);

                $post_link = "/{$parent_post->post_name}/";

                if ($parent_service) {
                    $post_link .= "$parent_service->post_name/";
                }

                $post_link .= "$post->post_name/";
            }

            $post_link = home_url($post_link);
        }

        if ($post->post_type == 'service_types') {
            $post_link = "/$post->post_name/";
            $post_link = home_url($post_link);
        }

        if ($post->post_type == 'offer_reviews') {
            $parent_post_type_id = Creditznatok::get_parent_service_type_id($post);
            $parent_post_type    = array_search($parent_post_type_id,
                CreditznatokConfig::$service_types_in_url_mapping);
            //$parent_offer        = Creditznatok::get_parent_posts($post, 'offers')[0];
            $parent_offer = CreditznatokSqlUtility::get_connected_from_post('offers', $post);
            $post_link           = "/$parent_post_type/$parent_offer->post_name/otzyvy/";
            $post_link           = home_url($post_link);
        }

        if ($post->post_type == 'service_reviews') {
            $parent_post_type_id = Creditznatok::get_parent_service_type_id($post);
            $parent_post_type    = array_search($parent_post_type_id,
                CreditznatokConfig::$service_types_in_url_mapping);
            //$parent_service      = Creditznatok::get_parent_posts($post, 'services')[0];
            $parent_service = CreditznatokSqlUtility::get_connected_from_post('services', $post);
            $post_link           = "/$parent_post_type/$parent_service->post_name/otzyvy/";
            $post_link           = home_url($post_link);
        }

        if ($post->post_type == 'seo_groups') {
            $parent_post_type_id = Creditznatok::get_parent_service_type_id($post);
            $parent_post_type    = array_search($parent_post_type_id,
                CreditznatokConfig::$service_types_in_url_mapping);
            $post_link           = "/$parent_post_type/{$post->post_name}/";
            $post_link           = home_url($post_link);
        }

        if ($post->post_type == 'seo_groups_child') {
            $parent_post_type_id = Creditznatok::get_parent_service_type_id();
            $parent_post_type    = array_search($parent_post_type_id,
                CreditznatokConfig::$service_types_in_url_mapping);
            $parent_seo_groups   = Creditznatok::get_parent_posts($post, 'seo_groups')[0];
            $post_link           = "/$parent_post_type/$parent_seo_groups->post_name/{$post->post_name}/";
            $post_link           = home_url($post_link);
        }

        if ($post->post_type == 'services') {
            $parent_post_type_id = Creditznatok::get_parent_service_type_id();
            $parent_post_type    = array_search($parent_post_type_id,
                CreditznatokConfig::$service_types_in_url_mapping);

            if ('mikrozaimy' != $parent_post_type) {
                $post_link = "/$parent_post_type/{$post->post_name}/";
                $post_link = home_url($post_link);
            }
        }

        if ($post->post_type == 'banks') {
            $post_link = "/banki/{$post->post_name}/";
            $post_link = home_url($post_link);
        }

        if ($post->post_type == 'bank_data') {
            $parent_bank = CreditznatokSqlUtility::get_parent_bank('bank_data', $post);
            $post_link   = "/banki/{$parent_bank->post_name}/rekvizity/";
            $post_link   = home_url($post_link);
        }

        if ($post->post_type == 'bank_reviews') {
            $parent_bank = CreditznatokSqlUtility::get_parent_bank('bank_reviews', $post);
            $post_link   = "/banki/{$parent_bank->post_name}/otzyvy/";
            $post_link   = home_url($post_link);
        }

        return $post_link;
    }


    public static function get_url_parts()
    {
        if (self::$url_parts) {
            return self::$url_parts;
        }

        $url = urldecode(esc_url($_SERVER['REQUEST_URI']));

        if (strpos($url, '?')) {
            $url = explode('?', $url);
            $url = $url[0];
        }

        if (strpos($url, '#')) {
            $url = explode('#', $url);
            $url = $url[0];
        }

        $url             = preg_replace("/^\//", "", $url);
        $url             = preg_replace("/\/$/", "", $url);
        $url_parts       = explode('/', $url);
        self::$url_parts = $url_parts;

        return $url_parts;
    }

    function crediznatok_rewrite_permalinks()
    {
        if (is_admin()) {
            return;
        }

        global $url_parts;
        $url_parts      = self::get_url_parts();
        $first_url_part = $url_parts[0];

        if (count($url_parts) == 1 && $first_url_part == 'index.php'){
            if ( wp_redirect( get_home_url() ) ) {
                exit;
            }
        }

        if (isset(CreditznatokConfig::$service_types_in_url_mapping[$first_url_part])) {
            $url_fragments_count = count($url_parts);

            if (1 == $url_fragments_count) {
                $_GET['service_types']  = $first_url_part;
                $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . 'service_types=' . $first_url_part;

                return;
            }

            if (2 == $url_fragments_count) {
                global $url_post;
                $url_post = self::getPostFor2SlugElements($url_parts);
                $post     = $url_post;

                if ($post) {
                    add_filter('request', function ($query_vars) {
                        global $url_post;
                        $query_vars['page_id'] = $url_post->ID;

                        return $query_vars;
                    });

                    $_GET[$post->post_type] = $url_parts[1];
                    $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . $url_parts[1];

                    return;
                }

                /* //поиск совпадений по слагу
                 $args = array(
                     'post_type' => [
                         'offers',
                         'services',
                         'service_types',
                         'banks',
                         'seo_groups',
                         'seo_groups_child'
                     ],
                     'name' => end($url_parts),
                     'post_status' => 'any'
                 );

                 $query = new WP_Query($args);

                 $post = $query->posts[0];
                 if ($post) {
                     $_GET[$post->post_type] = end($url_parts);
                     $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . end($url_parts);
                     return;
                 }*/
            }

            if (3 == $url_fragments_count) {
                $third_part = $url_parts[2];

                if ($third_part == 'otzyvy') {
                    $second_part = $url_parts[1];
                    $args        = [
                        'post_type'   => [
                            'services',
                            'offers',
                        ],
                        'name'        => $second_part,
                        'post_status' => 'any'
                    ];

                    $query = new WP_Query($args);

                    if (count($query->posts) > 1) {
                        foreach ($query->posts as $current_post) {
                            $parent_type = CreditznatokSqlUtility::get_parent_service_type($current_post->post_type,
                                $current_post);

                            if ($parent_type->post_name == $url_parts[0]) {
                                $post = $current_post;
                                continue;
                            }
                        }
                    } else {
                        $post = $query->posts[0];
                    }

                    $needed_post_type = substr($post->post_type, 0, -1) . '_reviews';
                    $post             = CreditznatokSqlUtility::get_review_post($needed_post_type, $post);

                    if ($post) {
                        $_GET[$post->post_type] = $post->post_name;
                        $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . $post->post_name;

                        return;
                    }

                } else {
                    global $url_post;
                    $url_post = self::getPostFor3SlugElements($url_parts);
                    $post     = $url_post;

                    if ($post) {
                        add_filter('request', function ($query_vars) {
                            global $url_post;
                            $query_vars['page_id'] = $url_post->ID;

                            return $query_vars;
                        });

                        $_GET[$post->post_type] = $third_part;
                        $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . $third_part;

                        return;
                    }
                }
            }

            //поиск совпадений по слагу
            /*$args = array(
                'post_type'   => [
                    'offers',
                    'services',
                    'service_types',
                    'banks',
                    'seo_groups',
                    'seo_groups_child'
                ],
                'name'        => end($url_parts),
                'post_status' => 'any'
            );

            $query = new WP_Query($args);

            $post = $query->posts[0];
            if ($post) {
                $_GET[$post->post_type] = end($url_parts);
                $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . end($url_parts);

                return;
            }*/
        }

        if (count($url_parts) >= 2 && $url_parts[0] == 'mfo' && $url_parts[1] == 'search') {
            $_GET['pagename']       = 'mfo-search';
            $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?pagename=mfo-search';

            return;
        }

        if ($first_url_part == 'banki') {
            $url_fragments_count = count($url_parts);
            if ($url_fragments_count >= 2) {
                if (in_array($url_parts[1], CreditznatokBankRatings::$bank_ratings_urls)) {
                    $_GET['pagename']       = $url_parts[1];
                    $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?pagename=' . $url_parts[1];

                    return;
                }
            }

            if (2 == $url_fragments_count) {
                $second_part = $url_parts[1];
                $args        = array(
                    'post_type'   => 'banks',
                    'name'        => $second_part,
                    'post_status' => 'any'
                );

                $query                  = new WP_Query($args);
                $post                   = $query->posts[0];
                $_GET[$post->post_type] = $second_part;
                $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . $second_part;

                return;
            }

            if (3 == $url_fragments_count) {
                $second_part = $url_parts[1];
                $third_part  = $url_parts[2];
                $args        = array(
                    'post_type'   => 'banks',
                    'name'        => $second_part,
                    'post_status' => 'any'
                );

                $query = new WP_Query($args);
                $bank  = $query->posts[0];

                switch ($third_part) {
                    case 'rekvizity':
                        $needed_post_type = 'bank_data';
                        break;
                    case 'otzyvy':
                        $needed_post_type = 'bank_reviews';
                        break;
                }

                $post = CreditznatokSqlUtility::get_bank_child($needed_post_type, $bank);

                if ($post) {
                    $_GET[$post->post_type] = $post->post_name;
                    $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'] = '/?' . $post->post_type . '=' . $post->post_name;
                }

                return;
            }
        }
    }

    public static function getPostFor2SlugElements($url_parts)
    {

        $parent_service_type_id = CreditznatokConfig::$service_types_in_url_mapping[$url_parts[0]];
        $post_types             = ['seo_groups', 'services'];
        if ($parent_service_type_id == 135) { //поиск также среди продуктов для мирозаймов
            $post_types[] = 'offers';
        }

        $parent_service_type = get_post($parent_service_type_id);

        $child_post_ID = CreditznatokSqlUtility::getChildPostBySlag($post_types, $url_parts[1], $parent_service_type);

        $post = get_post($child_post_ID);

        return $post;
    }

    public static function getPostFor3SlugElements($url_parts)
    {
        $parent_service_type_id = CreditznatokConfig::$service_types_in_url_mapping[$url_parts[0]];
        $parent_service_type    = get_post($parent_service_type_id);
        $child_post_ID          = CreditznatokSqlUtility::getChildPostBySlag(['seo_groups', 'services'], $url_parts[1],
            $parent_service_type);
        $second_slug_post       = get_post($child_post_ID);

        if ( ! $second_slug_post) {
            return false;
        }

        $third_slug_post_ID = CreditznatokSqlUtility::getChildPostBySlag(['offers', 'seo_groups_child'], $url_parts[2],
            $second_slug_post);
        $third_slug_post    = get_post($third_slug_post_ID);

        return $third_slug_post;
    }

}