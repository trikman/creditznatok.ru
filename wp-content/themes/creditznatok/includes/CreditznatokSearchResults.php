<?php

class CreditznatokSearchResults
{
    private static $all_search_results = null;

    function create_sort_query($meta_key = 'percents_min', $order = 'ASC', $parent_post_id)
    {

        //ID постов типа услуг
        //41    кредитные карты
        //132   кредиты
        //135   микрозаймы
        //137   вклады
        //139   ипотека
        //1265  кредиты для бизнеса
        //5284  автокредиты
        //6275  дебетовые карты
        $result = null;

        switch ($parent_post_id) {

            case ($parent_post_id == 41 || $parent_post_id == 132 || $parent_post_id == 135):

                $result = array(
                    'meta_key'    => $meta_key,
                    'orderby'    => array('meta_value_num' => $order, 'meta_acceptance' => 'DESC', 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_percents_min' => array(
                            'key'     => $meta_key,
                            'compare' => 'EXISTS',
                            'type'    => 'NUMERIC',
                        ),

                        'meta_acceptance' => array(
                            'key'     => 'acceptance',
                            'compare' => 'EXISTS ',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;

            case ($parent_post_id == 6275 || $parent_post_id == 5284 || $parent_post_id == 139 || $parent_post_id == 1265):

                $result = array(
                    'meta_key'    => $meta_key,
                    'orderby'    => array('meta_value_num' => $order, 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_percents_min' => array(
                            'key'     => $meta_key,
                            'compare' => 'EXISTS',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;

            case ($parent_post_id == 137):

                $result = array(
                    'meta_key'    => $meta_key,
                    'orderby'    => array('meta_value_num' => $order, 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_percents_min' => array(
                            'key'     => $meta_key,
                            'compare' => 'EXISTS',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;
        }

        return $result;

    }

    public static function default_sort($parent_post_id)
    {

        //ID типов услуг
        //41    кредитные карты
        //132   кредиты
        //135   микрозаймы
        //137   вклады
        //139   ипотека
        //1265  кредиты для бизнеса
        //5284  автокредиты
        //6275  дебетовые карты
        $result = null;

        switch ($parent_post_id) {

            case ($parent_post_id == 41 || $parent_post_id == 132 || $parent_post_id == 135 || $parent_post_id == 139 || $parent_post_id == 1265 || $parent_post_id == 5284):

                $result = array(
                    'meta_key'    => 'acceptance',
                    'orderby'    => array('meta_acceptance' => 'DESC', 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_acceptance' => array(
                            'key'     => 'acceptance',
                            'compare' => 'EXISTS ',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;

            case ($parent_post_id == 6275):

                $result = array(
                    'meta_key'    => 'percents_min',
                    'orderby'    => array('meta_value_num' => 'ASC', 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_percents_min' => array(
                            'key'     => 'percents_min',
                            'compare' => 'EXISTS',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;

            case ($parent_post_id == 137):

                $result = array(
                    'meta_key'    => 'percents_min',
                    'orderby'    => array('meta_value_num' => 'DESC', 'title' => 'ASC'),
                    'meta_query' => array(
                        'relation' => 'AND',

                        'meta_percents_min' => array(
                            'key'     => 'percents_min',
                            'compare' => 'EXISTS',
                            'type'    => 'NUMERIC',
                        ),
                    ),
                );

                break;
        }

        return $result;

    }

    public static function get_sort()
    {
        $args           = array();
        $parent_post_id = Creditznatok::get_parent_service_type_id();

        if (isset ($_POST['sort']) && $_POST['sort'] !== '') {
            $order     = 'ASC';
            $sort_meta = null;

            switch ($_POST['sort']) {

                case 'razmer_zayma':
                    $sort_meta = 'razmer_zayma_max';
                    break;

                case 'percents':
                    $sort_meta = 'percents_min';
                    break;

                case 'acceptance':
                    $sort_meta = 'acceptance';
                    $order     = 'DESC';
                    break;

                case 'srok':
                    $sort_meta = 'srok_min';
                    $order     = 'DESC';
                    break;

                case 'time_min':
                    $sort_meta = 'time_min';
                    break;

                case 'time':
                    $sort_meta = 'time_min';
                    break;

                case 'crfp-average-rating':
                    $sort_meta       = '';
                    $args['orderby'] = 'title';
                    $args['order']   = $order;
                    break;
            }

            if ($sort_meta) {
                $sort = self::create_sort_query($sort_meta, $order, $parent_post_id);
                $args = wp_parse_args($sort, $args);
            }

        } else {

            $default_sort = self::default_sort($parent_post_id);
            $args         = wp_parse_args($default_sort, $args);
        }

        return $args;
    }

    public static function get_all_filters()
    {
        //error_log(print_r($_POST,true));
        $args         = array();
        $sorting_keys = array(
            'doc',
            'min_deposit',
            'cashback',
            'free_service',
            'without_garantors',
            'without_collateral',
            'preferential_dissolution',
            'capitalization',
            'way2get',
            'currency_new',
            'depositing',
            'partial_withdrawal',
            'without_income_verification'
        );

        foreach ($sorting_keys as $key) {
            if (isset ($_POST[$key]) && $_POST[$key] !== '') {
                if ( ($key=='without_income_verification') and ($_POST[$key] == 'on') ) {
                    $meta_key = 'proof_of_income';
                    $meta_value = 'Не требуется';
                } else {
                    $meta_key = $key;
                    $meta_value = $_POST[$key];
                }

                $args['meta_query'][] = array(
                    'key'   => $meta_key,
                    'value' => $meta_value,
                );
                //error_log(print_r($args,true));
            }
        }

        if (CreditznatokConfig::isGeoIpEnable) {
            $geoip_city = Creditznatok::get_geoip_city_for_query();
            $cities     = [$geoip_city, 'Показывать во всех регионах'];
            if ($cities) {
                $args['tax_query'][] = array(
                    array(
                        'taxonomy' => 'geoip_taxonomy',
                        'field'    => 'name',
                        'terms'    => $cities
                    )
                );
            }
        }

        if (isset ($_POST['razmer_zayma']) && $_POST['razmer_zayma'] !== '') {

            $args['meta_query'][] = array(
                'relation' => 'AND',
                array(
                    'key'     => 'razmer_zayma_min',
                    'value'   => str_replace(' ', '', $_POST['razmer_zayma']),
                    'type'    => 'numeric',
                    'compare' => '<='
                ),
                array(
                    'key'     => 'razmer_zayma_max',
                    'value'   => str_replace(' ', '', $_POST['razmer_zayma']),
                    'type'    => 'numeric',
                    'compare' => '>='
                )
            );
        }

        if (isset ($_POST['razmer_zayma_from']) && $_POST['razmer_zayma_from'] !== '') { //кредитные карты

            $args['meta_query'][] = array(
                'key'     => 'razmer_zayma_max',
                'value'   => str_replace(' ', '', $_POST['razmer_zayma_from']),
                'type'    => 'numeric',
                'compare' => '>='
            );
        }

        if (isset ($_POST['srok']) && $_POST['srok'] !== '') {

            $args['meta_query'][] = array(
                'relation' => 'AND',
                array(
                    'key'     => 'srok_min',
                    'value'   => str_replace(' ', '', $_POST['srok']),
                    'type'    => 'numeric',
                    'compare' => '<='
                ),
                array(
                    'key'     => 'srok_max',
                    'value'   => str_replace(' ', '', $_POST['srok']),
                    'type'    => 'numeric',
                    'compare' => '>='
                )
            );
        }

        return $args;

    }

    public static function check_autocredit_post($post_id)
    {

        if (isset($_POST['vehicle_type']) && $_POST['vehicle_type'] !== '') {
            $vehicle_type = get_post_meta($post_id, 'vehicle_type', true);

            foreach ($_POST['vehicle_type'] as $value) {

                if ( ! in_array($value, $vehicle_type)) {
                    return false;
                }
            }

        }

        if (isset($_POST['manufacturer_type']) && $_POST['vehicle_type'] !== '') {
            $manufacturer_type = get_post_meta($post_id, 'manufacturer_type', true);

            foreach ($_POST['manufacturer_type'] as $value) {

                if ( ! in_array($value, $manufacturer_type)) {
                    return false;
                }
            }
        }

        return true;
    }

    //Шорткод продуктов, для разных типов постов, выводит списком продукты подходящие под запрос.
    //TODO: ОПТИМИЗАЦИЯ ВЫВОДА ПАРАМЕТРОВ, ОПТИМИЗАЦИЯ ОБРАБОТКИ ПАРАМЕТРОВ ЗАПРОСА WP_QUERY
    public static function get_all_search_results()
    {
        if (self::$all_search_results) {
            return self::$all_search_results;
        }

        $parent_post_id = Creditznatok::get_parent_service_type_id();
        $post           = get_post($parent_post_id);
        $post_type      = $post->post_type;

        $args = array(
            'post_type'       => 'offers',
            'posts_per_page'  => -1,
            'connected_type'  => "offers_to_{$post_type}",
            'connected_items' => $post->ID,
            'post_status'     => 'publish',
            'fields'          => 'ids'
        );
        $args = wp_parse_args(self::get_sort(), $args);

        if (CreditznatokConfig::isGeoIpEnable) {
            $geoip_city = Creditznatok::get_geoip_city_for_query();
            $cities     = [$geoip_city, 'Показывать во всех регионах'];
            if ($cities) {
                $args['tax_query'][] = array(
                    array(
                        'taxonomy' => 'geoip_taxonomy',
                        'field'    => 'name',
                        'terms'    => $cities
                    )
                );
            }
        }

        $publish_posts = Creditznatok::get_posts('offers', $args);

        //вывод постов со статусом личное ниже основной выборки, только для типа постов "Услуги банка" и для сео группировок без опубликованных постов
        if ('services' == $post_type || ('seo_groups' == $post_type && ! count($publish_posts))) {

            $private_args                = $args;
            $private_args['post_status'] = array(
                'private',
                'inactive1'
            );

            $private_posts = Creditznatok::get_posts('offers', $private_args);
            $posts         = array_merge($publish_posts, $private_posts);
            $posts         = array_unique($posts,
                SORT_REGULAR); //удаление дублированных постов, появляющихся в результате разных статусов

        } else {
            $posts = $publish_posts;
        }

        $result = array();

        if (count($posts)) {
            if (isset ($_POST['banks']) && $_POST['banks']) { // banks обрабатываем особо (там multiselect). ВОЗМОЖНО ЕСТЬ СПОСОБ ОПТИМИЗИРОВАТЬ, ДОБАВИТЬ ЭТИ ПАРАМЕТРЫ В WP_QUERY
                $banks = $_POST['banks'];

            } else {
                $banks = false;
            }

            foreach ($posts as $post) {

                if ($parent_post_id == 135) {
                    $result[] = $post;
                    continue;
                }

                if ($banks) {
                    $post_bank_id = Creditznatok::get_bank($post);
                    if ( ! in_array($post_bank_id, $banks)) {
                        continue;
                    }
                }

                $result[] = $post;
            }
        }


        self::$all_search_results = $result;

        return $result;
    }

    public static function get_filtered_search_results()
    {
        
        $result         = array();
        $post           = get_post();
        $post_type      = $post->post_type;
        $parent_post_id = Creditznatok::get_parent_service_type_id($post);

        $args = array(
            'post_type'       => 'offers',
            'posts_per_page'  => -1,
            'connected_type'  => "offers_to_{$post_type}",
            'connected_items' => $post,
            'post_status'     => 'publish',
            'fields'          => 'ids',
        );

        $args = wp_parse_args(self::get_sort(), $args);
        $args = wp_parse_args(self::get_all_filters(), $args);

        $publish_posts = Creditznatok::get_posts('offers', $args);
        
        //вывод постов со статусом личное ниже основной выборки, 
        //только для типа постов "Услуги банка" и для сео группировок 
        //без опубликованных постов
        if ('services' == $post_type || ('seo_groups' == $post_type && ! count($publish_posts))) {
            $private_args                = $args;
            $private_args['post_status'] = array(
                'private',
                'inactive1'
            );

            $private_posts = Creditznatok::get_posts('offers', $private_args);
            $posts         = array_merge($publish_posts, $private_posts);
            $posts         = array_unique($posts,
                SORT_REGULAR); //удаление дублированных постов, появляющихся в результате разных статусов
        } else {
            $posts = $publish_posts;
        }

        //******************************************************************
        // спринт 11 - если никакие продукты не заданы
        // выведем все личные продукты на странице seo-группировки
        //******************************************************************
        if ( ( 'seo_groups' == $post_type ) and ( empty($publish_posts) ) ){
            // Find connected service_types
            $connected_service_types = new WP_Query( array(
              'connected_type'  => 'seo_groups_to_service_types',
              'connected_items' => get_queried_object(),
              'nopaging'        => true,
              'fields'          => 'ids',
            ) );
            if ( $connected_service_types->have_posts() ) :
                $service_types_ids = array();
                while ( $connected_service_types->have_posts() ) : $connected_service_types->the_post();
                    $service_types_ids[]=get_the_ID();
                endwhile;
            endif;
            wp_reset_postdata();

            // Find connected offers
            $connected_offers_args = array(
                'post_type'       => 'offers',
                'connected_type'  => 'offers_to_service_types',
                'connected_items' => $service_types_ids,
                'nopaging'        => true,
                'post_status'     => 'private',
                'fields'          => 'ids',
                'posts_per_page'  => -1,
            );
            /*
            $connected_offers = new WP_Query( array(
                'post_type'       => 'offers',
                'connected_type'  => 'offers_to_service_types',
                'connected_items' => $service_types_ids,
                'nopaging'        => true,
                'post_status'     => 'private',
                'fields'          => 'ids',
                'posts_per_page'  => -1,
            ) );
            */
            $posts = Creditznatok::get_posts('offers', $connected_offers_args);
            wp_reset_postdata();
        } 
        // 11 спринт end

        if (count($posts)) {

            if (isset ($_POST['banks']) && $_POST['banks']) { // banks обрабатываем особо (там multiselect). ВОЗМОЖНО ЕСТЬ СПОСОБ ОПТИМИЗИРОВАТЬ, ДОБАВИТЬ ЭТИ ПАРАМЕТРЫ В WP_QUERY
                $banks = $_POST['banks'];
            } else {
                $banks = false;
            }

            foreach ($posts as $post_id) {
                if ($parent_post_id == 135) {
                    $result[] = $post_id;
                    continue;
                }

                if ($banks) {
                    $post_bank_id = Creditznatok::get_bank($post_id);

                    if ( ! in_array($post_bank_id, $banks)) {
                        continue;
                    }
                }

                if ($parent_post_id == 5284 && ! self::check_autocredit_post($post_id)) {
                    continue;
                };

                $result[] = $post_id;
            }
        }

        return $result;
    }
}