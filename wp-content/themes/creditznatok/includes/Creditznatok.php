<?php
include_once('configs/CreditznatokLoadConfigs.php');
include_once('CreditznatokBankRatings.php');
include_once('CreditznatokBanners.php');
include_once('CreditznatokNews.php');
include_once('CreditznatokAjax.php');
include_once('CreditznatokCreditCalculator.php');
include_once('CreditznatokCurlHelper.php');
include_once('CreditznatokWeekChangeStatus.php');       //еженедельное планирование смены статуса у продуктов
include_once('CreditznatokShortcodes.php');                //универсальный шорткод продуктов
include_once('CreditznatokSearchResults.php');           //класс отвечающий за поисковую выдачу
include_once('geoip/CreditznatokGeoip.php');
include_once('CreditznatokWidgets.php');
include_once('CreditznatokFilterForm.php');
include_once('CreditznatokSql.php');
include_once('CreditznatokSqlUtility.php');
include_once('CreditznatokRewriteRules.php');
include_once('NewRegisterStructure.php');
include_once('CreditznatokSubscribeForms.php');

function cz_print_r($some)
{
    if (current_user_can('update_core') || (defined('CreditznatokConfig::isTestSite') && CreditznatokConfig::isTestSite)) {
        echo '<pre>';
        print_r($some);
        echo '</pre>';
    }
}

//include_once('fixes/index.php');              //разнообразные фиксы, обычно срабатывающие единоразово при запуске вручную из кода

$creditznatok = new Creditznatok();

class Creditznatok
{

    private static $all_banks = null;

    function __construct()
    {
        add_action('init', [$this, 'register_post_types']);

        add_action('p2p_init', [$this, 'creditznatok_connection_types']);

        add_action('clean_post_cache', [$this, 'creditznatok_clean_post_cache'], 10, 2);

        add_filter('comment_form_default_fields', [$this, 'remove_comment_fields']);

        add_filter('comments_open', [$this, 'filter_comments_open'], 10, 2);

        add_filter('comment_form_fields', [$this, 'reorder_comment_fields']);

        add_filter('cancel_comment_reply_link', [$this, 'return_false']);                                //убираем ссылку на "отменить комментарий", для нее не работают пермалинки

        add_action('add_meta_boxes', [$this, 'creditznatok_add_meta_boxes']);

        add_filter('aioseop_title', [$this, 'change_mfo_title'], 1);                                    //смена title на страницах МФО
        add_action('aioseop_title', [$this, 'rewrite_title'], 100);

        add_filter ('aioseop_description', [$this, 'aioseop_description_do_shortcode']);

        add_action('wp',        [$this, 'handle_mfo']);

        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles_for_all_pages'], 99);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_front_end_scripts'], 99);

        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_styles_for_all_pages'], 99);

        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts'], 99);

        add_filter('p2p_connectable_args', [$this, 'connectable_results_per_page'], 10, 3);

        add_filter('template_include', [$this, 'use_custom_template'], 99);

        add_filter('wp_unique_post_slug', [$this, 'unique_slug_for_custom_posts'], 10, 6);

        add_action('add_meta_boxes', [$this, 'remove_metabox_from_all_post_types'], 999);

        /**
         * Replaces the default excerpt editor with TinyMCE.
         */
        add_action( 'add_meta_boxes', [ $this, 'switch_boxes' ] );

        add_filter('manage_posts_columns', [$this, 'custom_table_head']);

        add_action('manage_posts_custom_column', [$this, 'custom_table_content'], 10, 2);

        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        remove_action('template_redirect', 'redirect_canonical');

        add_action('save_post', [$this, 'update_mfo_active_letters'], 10, 3);

        add_action('init', [$this, 'add_excerpts_to_pages']);


        $user = wp_get_current_user();
        if (!in_array('seo', $user->roles)){
            add_action('save_post', [$this, 'extra_fields_update'], 0);
            add_action('save_post', [$this, 'creditznatok_save_data_from_meta_boxes'], 10, 2);
            add_action('save_post', [$this, 'save_city_id_as_text_meta_field'], 10, 2);                //Сохранение адреса у дочерних постов мфо в текстовом виде в мета-поле city_name, для фильтрации по алфавиту в WP_Query. Изначально хранится только id поста regions
            add_action('save_post', [$this, 'save_geoip_meta_for_child_products'], 99, 2);             //Сохраняем geo-ip параметры при сохранении услуги банка для дочерних продуктов
            add_action('save_post', [$this, 'save_geoip_meta_for_products'], 99, 2);                   //Сохраняем geo-ip параметры родительской услуги банка при сохранении продукта
            add_action('save_post', [$this, 'save_acceptance_for_child_products'], 99, 2);              //Сохраняем процент одобрения при сохранении услуги банка для дочерних продуктов
            add_action('save_post', [$this, 'save_acceptance_for_products'], 99, 2);                   //Сохраняем процент одобрения родительской услуги банка при сохранении продукта
            add_action('save_post', [$this, 'extf_save_hook']);                                        //сохранение доп параметров, тайтла, сеосортировки
        }

        self::turn_off_rest_api();
        self::register_acf_option_page();
    }

    public static function get_footer_menu()
    {
        $footer_menu   = [];
        $service_types = array_keys(CreditznatokConfig::$service_types_mapping);
        foreach ($service_types as $service_type) {
            $linked_posts = get_field('footer_' . $service_type, 'option');
            if ($linked_posts) {
                $footer_menu[CreditznatokConfig::$service_types_mapping[$service_type]] = $linked_posts;
            }
        }

        return $footer_menu;
    }

    public static function register_acf_option_page()
    {
        $args = [
            /* (string) The title displayed on the options page. Required. */
            'page_title'  => 'Настройки creditznatok',

            /* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
            'menu_title'  => '',

            /* (string) The slug name to refer to this menu by (should be unique for this menu).
            Defaults to a url friendly version of menu_slug */
            'menu_slug'   => 'cz-options',

            /* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
            Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
            'capability'  => 'edit_posts',

            /* (int|string) The position in the menu order this menu should appear.
            WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
            Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
            Defaults to bottom of utility menu items */
            'position'    => false,

            /* (string) The slug of another WP admin page. if set, this will become a child page. */
            'parent_slug' => '',

            /* (string) The icon class for this menu. Defaults to default WordPress gear.
            Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
            'icon_url'    => 'dashicons-admin-site',

            /* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
            If set to false, this parent page will appear alongside any child pages. Defaults to true */
            'redirect'    => true,

            /* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
            Defaults to 'options'. Added in v5.2.7 */
            'post_id'     => 'options',

            /* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
            Defaults to false. Added in v5.2.8. */
            'autoload'    => false,
        ];
        acf_add_options_page($args);
    }

    public static function turn_off_rest_api()
    {
        // Отключаем сам REST API
        add_filter('rest_enabled', '__return_false');

        // Отключаем фильтры REST API
        remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
        remove_action('wp_head', 'rest_output_link_wp_head', 10, 0);
        remove_action('template_redirect', 'rest_output_link_header', 11, 0);
        remove_action('auth_cookie_malformed', 'rest_cookie_collect_status');
        remove_action('auth_cookie_expired', 'rest_cookie_collect_status');
        remove_action('auth_cookie_bad_username', 'rest_cookie_collect_status');
        remove_action('auth_cookie_bad_hash', 'rest_cookie_collect_status');
        remove_action('auth_cookie_valid', 'rest_cookie_collect_status');
        remove_filter('rest_authentication_errors', 'rest_cookie_check_errors', 100);

        // Отключаем события REST API
        remove_action('init', 'rest_api_init');
        remove_action('rest_api_init', 'rest_api_default_filters', 10, 1);
        remove_action('parse_request', 'rest_api_loaded');

        // Отключаем Embeds связанные с REST API
        remove_action('rest_api_init', 'wp_oembed_register_route');
        remove_filter('rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4);

        remove_action('wp_head', 'wp_oembed_add_discovery_links');
        // если собираетесь выводить вставки из других сайтов на своем, то закомментируйте след. строку.
        remove_action('wp_head', 'wp_oembed_add_host_js');

    }

    function add_excerpts_to_pages() {
        add_post_type_support( 'page', 'excerpt' );
    }

    function aioseop_description_do_shortcode($description){
        return do_shortcode($description);
    }

    public static function rewrite_title()
    {
        return self::get_title();
    }

    public static function get_post_excerpt(WP_Post $post, $size = 160)
    {
        if ( ! ($post instanceof WP_Post)) {
            return;
        }

        $string = '';

        if ($post->post_excerpt) {
            $string = $post->post_excerpt;
        } elseif ($post->post_content) {
            $string = $post->post_content;
        }

        $string = strip_shortcodes($string);
        $string = wp_strip_all_tags($string);

        $length = strlen(utf8_decode($string));
        if ($length >= $size - 3) {
            $string = mb_substr($string, 0, $size) . '...';
        }

        return $string;
    }

    public static function format_rus_date($date, $format = '')
    {
        $format = $format ? $format : 'j F Y';

        $translate = array(
            "am"        => "дп",
            "pm"        => "пп",
            "AM"        => "ДП",
            "PM"        => "ПП",
            "Monday"    => "Понедельник",
            "Mon"       => "Пн",
            "Tuesday"   => "Вторник",
            "Tue"       => "Вт",
            "Wednesday" => "Среда",
            "Wed"       => "Ср",
            "Thursday"  => "Четверг",
            "Thu"       => "Чт",
            "Friday"    => "Пятница",
            "Fri"       => "Пт",
            "Saturday"  => "Суббота",
            "Sat"       => "Сб",
            "Sunday"    => "Воскресенье",
            "Sun"       => "Вс",
            "January"   => "Января",
            "Jan"       => "Янв",
            "February"  => "Февраля",
            "Feb"       => "Фев",
            "March"     => "Марта",
            "Mar"       => "Мар",
            "April"     => "Апреля",
            "Apr"       => "Апр",
            "May"       => "Мая",
            "June"      => "Июня",
            "Jun"       => "Июн",
            "July"      => "Июля",
            "Jul"       => "Июл",
            "August"    => "Августа",
            "Aug"       => "Авг",
            "September" => "Сентября",
            "Sep"       => "Сен",
            "October"   => "Октября",
            "Oct"       => "Окт",
            "November"  => "Ноября",
            "Nov"       => "Ноя",
            "December"  => "Декабря",
            "Dec"       => "Дек",
            "st"        => "ое",
            "nd"        => "ое",
            "rd"        => "е",
            "th"        => "ое"
        );

        return strtr(self::format_date($date, $format), $translate);
    }

    public static function format_date($date = '', $format = 'j F Y')
    {
        $date = new DateTime($date);

        return $date->format($format);
    }

    public static function get_news_for_main_page()
    {
        $args = [
            'post_type'      => 'news',
            'posts_per_page' => 3,
            'post_status'    => 'publish',
        ];

        return self::get_posts('news', $args);
    }

    public static function get_bank_rating_query($rating_type, $per_page = 50, $paged = 1)
    {
        if (isset($bankRatingMapping[$rating_type])) {
            return false;
        }

        $args = [
            'post_type'      => 'banks',
            'posts_per_page' => $per_page,
            'meta_key'       => $rating_type,
            'orderby'        => 'meta_value_num',
            'order'          => 'DESC',
            'paged'          => $paged,
            'meta_query'     => [
                [
                    'key'     => $rating_type,
                    'value'   => '',
                    'compare' => '!='
                ]
            ]
        ];
        $query = new WP_Query($args);

        return $query;
    }

    public function update_mfo_active_letters($post_ID, WP_Post $post, $update)
    {
        if ($post->post_type != 'mfo') {
            return;
        }

        $letters = CreditznatokSqlUtility::get_first_mfo_letters();
        foreach ($letters as $key => $array) {
            if ($array['letter'] == 'Ё') {
                //MySQL не различает Е и Ё в текущей collate
                $letters[$key]['letter'] = 'Е';
            }
        }
        update_option('mfo_letters', $letters);
    }

    public static function get_bank_sorted_fields($post_id)
    {
        $result            = [];
        $bankRatingMapping = CreditznatokConfig::$bankRatingMapping;
        foreach ($bankRatingMapping as $meta_key => $title) {
            $meta_value = get_post_meta($post_id, $meta_key, 1);
            if ($meta_value) {
                $result[$meta_key] = $meta_value;
            }
        }

        //arsort($result, SORT_NUMERIC);

        return $result;
    }

    public static function get_bank_rating_field($post_id)
    {
        $result            = [];
        $bankRatingMapping = CreditznatokConfig::$bankRatingMapping;
        $url_parts = CreditznatokRewriteRules::get_url_parts();
        if (isset($bankRatingMapping[$url_parts[1]])){
            $meta_value = get_post_meta($post_id, $url_parts[1], 1);
            if ($meta_value) {
                $result[$url_parts[1]] = $meta_value;
            }
        }

        return $result;
    }

    public static function format_number($number)
    {
        return number_format($number, 0, ' ', ' ') . ' т. ' . CreditznatokConfig::CurrencyNameShort;
    }

    public static function mbStringToArray($string)
    {
        $array  = [];
        $strlen = mb_strlen($string);
        while ($strlen) {
            $array[] = mb_substr($string, 0, 1, "UTF-8");
            $string  = mb_substr($string, 1, $strlen, "UTF-8");
            $strlen  = mb_strlen($string);
        }

        return $array;
    }

    public static function get_archive_mfo_query()
    {
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $args = array(
            'post_type'      => 'mfo',
            'orderby'        => 'title',
            'order'          => 'ASC',
            'post_parent'    => 0,
            'posts_per_page' => 40,
            'paged'          => $paged
        );

        $query = new WP_Query($args);

        return $query;
    }

    public static function filterWhereByFirstLetter($sql)
    {
        global $wpdb;
        $url_parts = CreditznatokRewriteRules::get_url_parts();

        if (isset($url_parts[2])) {
            $sql .= $wpdb->prepare(" AND $wpdb->posts.post_title LIKE %s ", $url_parts[2] . '%');
        }

        return $sql;
    }

    public static function getLetterForMfoSearch()
    {
        $url_parts = CreditznatokRewriteRules::get_url_parts();
        if (isset($url_parts[2])) {
            return $url_parts[2];
        }

        return false;
    }

    public static function get_mfo_search_query()
    {
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        add_action('posts_where', ['Creditznatok', 'filterWhereByFirstLetter']);
        $args = [
            'post_type'      => 'mfo',
            'orderby'        => 'title',
            'order'          => 'ASC',
            'post_parent'    => 0,
            'posts_per_page' => 40,
            'paged'          => $paged
        ];

        $query = new WP_Query($args);
        remove_action('posts_where', ['Creditznatok', 'filterWhereByFirstLetter']);

        return $query;
    }

    public static function format_mfo_work_time($time)
    {
        if (!$time){
            return null;
        }

        $time = DateTime::createFromFormat('H:i:s', $time);

        return $time->format('H:i');
    }

    function handle_mfo()
    {
        $post = get_post();

        if ( ! $post || $post->post_type != 'mfo' || ! $post->post_parent) {
            return;
        }

        define('DONOTCACHEDB', true);
        define('DONOTCACHEOBJECT', true);

        $address     = get_post_meta($post->ID, 'address', true);
        $city        = get_post_meta($post->ID, 'city_name', true);
        $coordinates = get_post_meta($post->ID, 'coordinates', true);
        $metro       = get_post_meta($post->ID, 'metro', true);

        if ( ! $address) {
            return;
        }

        $address = $city . ', ' . $address;

        if ( ! $coordinates || $coordinates['address'] != $address) {
            $coordinates = $this->update_geocode($post, $address, 'coordinates');
        }

        if ( ! $metro || $metro['address'] != $coordinates['lat_long']) {
            $metro = $this->update_metro_geocode($post, $coordinates['lat_long']);
        }
    }

    function update_metro_geocode(WP_Post $post, $address)
    {
        delete_post_meta($post->ID, 'metro');

        $geocode = $this->get_geocode($address, ['kind' => 'metro', 'sco' => 'latlong']);

        $data = [
            'address'  => $address,
            'geocode'  => $geocode,
            'lat_long' => false,
        ];

        if ($geocode) {
            $lat_long_str     = $geocode->GeoObject->Point->pos;
            $lat_long_arr     = explode(' ', $lat_long_str);
            $data['lat_long'] = $lat_long_arr[1] . ', ' . $lat_long_arr[0];
            $data['lat']      = $lat_long_arr[1];
            $data['long']     = $lat_long_arr[0];
        }

        update_post_meta($post->ID, 'metro', $data);

        return $data;
    }

    function update_geocode(WP_Post $post, $address, $meta)
    {
        delete_post_meta($post->ID, $meta);
        $address = str_replace(' ', '', $address);
        $geocode = $this->get_geocode($address);
        $data    = [
            'address'  => $address,
            'geocode'  => $geocode,
            'lat_long' => false,
        ];

        if ($geocode) {
            $lat_long_str     = $geocode->GeoObject->Point->pos;
            $lat_long_arr     = explode(' ', $lat_long_str);
            $data['lat_long'] = $lat_long_arr[1] . ', ' . $lat_long_arr[0];
            $data['lat']      = $lat_long_arr[1];
            $data['long']     = $lat_long_arr[0];
        }

        update_post_meta($post->ID, $meta, $data);

        return $data;
    }

    function get_geocode($address, $args = [])
    {
        $url          = 'https://geocode-maps.yandex.ru/1.x/';
        $default_args = [
            'format'  => 'json',
            'results' => 1,
            'geocode' => $address
        ];
        $args         = wp_parse_args($args, $default_args);
        $response     = CreditznatokCurlHelper::SingleCurl($url, $args);

        $results = false;
        if ($response) {
            $response = json_decode($response);
            $objects  = $response->response->GeoObjectCollection->featureMember;

            if (count($objects)) {
                $results = $objects[0];
            }
        }

        return $results;
    }

    function use_custom_template($template)
    {
        $post = get_post();

        if (is_single() && $post && $post->post_type == 'mfo') {
            if ($post->post_parent) {
                $template = locate_template('single-mfo-child.php');
            } else {
                $template = locate_template('single-mfo.php');
            }
        }

        return $template;
    }

    function unique_slug_for_custom_posts($slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug)
    {
        if ($slug) {
            if (isset(CreditznatokConfig::$posts_relations[$post_type])) {
                return $original_slug;
            }
        }

        return $slug;
    }

    function connectable_results_per_page($args, $ctype, $post_id)
    {
        $args['p2p:per_page'] = 10;

        return $args;
    }

    function custom_table_head($defaults)
    {
        $post_type = $_GET['post_type'];
        if (isset(CreditznatokConfig::$posts_relations[$post_type])) {
            foreach (CreditznatokConfig::$posts_relations[$post_type] as $post_name) {
                $defaults[$post_name] = NewRegisterStructure::$structure[$post_name];
            }
        }

        if ($_GET['post_type'] == 'banks') {
            $defaults['bank_reviews'] = 'Отзывы о банке';
            $defaults['bank_data'] = 'Реквизиты банка';
        }

        return $defaults;
    }

    function custom_table_content($column_name, $post_id)
    {
        // Патч отзывов и реквизитов банка
        if ($_GET['post_type'] == 'banks') {

            if ($column_name == 'bank_reviews' || $column_name == 'bank_data') {

                $posts = self::get_all_connected_posts($column_name, get_post($post_id));

                if (count($posts)) {
                    echo "<a target='_blank' href='" . admin_url() . "/post.php?post={$posts[0]->ID}&action=edit'>Редактировать</a>";
                } /*else {
					echo "<a target='_blank' href='" . admin_url() . "/post-new.php?post_type={$column_name}&select[tax_banks]={$tax_term_id}'>Создать новый</a>";
				}*/


            }

        } else {

            // Стандартный вариант
            if (isset(CreditznatokConfig::$posts_relations[$_GET['post_type']])) {

                $posts = self::get_all_connected_posts($column_name, get_post($post_id));

                if (!count($posts)) {
                    echo "&mdash;";
                } else {
                    $html = array();
                    foreach ($posts as $post) {
                        $html[] = "<a href='" . admin_url() . "/edit.php?post_type={$_GET['post_type']}'>{$post->post_title}</a> <a href='" . admin_url() . "/post.php?post={$post->ID}&action=edit'>[ред]</a>";
                    }
                    if (count($html) == 1) {
                        echo $html[0];
                    } else if (count($html) <= 2) {
                        echo '<ol><li>' . implode('<li>', $html) . '</ol>';
                    } else {
                        ?>
                        <div id="full-<?php echo $post_id; ?>"
                             style="display:none;"><?php echo '<ol><li>' . implode('<li>',
                                    $html) . '</ol>'; ?>
                            <BR>
                            <a href="javascript:;"
                               onClick="jQuery('#full-<?php echo $post_id; ?>').hide();jQuery('#short-<?php echo $post_id; ?>').show();"><b><i>[Скрыть]</i></b></a>
                        </div>
                        <div id="short-<?php echo $post_id; ?>"><?php echo '<ol><li>' . implode('<li>',
                                    array_slice($html, 0, 2)) . '</ol>'; ?>
                            <BR>
                            <a href="javascript:;"
                               onClick="jQuery('#full-<?php echo $post_id; ?>').show();jQuery('#short-<?php echo $post_id; ?>').hide();"><b><i>[Показать
                                        все <?php echo count($html); ?>]</i></b></a>
                        </div>
                        <?php
                    }
                }
            }
        }
    }

    function admin_enqueue_scripts()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-tabs');

        wp_enqueue_script('admin-scripts-for-all-pages',
            get_template_directory_uri() . '/includes/js/admin-area-scripts.js');

        wp_localize_script('admin-scripts-for-all-pages', 'creditznatok',
            ['ajaxUrl' => admin_url('admin-ajax.php'), 'currencyIn' => CreditznatokConfig::CurrencyIn]);
    }

    function custom_filter_dropdown($post_name)
    {
        $args = [
            'post_status' => 'any',
            'posts_per_page' => -1,
        ];
        $posts = $this->get_posts($post_name, $args);

        ?>
        <select id="taxselect-<?php echo $post_name; ?>" name="<?php echo $post_name; ?>">
            <option value=''> - <?php echo NewRegisterStructure::$structure[$post_name] ?> -</option>
            <?php foreach ($posts as $post) { ?>
                <option value="<?php echo $post->ID; ?>" <?php selected($post->post_name,
                    $_GET[$post_name]); ?>><?php echo $post->post_title; ?></option>
            <?php } ?>
        </select>
        <?php
    }

    function remove_metabox_from_all_post_types()
    {
        $current_user = wp_get_current_user();

        if (!in_array('seo', (array)$current_user->roles)) {
            return;
        }

        global $wp_meta_boxes;

        /** Create an array of meta boxes exceptions, ones that should not be removed (remove if you don't want/need) */
        $exceptions = array(
            'aiosp',
            'submitdiv',
            'postexcerpt2'
        );

        /** Loop through each page key of the '$wp_meta_boxes' global... */
        if (!empty($wp_meta_boxes)) : foreach ($wp_meta_boxes as $page => $page_boxes) :

            /** Loop through each contect... */
            if (!empty($page_boxes)) : foreach ($page_boxes as $context => $box_context) :

                /** Loop through each type of meta box... */
                if (!empty($box_context)) : foreach ($box_context as $box_type) :

                    /** Loop through each individual box... */
                    if (!empty($box_type)) : foreach ($box_type as $id => $box) :

                        /** Check to see if the meta box should be removed... */
                        if (!in_array($id, $exceptions)) :

                            /** Remove the meta box */
                            remove_meta_box($id, $page, $context);
                        endif;

                    endforeach;
                    endif;

                endforeach;
                endif;

            endforeach;
            endif;

        endforeach;
        endif;
    }

    public static function get_parent_service_type_for_url($post = null)
    {
        $result = false;

        if (!$post) {
            $post = get_post();
        }

        $parent_id = self::get_parent_service_type_id($post);
        if ($parent_id) {
            $result = isset(CreditznatokConfig::$service_types_for_url_mapping[$parent_id]) ? CreditznatokConfig::$service_types_for_url_mapping[$parent_id] : false;
        }

        return $result;
    }

    public static function get_parent_service_type($post = null, $debug = false)
    {
        $result = false;

        if (!$post) {
            $post = get_post();
        }

        $parent_id = self::get_parent_service_type_id($post, $debug);

        if ($parent_id) {
            $result = array_search($parent_id, CreditznatokConfig::$service_types_mapping);
        }

        return $result;
    }

    public static function get_parent_service_type_id($post = null, $debug = null)
    {
        $result = false;

        if (!$post) {
            $post = get_post();
        }

        if ('service_types' == $post->post_type) {
            return $post->ID;
        }

        if ($post->post_type == 'seo_groups_child') {
            $post = self::get_parent_posts($post, 'seo_groups')[0];
        }

        if ($post->post_type == 'offer_reviews') {
            $post = self::get_parent_posts($post, 'offers')[0];
        }

        if ($post->post_type == 'service_reviews') {
            $post = self::get_parent_posts($post, 'services')[0];
        }

        $args = array(
            'post_type' => 'service_types',
            'posts_per_page' => -1,
            'connected_type' => "{$post->post_type}_to_service_types",
            'connected_direction' => 'any',
            'fields' => 'ids',
            'connected_items' => $post,
            'post_status' => ['publish', 'private'],
        );

        $parent_posts = self::get_posts('service_types', $args);
        $parent_service_type_by_url = self::getParentServiceTypeIdByUrl();

        if (count($parent_posts)) {
            $result = $parent_posts[0];
        } elseif ($parent_service_type_by_url) {
            $result = $parent_service_type_by_url;
        }

        return $result;
    }

    public static function getParentServiceTypeIdByUrl()
    {
        global $url_parts;
        $first_url_part = $url_parts[0];

        if (isset(CreditznatokConfig::$service_types_in_url_mapping[$first_url_part])) {
            return CreditznatokConfig::$service_types_in_url_mapping[$first_url_part];
        }

        return false;
    }

    function return_false()
    {
        return false;
    }

    function change_mfo_title($title)
    {

        if (is_post_type_archive('mfo')) {
            $title = 'Микрофинансовые организации';
        }

        return $title;
    }

    function save_city_id_as_text_meta_field($post_id, $post)
    {

        if ($post->post_type != 'mfo') {
            return;
        }

        if ($post->post_parent === 0) {
            return;
        }

        $city_id = get_post_meta($post->ID, 'city', true);

        if ($city_id) {
            $city_post = get_post($city_id);
            $city = $city_post->post_title;
            update_post_meta($post->ID, 'city_name', $city);
        }
    }

    function enqueue_styles_for_all_pages()
    {
        //wp_enqueue_style('chosen-bootstrap', get_template_directory_uri() . '/includes/chosen/bootstrap-style/bootstrap-chosen.css');

        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/includes/css/new-style/bootstrap.min.css');
        wp_enqueue_style('bootstrap-select', get_template_directory_uri() . '/includes/css/new-style/bootstrap-select.min.css');
        //wp_enqueue_style('jquery-scroll-bar', get_template_directory_uri() . '/includes/css/new-style/jquery.scrollbar.css');
        wp_enqueue_style('creditznatok-style', get_template_directory_uri() . '/includes/css/new-style/style.css');
    }

    function enqueue_front_end_scripts()
    {
        wp_enqueue_script('jquery');

        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/includes/js/bootstrap.min.js');

        wp_enqueue_script('numeral', get_template_directory_uri() . '/includes/js/numeral.min.js');

        wp_enqueue_script('front-end-scripts', get_template_directory_uri() . '/includes/js/front-end-scripts.js',
            ['jquery'], false, true);
        wp_localize_script('front-end-scripts', 'creditznatok',
            ['ajax_url' => admin_url('admin-ajax.php'), 'template_directory_uri' => get_template_directory_uri()]);

        wp_enqueue_script('jquery-autocomplete', get_template_directory_uri() . '/includes/js/jquery.autocomplete.js',
            ['jquery'], false, true);
        wp_enqueue_script('bootstrap-select', get_template_directory_uri() . '/includes/js/bootstrap-select.min.js',
            ['jquery'], false, true);
    }

    function admin_enqueue_styles_for_all_pages()
    {
        wp_enqueue_style('admin_styles_for_all_pages', get_template_directory_uri() . '/includes/css/admin-style.css');
    }

    function save_acceptance_for_products($post_id, $post)
    {

        if ('offers' != $post->post_type) {
            return;
        }

        $parent_posts = self::get_all_connected_posts('services', $post);

        if (empty($parent_posts)) {
            return;
        }

        $parent_service_post = $parent_posts[0];
        $acceptance = get_post_meta($parent_service_post->ID, 'acceptance', true);

        if ($acceptance || $acceptance === 0) {
            update_post_meta($post_id, 'acceptance', $acceptance);
        }

    }

    function save_acceptance_for_child_products($post_id, $post)
    {

        if ('services' != $post->post_type) {
            return;
        }

        $acceptance = get_post_meta($post->ID, 'acceptance', true);

        if ($acceptance || $acceptance === 0) {
            $child_posts = self::get_all_connected_posts('offers', $post);

            if (!empty($child_posts)) {

                foreach ($child_posts as $child_post) {
                    update_post_meta($child_post->ID, 'acceptance', $acceptance);
                }
            }

        }
    }

    function save_geoip_meta_for_products($post_id, $post)
    {

        if ('offers' != $post->post_type) {
            return;
        }

        $parent_posts = self::get_all_connected_posts('services', $post_id);

        if (empty($parent_posts)) {
            return;
        }

        $parent_service_post = $parent_posts[0];
        $geoip_terms = wp_get_post_terms($parent_service_post->ID, 'geoip_taxonomy', array("fields" => "ids"));

        wp_set_object_terms($post_id, $geoip_terms, 'geoip_taxonomy');

    }

    function save_geoip_meta_for_child_products($post_id, $post)
    {
        if ('services' != $post->post_type) {
            return;
        }
        $child_posts = self::get_all_connected_posts('offers', $post, ['fields' => 'ids']);

        if (!empty($child_posts)) {
            $geoip_terms = wp_get_post_terms($post->ID, 'geoip_taxonomy', array("fields" => "ids"));

            if (empty ($geoip_terms)) {
                $geoip_terms = [];
            }

            foreach ($child_posts as $child_post_ID) {
                wp_set_object_terms((int)$child_post_ID, $geoip_terms, 'geoip_taxonomy');
            }
        }
    }

    function filter_comments_open($open, $post_id)
    {

        $post = get_post($post_id);
        $post_parent = get_post($post->post_parent);

        if ('page' == $post->post_type && $post_parent->post_parent == 1837) {
            $open = true;
        }

        return $open;
    }

    function remove_comment_fields($fields)
    {
        unset($fields['email']);
        unset($fields['url']);

        return $fields;
    }

    function extra_fields_update($post_id)
    {

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return false;
        }

        if (!isset($_POST['extra'])) {
            return false;
        }

        if (!wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__)) {
            return false;
        }

        if (!current_user_can('edit_post', $post_id)) {
            return false;
        }

        foreach ($_POST['extra'] as $key => $value) {
            if (!is_array($value)) {
                $value = trim($value);
            }

            if (empty($value) && 0 === $value) {
                delete_post_meta($post_id, $key);
            } else {
                update_post_meta($post_id, $key, $value);
            }
        }

        return $post_id;
    }

    public static function parent_posts($needed_post_type, $post, $ids = false)
    {

        if (!is_object($post)) {
            $post = get_post($post);
        }

        $args = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$post->post_type}_to_{$needed_post_type}",
            'connected_items' => $post->ID,
            'connected_direction' => 'to',
        );

        if ($ids) {
            $args['fields'] = 'ids';
        }

        $results = self::get_posts($needed_post_type, $args);

        if (count($results)) {
            return $results;
        } else {
            return array();
        }
    }

    /**
     *    Получение всех детей определенного типа для поста (объекта)
     *
     * @param      $needed_post_type
     * @param      $post
     * @param null $add_args
     *
     * @return array|mixed
     */
    public static function child_posts($needed_post_type, $post, $add_args = null)
    {
        if (!is_object($post)) {
            $post = get_post($post);
        }

        $args = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$post->post_type}_to_{$needed_post_type}",
            'connected_items' => $post->ID,
            'connected_direction' => 'from',
        );

        if ($add_args) {
            $args = wp_parse_args($add_args, $args);
        }

        $results = self::get_posts($needed_post_type, $args);

        if (count($results)) {
            return $results;

        } else {
            return array();
        }
    }

    /**
     *    Получение всех связанных постов, как детей так и родителей
     *
     * @param      $needed_post_type
     * @param      $post
     * @param null $add_args
     *
     * @return array|mixed
     */
    public static function get_all_connected_posts($needed_post_type, $post, $add_args = null)
    {
        if (!is_object($post)) {
            $post = get_post($post);
        }

        $args = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$post->post_type}_to_{$needed_post_type}",
            'connected_items' => $post->ID,
            'post_status' => 'any',
        );

        if ($add_args) {
            $args = wp_parse_args($add_args, $args);
        }

        $args2 = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$needed_post_type}_to_{$post->post_type}",
            'connected_items' => $post->ID,
            'post_status' => 'any',
        );

        if ($add_args) {
            $args2 = wp_parse_args($add_args, $args2);
        }

        $results = self::get_posts($needed_post_type, $args);
        $results2 = self::get_posts($needed_post_type, $args2);

        $results = array_merge($results, $results2);

        if (count($results)) {
            return $results;

        } else {
            return array();
        }
    }

    public static function get_linked_posts($needed_post_type, $post, $add_args = null)
    {
        if (!is_object($post)) {
            $post = get_post($post);
        }

        $args = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$needed_post_type}_to_{$post->post_type}",
            'connected_items' => $post->ID,
        );

        $geoip_city = self::get_geoip_city_for_query();
        if ($geoip_city) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'geoip_taxonomy',
                    'field' => 'name',
                    'terms' => $geoip_city
                )
            );
        }

        if ($add_args) {
            $args = wp_parse_args($add_args, $args);
        }

        $results = self::get_posts($needed_post_type, $args);

        if (count($results)) {
            return $results;

        } else {
            return array();
        }
    }

    public static function get_review_post($post_id = null)
    {
        $parent_service_type = self::get_parent_service_type();

        if ('mikrozaimy' == $parent_service_type) {
            $args = array(
                'post_type' => 'offer_reviews',
                'connected_type' => 'offer_reviews_to_offers',
                'connected_items' => $post_id,
                'fields' => 'ids'
            );

            $results = self::get_posts('offer_reviews', $args);

            if (count($results)) {
                return ($results[0]);
            }

        } else {

            $service_args = array(
                'post_type' => 'services',
                'connected_type' => "offers_to_services",
                'connected_items' => $post_id,
                'connected_direction' => 'from',
                'post_per_page' => -1,
                'fields' => 'ids',
            );
            $parent_service_id = self::get_posts('services', $service_args)[0];

            $args = array(
                'post_type' => 'service_reviews',
                'connected_type' => 'service_reviews_to_services',
                'connected_items' => $parent_service_id,
                'fields' => 'ids'
            );

            $results = self::get_posts('service_reviews', $args);

            if (count($results)) {
                return ($results[0]);
            }
        }

        return '';
    }

    public static function get_bank_reviews_link($post = null)
    {

        if (!$post) {
            $post = get_post();
        }

        $bank_reviews = Creditznatok::get_all_connected_posts('bank_reviews', $post);

        if (count($bank_reviews)) {
            return get_permalink($bank_reviews[0]);
        }

        return '';
    }

    public static function get_bank_data_link($post = null)
    {

        if (!$post) {
            $post = get_post();
        }

        $bank_data = Creditznatok::get_all_connected_posts('bank_data', $post);

        if (count($bank_data)) {
            return get_permalink($bank_data[0]);
        }

        return '';
    }

    function meta_box_for_offers($post)
    {
        ?>
        <style type="text/css">
            <?php if(current_user_can('editor')){ ?>
            .post-php.post-type-offers #postcustom {
                display: none !important;
            }

            <?php } ?>
        </style>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#rt_tabs").tabs();
            });
        </script>

        <div class="reviews">

            <table border=0>
                <tr>
                    <td><b>Страница отзывов о продукте</b></td>
                    <td> &mdash; <span class="cp-create-reviews-page-wrap">
							<?php
                            $children = self::get_all_connected_posts('offer_reviews', $post);
                            if ($children) { ?>
                                <a target="_blank"
                                   href="<?php echo admin_url() . "post.php?post={$children[0]->ID}&action=edit"; ?>"><?php echo $children[0]->post_title; ?></a>
                            <?php } else { ?>
                                не создана. <a href="#" class="cp-create-reviews-page"
                                               data-post-id="<?php echo $post->ID; ?>">Создать страницу отзывов</a>
                            <?php } ?>
						</span>
                    </td>
                </tr>
                <tr>
                    <td><b>Страница отзывов об услуге банка</b></td>
                    <td> &mdash;
                        <span class="cp-create-reviews-page-wrap">
                            <?php
                            $parent_service = self::get_all_connected_posts('services', $post);
                            if ($parent_service) {
                                $children = self::get_all_connected_posts('service_reviews', $parent_service[0]->ID);
                                if ($children) { ?>
                                    <a target="_blank"
                                       href="<?php echo admin_url() . "post.php?post={$children[0]->ID}&action=edit"; ?>"><?php echo $children[0]->post_title; ?></a>
                                <?php } else { ?>
                                    не создана. <a href="#" class="cp-create-reviews-page"
                                                   data-post-id="<?php echo $parent_service[0]->ID; ?>">Создать страницу отзывов</a>
                                <?php }
                            } else { ?>
                                Услуга банка еще не создана.
                            <?php } ?>
                        </span>
                    </td>
                </tr>
            </table>

        </div>
        <?php

        $post_service_type = $this->get_parent_service_type();

        $min_default = 0;
        $max_default = 999999999;

        $razmer_zayma_min = get_post_meta($post->ID, 'razmer_zayma_min', true);
        if ($razmer_zayma_min === '') {
            $razmer_zayma_min = $min_default;
        }

        $razmer_zayma_max = get_post_meta($post->ID, 'razmer_zayma_max', true);
        if ($razmer_zayma_max === '') {
            $razmer_zayma_max = $max_default;
        }

        $percents_min = get_post_meta($post->ID, 'percents_min', true);
        if ($percents_min === '') {
            $percents_min = $min_default;
        }

        $percents_max = get_post_meta($post->ID, 'percents_max', true);
        if ($percents_max === '') {
            $percents_max = $max_default;
        }

        $srok_min = get_post_meta($post->ID, 'srok_min', true);
        if ($srok_min === '') {
            $srok_min = $min_default;
        }

        $srok_max = get_post_meta($post->ID, 'srok_max', true);
        if ($srok_max === '') {
            $srok_max = $max_default;
        }

        $vozrast_min = get_post_meta($post->ID, 'vozrast_min', true);
        if ($vozrast_min === '') {
            $vozrast_min = $min_default;
        }

        $vozrast_max = get_post_meta($post->ID, 'vozrast_max', true);
        if ($vozrast_max === '') {
            $vozrast_max = $max_default;
        }

        $time_min = get_post_meta($post->ID, 'time_min', true);
        if ($time_min === '') {
            $time_min = $min_default;
        }

        $time_max = get_post_meta($post->ID, 'time_max', true);
        if ($time_max === '') {
            $time_max = $max_default;
        }


        ?>

        <div id="rt_tabs">
            <ul>
                <li><a href="#banks">Инфо для банка</a></li>
                <li><a href="#cards">Инфо по картам</a></li>
            </ul>

            <div id="cards">
                <div class="tab-options">
                    <div class="option-wrapper">
                        <div class="option-header">
                            <?php if ($post_service_type == 'debetovye_karty') {
                                echo _e('Стоимость годового обслуживания', 'multi');
                            } else {
                                echo _e('Размер займа', 'multi');
                            }
                            ?>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('min', 'multi'); ?><br>

                                <input type="text" name="extra[razmer_zayma_min]"
                                       value="<?php echo $razmer_zayma_min; ?>"/>
                            </label>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('max', 'multi'); ?><br>
                                <input type="text" name="extra[razmer_zayma_max]"
                                       value="<?php echo $razmer_zayma_max; ?>"/>
                            </label>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('Точное значение', 'multi'); ?>
                                <input type="hidden" name="extra[razmer_zayma_single]" value="false"/>
                                <input type="checkbox" name="extra[razmer_zayma_single]"
                                       value="true" <?php checked(get_post_meta($post->ID, 'razmer_zayma_single', true), 'true'); ?> />
                            </label>
                        </div>
                    </div>

                    <div class="option-wrapper">
                        <div class="option-header">
                            <?php echo _e('Количество %', 'multi'); ?>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('min', 'multi'); ?><br>
                                <input type="text" name="extra[percents_min]" value="<?php echo $percents_min; ?>"/>
                            </label>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('max', 'multi'); ?><br>
                                <input type="text" name="extra[percents_max]" value="<?php echo $percents_max; ?>"/>
                            </label>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php echo _e('Точное значение', 'multi'); ?>
                                <input type="hidden" name="extra[percents_single]" value="false"/>
                                <input type="checkbox" name="extra[percents_single]"
                                       value="true" <?php checked(get_post_meta($post->ID, 'percents_single', true), 'true'); ?> />
                            </label>
                        </div>
                    </div>

                    <?php if ($post_service_type == 'mikrozaimy' || $post_service_type == 'credit_dlya_business' || $post_service_type == 'ipoteka' || $post_service_type == 'avtokredity') { ?>
                        <div class="option-wrapper">
                            <?php echo _e('Документ', 'multi'); ?><br>
                            <div class="option-single-value">
                                <label>
                                    <?php $doc = get_post_meta($post->ID, 'doc', true); ?>
                                    <select name="extra[doc]">
                                        <option value="">Выберите документ</option>
                                        <option <?php selected($doc, 'Справка о доходах'); ?> value="Справка о доходах">
                                            Справка о доходах
                                        </option>
                                        <option <?php selected($doc, 'По паспорту'); ?> value="По паспорту">По
                                            паспорту
                                        </option>
                                        <option <?php selected($doc, 'По 2м документам'); ?> value="По 2м документам">По
                                            2м документам
                                        </option>
                                        <option <?php selected($doc, 'ПТС'); ?> value="ПТС">ПТС</option>
                                        <option <?php selected($doc, 'Учредительные документы'); ?>
                                            value="Учредительные документы">Учредительные документы
                                        </option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type == 'kredity' || $post_service_type == 'creditnye_karty') { ?>
                        <div class="option-wrapper">
                            <?php echo _e('Подтверждение дохода', 'multi'); ?><br>
                            <div class="option-single-value">
                                <label>
                                    <?php $proof_of_income = get_post_meta($post->ID, 'proof_of_income', true); ?>
                                    <select name="extra[proof_of_income]">
                                        <option value="">Выберите значение</option>
                                        <option <?php selected($proof_of_income, 'Требуется'); ?> value="Требуется">
                                            Требуется
                                        </option>
                                        <option <?php selected($proof_of_income, 'Не требуется'); ?>
                                            value="Не требуется">Не требуется
                                        </option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type != 'debetovye_karty') { ?>
                        <div class="option-wrapper">
                            <div class="option-header">
                                <?php if ($post_service_type == 'kredity' || $post_service_type == 'avtokredity' || $post_service_type == 'credit_dlya_business') {
                                    echo _e('Срок (месяцев)', 'multi');
                                } elseif ($post_service_type == 'vklady' || $post_service_type == 'mikrozaimy' || $post_service_type == 'creditnye_karty') {
                                    echo _e('Срок (дней)', 'multi');
                                } elseif ($post_service_type == 'ipoteka') {
                                    echo _e('Срок (лет)', 'multi');
                                } else {
                                    echo _e('Срок', 'multi');
                                } ?>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('min', 'multi'); ?><br>
                                    <input type="text" name="extra[srok_min]" value="<?php echo $srok_min; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('max', 'multi'); ?><br>
                                    <input type="text" name="extra[srok_max]" value="<?php echo $srok_max; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('Точное значение', 'multi'); ?>
                                    <input type="hidden" name="extra[srok_single]" value="false"/>
                                    <input type="checkbox" name="extra[srok_single]"
                                           value="true" <?php checked(get_post_meta($post->ID, 'srok_single', true), 'true'); ?> />
                                </label>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type == 'kredity' || $post_service_type == 'creditnye_karty' || $post_service_type == 'mikrozaimy' || $post_service_type == 'credit_dlya_business' || $post_service_type == 'ipoteka' || $post_service_type == 'avtokredity') { ?>
                        <div class="option-wrapper">
                            <div class="option-header">
                                <?php echo _e('Возраст (лет)', 'multi'); ?>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('min', 'multi'); ?><br>
                                    <input type="text" name="extra[vozrast_min]" value="<?php echo $vozrast_min; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('max', 'multi'); ?><br>
                                    <input type="text" name="extra[vozrast_max]" value="<?php echo $vozrast_max; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('Точное значение', 'multi'); ?>
                                    <input type="hidden" name="extra[vozrast_single]" value=""/>
                                    <input type="checkbox" name="extra[vozrast_single]"
                                           value="true" <?php checked(get_post_meta($post->ID, 'vozrast_single', true), 'true'); ?> />
                                </label>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type == 'kredity' || $post_service_type == 'creditnye_karty' || $post_service_type == 'mikrozaimy' || $post_service_type == 'credit_dlya_business' || $post_service_type == 'ipoteka' || $post_service_type == 'avtokredity') { ?>
                        <div class="option-wrapper">
                            <div class="option-header">
                                <?php if ($post_service_type == 'mikrozaimy') {
                                    echo _e('Время одобрения (минут)', 'multi');
                                } elseif ($post_service_type == 'kredity' || $post_service_type == 'avtokredity' || $post_service_type == 'ipoteka' || $post_service_type == 'credit_dlya_business' || $post_service_type == 'creditnye_karty') {
                                    echo _e('Время одобрения (дней)', 'multi');
                                } else {
                                    echo _e('Время одобрения', 'multi');
                                } ?>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('min', 'multi'); ?><br>
                                    <input type="text" name="extra[time_min]" value="<?php echo $time_min; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('max', 'multi'); ?><br>
                                    <input type="text" name="extra[time_max]" value="<?php echo $time_max; ?>"/>
                                </label>
                            </div>
                            <div class="option-single-value">
                                <label>
                                    <?php echo _e('Точное значение', 'multi'); ?>
                                    <input type="hidden" name="extra[time_single]" value="false"/>
                                    <input type="checkbox" name="extra[time_single]"
                                           value="true" <?php checked(get_post_meta($post->ID, 'time_single', true), 'true'); ?> />
                                </label>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="option-wrapper">
                        <div class="option-header">
                            <?php echo _e('Валюта', 'multi'); ?>
                        </div>
                        <div class="option-single-value">
                            <label>
                                <?php $current_currency = get_post_meta($post->ID, 'currency_new', true); ?>
                                <select name="extra[currency_new]">
                                    <option value="">Выберите валюту</option>
                                    <option <?php selected( $current_currency ,CreditznatokConfig::Currency ); ?> value="<?php echo CreditznatokConfig::Currency?>"><?php echo CreditznatokConfig::CurrencySingle; ?></option>
                                    <option <?php selected($current_currency, 'usd'); ?> value="usd">Доллар</option>
                                    <option <?php selected($current_currency, 'eur'); ?> value="eur">Евро</option>
                                </select>
                            </label>
                        </div>
                    </div>

                    <?php
                    global $current_user;
                    $user_roles = $current_user->roles;
                    $user_role = array_shift($user_roles);

                    if ('editor' == $user_role) {
                        ?>
                        <input type="hidden" name="extra[link]"
                               value="<?php echo get_post_meta($post->ID, 'link', true); ?>"/>
                        <?php
                    } else {
                        ?>
                        <div class="option-wrapper">
                            <label><?php echo _e('Ссылка на оформление', 'multi'); ?><br/>
                                <input type="text" name="extra[link]"
                                       value="<?php echo get_post_meta($post->ID, 'link', true); ?>"/>
                            </label>
                        </div>
                    <?php } ?>

                    <div class="option-wrapper">
                        <label><?php echo _e('Текст для кнопки', 'multi'); ?><br/>
                            <input type="text" name="extra[text]"
                                   value="<?php echo get_post_meta($post->ID, 'text', true); ?>"/>
                        </label>
                    </div>

                    <?php if ($post_service_type == 'avtokredity' || $post_service_type == 'ipoteka') { ?>
                        <div class="option-wrapper">
                            <label><?php echo _e('Минимальный взнос', 'multi'); ?><br/>
                                <input type="text" name="extra[min_deposit]"
                                       value="<?php echo get_post_meta($post->ID, 'min_deposit', true); ?>"/>
                            </label>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type == 'creditnye_karty' || $post_service_type == 'debetovye_karty') : ?>
                        <div class="option-wrapper">
                            <label>
                                <?php echo _e('Тип карты', 'multi'); ?><br>
                                <?php $card_type = get_post_meta($post->ID, 'card_type', true); ?>
                                <select name="extra[card_type]">
                                    <option value="">Выберите валюту</option>
                                    <option <?php selected($card_type, 'visa'); ?> value="visa">Visa</option>
                                    <option <?php selected($card_type, 'master_card'); ?> value="master_card">
                                        MasterCard
                                    </option>
                                </select>
                            </label>
                        </div>
                    <?php endif; ?>

                    <?php if ($post_service_type == 'vklady') { ?>
                        <div class="option-wrapper">
                            <label>
                                <?php echo _e('Страхование вклада', 'multi'); ?>
                                <input type="hidden" name="extra[deposit_insurance]" value=""/>
                                <input type="checkbox" name="extra[deposit_insurance]"
                                       value="true" <?php checked(get_post_meta($post->ID, 'deposit_insurance', true), 'true'); ?> />
                            </label>
                        </div>

                        <div class="option-wrapper">
                            <label>
                                <?php echo _e('Досрочное закрытие', 'multi'); ?><br>
                                <?php $early_close = get_post_meta($post->ID, 'early_close', true); ?>
                                <select name="extra[early_close]">
                                    <option value="">Выберите тип</option>
                                    <option <?php selected($early_close, 'early_withdrawal'); ?>
                                        value="early_withdrawal">Досрочное расторжение
                                    </option>
                                    <!--<option <?php selected($early_close, 'early_dissolution'); ?> value="early_dissolution">Досрочное расторжение</option>-->
                                </select>
                            </label>
                        </div>
                    <?php } ?>

                    <?php if ($post_service_type == 'avtokredity') { ?>
                        <div class="option-wrapper">
                            <div class="option-single-value">
                                <?php echo _e('Вид ТС', 'multi'); ?>
                                <?php $vehicle_type = get_post_meta($post->ID, 'vehicle_type', true); ?>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[vehicle_type][]"
                                               value="Легковой транспорт" <?php if (in_array('Легковой транспорт', $vehicle_type)) echo 'checked="checked"'; ?> />
                                        Легковой транспорт
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[vehicle_type][]"
                                               value="Мототранспорт" <?php if (in_array('Мототранспорт', $vehicle_type)) echo 'checked="checked"'; ?> />
                                        Мототранспорт
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[vehicle_type][]"
                                               value="Коммерческий транспорт" <?php if (in_array('Коммерческий транспорт', $vehicle_type)) echo 'checked="checked"'; ?> />
                                        Коммерческий транспорт
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[vehicle_type][]"
                                               value="Водный транспорт" <?php if (in_array('Водный транспорт', $vehicle_type)) echo 'checked="checked"'; ?> />
                                        Водный транспорт
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[vehicle_type][]"
                                               value="Мотоцикл" <?php if (in_array('Мотоцикл', $vehicle_type)) echo 'checked="checked"'; ?> />
                                        Мотоцикл
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="option-wrapper">
                            <div class="option-single-value">
                                <?php echo _e('Тип ТС', 'multi'); ?>
                                <?php $manufacturer_type = get_post_meta($post->ID, 'manufacturer_type', true); ?>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[manufacturer_type][]"
                                               value="Иностранный новый" <?php if (in_array('Иностранный новый', $manufacturer_type)) echo 'checked="checked"'; ?> />
                                        Иностранный новый
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[manufacturer_type][]"
                                               value="Отечественный новый"<?php if (in_array('Отечественный новый', $manufacturer_type)) echo 'checked="checked"'; ?> />
                                        Отечественный новый
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[manufacturer_type][]"
                                               value="Иностранный подержанный" <?php if (in_array('Иностранный подержанный', $manufacturer_type)) echo 'checked="checked"'; ?> />
                                        Иностранный подержанный
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="extra[manufacturer_type][]"
                                               value="Отечественный подержанный" <?php if (in_array('Отечественный подержанный', $manufacturer_type)) echo 'checked="checked"'; ?> />
                                        Отечественный подержанный
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>

            </div>
            <div id="banks">
                <h3>О банке</h3>

                <label><?php echo _e('Актив', 'multi'); ?><br/>
                    <input type="text" name="extra[active]"
                           value="<?php echo get_post_meta($post->ID, 'active', 1); ?>"/></label><br>

                <p class="phone-opt">
                    <label><?php echo _e('Номер лицензии банка', 'multi'); ?><br/>
                        <input type="text" name="extra[licension]"
                               value="<?php echo get_post_meta($post->ID, 'licension', 1); ?>"/></label><br>

                    <label><?php echo _e('Головной офис', 'multi'); ?><br/>
                        <input type="text" name="extra[head_office]"
                               value="<?php echo get_post_meta($post->ID, 'head_office', 1); ?>"/></label><br>

                    <label><?php echo _e('Номера телефонов горячей линии', 'multi'); ?><br/>
                        <input type="text" name="extra[phones]"
                               value="<?php echo get_post_meta($post->ID, 'phones', 1); ?>"/></label><br>

                    <label><?php echo _e('Официальный сайт Сбербанка России', 'multi'); ?><br/>
                        <input type="text" name="extra[offsite]"
                               value="<?php echo get_post_meta($post->ID, 'offsite', 1); ?>"/></label><br>

                    <label><?php echo _e('Интернет банк', 'multi'); ?><br/>
                        <input type="text" name="extra[online_bank]"
                               value="<?php echo get_post_meta($post->ID, 'online_bank', 1); ?>"/></label><br>
                </p>

                <h3>Реквизиты</h3>

                <p class="phone-opt">
                    <label><?php echo _e('ОГРН', 'multi'); ?><br/>
                        <input type="text" name="extra[ogrn]"
                               value="<?php echo get_post_meta($post->ID, 'ogrn', 1); ?>"/></label><br>

                    <label><?php echo _e('ИНН', 'multi'); ?><br/>
                        <input type="text" name="extra[inn]" value="<?php echo get_post_meta($post->ID, 'inn', 1); ?>"/></label><br>

                    <label><?php echo _e('КПП', 'multi'); ?><br/>
                        <input type="text" name="extra[kpp]" value="<?php echo get_post_meta($post->ID, 'kpp', 1); ?>"/></label><br>

                    <label><?php echo _e('ОКПО', 'multi'); ?><br/>
                        <input type="text" name="extra[okpo]"
                               value="<?php echo get_post_meta($post->ID, 'okpo', 1); ?>"/></label><br>

                    <label><?php echo _e('БИК', 'multi'); ?><br/>
                        <input type="text" name="extra[bik]" value="<?php echo get_post_meta($post->ID, 'bik', 1); ?>"/></label><br>

                    <label><?php echo _e('SWIFT', 'multi'); ?><br/>
                        <input type="text" name="extra[swift]"
                               value="<?php echo get_post_meta($post->ID, 'swift', 1); ?>"/></label><br>

                    <label><?php echo _e('IBAN ', 'multi'); ?><br/>
                        <input type="text" name="extra[iban]"
                               value="<?php echo get_post_meta($post->ID, 'iban', 1); ?>"/></label><br>
                </p>
            </div>
        </div>
        <input type="hidden" name="need_check" value="review"/>
        <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>"/>
        <div style="clear:both;"></div>

        <?php
    }

    function meta_box_for_services($post)
    {
        ?>
        <div class="reviews">
            <table border="0">
                <tr>
                    <td><b>Процент одобрения</b></td>
                    <td> &mdash; <input type="text" value="<?php echo get_post_meta($post->ID, 'acceptance', true); ?>"
                                        name="acceptance"></td>
                </tr>
                <tr>
                    <td><b>Страница отзывов об услуге банка</b></td>
                    <td> &mdash; <span class="cp-create-reviews-page-wrap">
							<?php
                            $children = self::get_all_connected_posts('service_reviews', $post);
                            if ($children) { ?>
                                <a target="_blank"
                                   href="<?php echo admin_url() . "post.php?post={$children[0]->ID}&action=edit"; ?>"><?php echo $children[0]->post_title; ?></a>
                            <?php } else { ?>
                                не создана. <a href="#" class="cp-create-reviews-page"
                                               data-post-id="<?php echo $post->ID; ?>">Создать страницу отзывов</a>
                            <?php } ?>
						</span>
                    </td>
                </tr>
            </table>
        </div>
        <?
    }

    function meta_box_for_banks($post)
    {
        ?>
        <div class="reviews">
            <table border="0">
                <tr>
                    <td><b>Страница отзывов о банке</b></td>
                    <td> &mdash; <span class="cp-create-reviews-page-wrap">
							<?php
                            $children = self::get_all_connected_posts('bank_reviews', $post);
                            if ($children) { ?>
                                <a target="_blank"
                                   href="<?php echo admin_url() . "post.php?post={$children[0]->ID}&action=edit"; ?>"><?php echo $children[0]->post_title; ?></a>
                            <?php } else { ?>
                                не создана. <a href="#" class="cp-create-reviews-page"
                                               data-post-id="<?php echo $post->ID; ?>">Создать страницу отзывов</a>
                            <?php } ?>
						</span>
                    </td>
                </tr>
            </table>
        </div>
        <?
    }

    function creditznatok_connection_types()
    {
        foreach (CreditznatokConfig::$posts_relations as $post_type => $related_posts) {

            foreach ($related_posts as $related_post) {

                p2p_register_connection_type(array(
                    'name' => "{$post_type}_to_{$related_post}",
                    'from' => $post_type,
                    'to' => $related_post,
                    'admin_dropdown' => 'from'
                ));
            }
        }
    }

    function register_post_types()
    {
        //Тип записи МФО
        $labels = array(
            "name" => "МФО",
            "singular_name" => "МФО",
            "menu_name" => "МФО",
            "all_items" => "Все МФО",
            "add_new" => "Добавить новую",
            "add_new_item" => "Добавить новую МФО",
            "edit" => "Редактировать",
            "edit_item" => "Редактировать МФО",
            "new_item" => "Новая МФО",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => true,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => true,
            "rewrite" => array("slug" => "mfo", "with_front" => true),
            "query_var" => true,
            "supports" => array(
                "title",
                "revisions",
                "thumbnail",
                "page-attributes"
            ),
        );
        register_post_type("mfo", $args);

        //Тип записи Регионы
        $labels = array(
            "name" => "Регионы",
            "singular_name" => "Регионы",
            "menu_name" => "Регионы",
            "all_items" => "Все регионы",
            "add_new" => "Добавить новый",
            "add_new_item" => "Добавить новый регионы",
            "edit" => "Редактировать",
            "edit_item" => "Редактировать регион",
            "new_item" => "Новый регион",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => false,
            "show_ui" => true,
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => true,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => true,
            //"rewrite"             => array( "slug" => "", "with_front" => true ),
            "query_var" => true,
            "supports" => array(
                "title",
                'page-attributes',
            ),
        );
        register_post_type("regions", $args);

        // параметры
        $args = array(
            'public' => false,
            'show_in_nav_menus' => false, // равен аргументу public
            'show_ui' => false, // равен аргументу public
            'show_tagcloud' => false, // равен аргументу show_ui
            'hierarchical' => false,
            'update_count_callback' => '',
            'rewrite' => false,
            'capabilities' => array(),
            'meta_box_cb' => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
            'show_admin_column' => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
            '_builtin' => false,
            'show_in_quick_edit' => null, // по умолчанию значение show_ui
        );
        register_taxonomy('geoip_taxonomy', array('offers', 'services'), $args);
    }

    function creditznatok_clean_post_cache($post_id, $post = null)
    {
        if (!$post) {
            $post = get_post($post_id);
        }

        wp_cache_set("posts:{$post->post_type}:last_changed", microtime(), 'creditznatok');
    }

    public static function get_posts($post_type, $args = [])
    {

        $cache_key = md5(serialize($args));
        $last_changed = wp_cache_get("posts:$post_type:last_changed", 'creditznatok');

        if (!$last_changed) {
            $last_changed = microtime();
            wp_cache_set("posts:$post_type:last_changed", $last_changed, 'creditznatok');
        }

        $posts = wp_cache_get("posts:$post_type:$cache_key:$last_changed", 'creditznatok');

        if (false === $posts) {
            $defaults = array(
                'post_type' => $post_type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
            );

            $args = wp_parse_args($args, $defaults);
            $query = new WP_Query($args);
            $posts = $query->posts;

            wp_cache_set("posts:$post_type:$cache_key:$last_changed", $posts, 'creditznatok');

        }

        return $posts;
    }

    function creditznatok_get_query($post_type, $args = array())
    {
        $cache_key = md5(serialize($args));
        $last_changed = wp_cache_get("posts:$post_type:last_changed", 'creditznatok');

        if (!$last_changed) {
            $last_changed = microtime();
            wp_cache_set("posts:$post_type:last_changed", $last_changed, 'creditznatok');
        }

        $query = wp_cache_get("query:$post_type:$cache_key:$last_changed", 'creditznatok');

        if (false === $query) {
            $defaults = array(
                'post_type' => $post_type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
            );

            $args = wp_parse_args($args, $defaults);
            $query = new WP_Query($args);

            wp_cache_set("query:$post_type:$cache_key:$last_changed", $query, 'creditznatok');
        }

        return $query;
    }

    function creditznatok_save_data_from_meta_boxes($post_id, $post)
    {

        //сохранение процента одобрения на уровне услуги банков
        if ('services' == $post->post_type) {

            if (isset ($_POST['acceptance']) && $_POST['acceptance'] != '') {

                update_post_meta($post_id, 'acceptance', $_POST['acceptance']);
            }
        }
    }

    function creditznatok_add_meta_boxes()
    {
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

        if ($post_id) {
            add_meta_box('extra_fields', 'Дополнительные параметры', [$this, 'meta_box_for_offers'], 'offers', 'normal', 'high');
            add_meta_box('extra_fields', 'Дополнительные параметры', [$this, 'meta_box_for_services'], 'services', 'normal', 'high');
            add_meta_box('extra_fields', 'Дополнительные параметры', [$this, 'meta_box_for_banks'], 'banks', 'normal', 'high');

            add_meta_box('extf_sitetitle', 'Заголовок для сайта', [$this, 'extf_sitetitle'], 'services', 'normal');
            add_meta_box('extf_sitetitle', 'Заголовок для сайта', [$this, 'extf_sitetitle'], 'seo_groups', 'normal');
            add_meta_box('extf_sitetitle', 'Заголовок для сайта', [$this, 'extf_sitetitle'], 'offers', 'normal');
            add_meta_box('sort', 'Сортировка SEO групп', [$this, 'extf_seosort'], 'seo_groups', 'normal');
            add_meta_box('extf_params', 'Дополнительные параметры', [$this, 'get_extra_params_metabox'], 'offers', 'normal');
        }
    }

    public static function print_h1()
    {
        $post = get_post();
        ?>
            <h1><?php echo $post->post_title; ?></h1>
        <?php
        /*?>
        <div class="cp-post-header">
            <img class="alignnone size-full wp-image-86" width="48" height="48" alt="<?php echo $post->post_title; ?>"
                 src="<?php echo get_post_meta($post->ID, 'title_icon_link', 1); ?>">
            <h1><?php echo $post->post_title; ?></h1>
        </div>
        <div class="clearfix"></div>
        <?php*/
    }

    function get_extra_params_metabox($post)
    {
        $type = self::get_parent_service_type();
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
        if (isset(CreditznatokTabsUtility::$$type)) {
            $params = CreditznatokTabsUtility::$$type;
        } else {
            return;
        }

        $extf_meta = get_post_meta($post->ID, 'extf_meta', 1);
        ?>
        <h2>Дополнительные опции</h2>
        <table>
            <tr>
                <td width="300px">Процент одобрения</td>
                <td><input type="text" size="70" name="acceptance"
                           value="<?php echo get_post_meta($post->ID, 'acceptance', true); ?>"></td>
            </tr>
            <tr>
                <td width="300px">Ссылка на иконку</td>
                <td><input type="text" size="70" name="title_icon_link"
                           value="<?php echo get_post_meta($post->ID, 'title_icon_link', true); ?>"></td>
            </tr>
        </table>
        <?php foreach ($params as $group_title => $args) { ?>
        <h2><?php echo $group_title; ?></h2>
        <table>
            <?php foreach ($args as $args_title => $args_array) { ?>
                <tr>
                    <td width="300px"><?php echo $args_title; ?></td>
                    <td><?php $this->echo_extf_admin_param($args_array, $args_title, $group_title, $post_id, $type, $extf_meta); ?></td>
                </tr>
            <?php } ?>
        </table>
        <?php
    }
    }

    /**
     * Функция вывода метабокса редактирования параметра для админки
     * @param $args_array
     * @param $args_title
     * @param $group_title
     * @param $post_id
     * @param $type
     * @param $extf_meta
     */
    function echo_extf_admin_param($args_array, $args_title, $group_title, $post_id, $type, $extf_meta)
    {
        $value = isset ($extf_meta[$group_title][$args_title]) ? $extf_meta[$group_title][$args_title] : null;
        $class = isset($args_array['class']) ? $args_array['class'] : '';

        if ($args_title == 'Процент одобрения' && $value !== '0' && empty($value)) {
            $value = 10;
        }

        if ($type == 'kredity') {
            if ($args_title == 'В отделении банка' && is_null($extf_meta)) {
                $value = true;
            }
        }

        $checked = '';
        if ($value) {
            $checked = ' checked';
        }

        if ($type == 'mikrozaimy' && $group_title == 'Способ получения') {
            $checked = '';

            if ($value) {
                $checked = ' checked';
            }
        }

        switch ($args_array['type']) {
            case 'string': ?>
                <input class="<?php echo $class; ?>" type="text" size="70" name="extf_meta[<?php echo $group_title; ?>][<?php echo $args_title; ?>]"
                       value="<?php echo $value; ?>">
                <?php
                break;
            case 'checkbox': ?>
                <input class="<?php echo $class; ?>" type="checkbox"
                       name="extf_meta[<?php echo $group_title; ?>][<?php echo $args_title; ?>]" <?php echo $checked; ?>>
                <?php
                break;
            case 'way2get': ?>
                <input class="<?php echo $class; ?>" type="checkbox" value="<?php echo $args_title; ?>"
                       name="extf_meta[<?php echo $group_title; ?>][<?php echo $args_title; ?>]" <?php echo $checked; ?>>
                <?php
                break;
            case 'select': ?>
                <select class="<?php echo $class; ?>" name="extf_meta[<?php echo $group_title; ?>][<?php echo $args_title; ?>]">
                    <option value=''> -</option>
                    <?php foreach ($args_array['value'] as $key => $item) { ?>
                        <option <?php selected($key, $value); ?>><?php echo $key; ?></option>
                    <?php } ?>
                </select>
                <?php
                break;
            case 'table':
                $tn    = md5($args_title . $args_array['name']);
                $table = empty($value) ? get_option('table_' . $tn, '') : $value;

                wp_editor($table, $tn, array(
                    'wpautop'          => 0,
                    'media_buttons'    => 0,
                    'textarea_name'    => "extf_meta[$group_title][$args_title]",
                    'textarea_rows'    => 15,
                    'tabindex'         => null,
                    'editor_css'       => '',
                    'editor_class'     => $class,
                    'teeny'            => 0,
                    'dfw'              => 0,
                    'tinymce'          => 1,
                    'quicktags'        => 1,
                    'drag_drop_upload' => false
                ));
                break;
        }
    }

    function extf_save_hook()
    {
        $post = get_post();
        $type = self::get_parent_service_type();

        if (isset($_POST['sitetitle']) && $_POST['sitetitle'] != '') {
            update_post_meta($post->ID, 'sitetitle', $_POST['sitetitle']);
        }

        if (isset($_POST['title_icon_link']) && $_POST['title_icon_link'] != '') {
            update_post_meta($post->ID, 'title_icon_link', $_POST['title_icon_link']);
        }

        if (isset($_POST['acceptance'])) {
            update_post_meta($post->ID, 'acceptance', $_POST['acceptance']);
        }

        if (isset($_POST['seosort']) && $_POST['seosort'] != '') {
            update_post_meta($post->ID, 'seosort', $_POST['seosort']);
        }

        $extf_meta = $_REQUEST['extf_meta'];

        if ($type == 'mikrozaimy') {
            delete_post_meta($post->ID, 'way2get');

            foreach ($extf_meta['Способ получения'] as $name => $value) {
                add_post_meta($post->ID, 'way2get', $name);
            }
        }

        delete_post_meta($post->ID, 'extf_meta');
        add_post_meta($post->ID, 'extf_meta', $extf_meta, true);
    }

    function extf_sitetitle($post)
    {
        ?>
        <input type="text" name="sitetitle" value="<?php echo get_post_meta($post->ID, 'sitetitle', true); ?>" size=70>
        <?php
    }

    function extf_seosort($post)
    {
        ?>
        <input type="text" name="seosort" value="<?php echo get_post_meta($post->ID, 'seosort', true); ?>" size=70>
        <?php
    }

    public static function alt_post_title($post)
    {

        if (is_object($post)) {
            $post_id = $post->ID;
        } else {
            $post_id = $post;
        }

        if ($title = get_post_meta($post_id, 'sitetitle', true)) {

            return $title;
        } else {

            if (is_object($post)) {
                return $post->post_title;
            } else {
                return get_the_title($post_id);
            }
        }
    }

    public static function get_geoip_city_for_query()
    {
        if ( ! CreditznatokConfig::isGeoIpEnable) {
            return false;
        }

        if (isset ($_POST['city_filter']) && $_POST['city_filter']) {
            return $_POST['city_filter'];
        }

        $loc = CreditznatokGeoip::get_geo('loc');
        if ($loc) {
            return $loc;
        }

        $real_loc = CreditznatokGeoip::get_geo('real_loc');
        if ($real_loc && $real_loc != 'nogeoip') {
            return $real_loc;
        }

        if ($real_loc == 'nogeoip') {
            return false;
        }

        return false;

    }

    public static function get_meta_description()
    {
        $result = '';
        $description = '';

        $post = get_post();

        if (is_singular('mfo')) {
            if ($post->post_parent) {
                $parent_mfo  = get_post($post->post_parent);
                $address     = str_replace('Филиал в ', '', $post->post_title);
                $description = 'Информация об отделении МФО «' . $parent_mfo->post_title . '» в ' . $address . '. Адрес отделения, схема проезда.';
            } else {
                $description = 'Подробная информация о микрофинансовой организации «' . $post->post_title . '». Полное юридические наименование, дата внесения в государственный реестр.';
            }
        } elseif (is_post_type_archive('mfo')){
            $description = 'Каталог МФО: рейтинг, подробная информация. Фирмы, выдающие микрозаймы.';
        } elseif ($post->post_name == 'banki') {
            if (is_paged()) {
                $description = 'Выбрать банк на сайте CreditZnatok. Подробная информация о банках в одном каталоге.';
            }else{
                $description = 'Полный список кредитных организаций. Все банки России на сайте CreditZnatok.';
            }
        } elseif (is_singular('banks')){
            $description = 'Подробная информация о банке «' . $post->post_title . '». Вся информация о кредитных организациях на сайте CreditZnatok.';
        } elseif (is_singular('bank_data')){
            $parent_bank = CreditznatokSqlUtility::get_parent_bank('bank_data', $post);
            $description = 'Банковские реквизиты «' . $parent_bank->post_title . '». Полный перечень платежных реквизитов банков на сайте CreditZnatok.';
        } elseif (is_singular('bank_reviews')){
            $parent_bank = CreditznatokSqlUtility::get_parent_bank('bank_reviews', $post);
            $description = 'Мнения клиентов об обслуживании в банке «' . $parent_bank->post_title . '». Читайте отзывы о кредитных организациях на сайте CreditZnatok.';
        }

        if ($description) {
            $result = '<meta name="description" content="' . do_shortcode($description) . '" />';
        }

        return $result;
    }

    public static function get_post_name(WP_Post $post)
    {
        $post = $post ? $post : get_post();
        $meta = get_post_meta($post->ID, 'sitetitle', 1);

        return $meta ? $meta : $post->post_name;
    }

    public static function get_title()
    {
        $aoisp_title = false;
        if (class_exists('All_in_One_SEO_Pack')) {
            $aiosp = new All_in_One_SEO_Pack();
            global $aioseop_options;
            $post = $aiosp->get_queried_object();
            if ( ! empty($aioseop_options['aiosp_rewrite_titles'])) {
                $aoisp_title = $aiosp->get_aioseop_title($post);
                $aoisp_title = $aiosp->apply_cf_fields($aoisp_title);
            }
        }

        $post          = get_post();
        $title         = wp_get_document_title();
        $custom_title  = get_post_meta($post->ID, 'sitetitle', true);
        $all_one_title = get_post_meta($post->ID, '_aioseop_title', true);

        $display_title = $title;

        if (is_post_type_archive('mfo')) {
            if (is_paged()) {
                $display_title = 'Список микрофинансовых организаций | страница ' . get_query_var('paged');
            } else {
                $display_title = 'Список МФО в Москве, микрофинансовые организации Москвы.';
            }
        } elseif (is_singular('mfo')) {
            if ($post->post_parent) {
                $parent_mfo    = get_post($post->post_parent);
                $filial_city = get_the_title(get_field('city'));
                $filial_adress = get_field('address');
                $address       = str_replace('Филиал в ', '', $post->post_title);
                $display_title = 'Филиал МФО «' . $parent_mfo->post_title . '» в ' . $filial_city . ' по адресу ' . $filial_adress . ', информация об отделении';
            } else {
                if ($all_one_title) {
                    $display_title = $all_one_title;
                } else {
                    $display_title = 'МФО «' . $post->post_title . '»: рейтинг, регистрационный номер, юридический адрес, реквизиты, контакты';
                }
            }
        } elseif ($post->post_name == 'banki') {
            if (is_paged()) {
                $display_title = 'Перечень банков Москвы и России | страница ' . get_query_var('paged');
            } else {
                $display_title = 'Список банков Москвы и России, каталог, рейтинг банков';
            }
        } elseif (is_singular('banks')) {
            $display_title = 'Информация о банке «' . $post->post_title . '»: номер лицензии, адрес, телефон, сайт';
        } elseif (is_singular('bank_data')) {
            $parent_bank   = CreditznatokSqlUtility::get_parent_bank('bank_data', $post);
            $display_title = 'Реквизиты банка «' . $parent_bank->post_title . '»: КПП, ИНН, ОГРН, ОКПО, SWIFT, IBAN';
        } elseif (is_singular('bank_reviews')) {
            $parent_bank   = CreditznatokSqlUtility::get_parent_bank('bank_reviews', $post);
            $display_title = 'Отзывы клиентов банка «' . $parent_bank->post_title . '», оставить отзыв об услугах банка «' . $parent_bank->post_title . '»';
        } elseif ($aoisp_title) {
            $display_title = $aoisp_title;
        } elseif ($custom_title){
            $display_title = $custom_title;
        }

        return do_shortcode($display_title);
    }

    function reorder_comment_fields($fields)
    {
        $new_fields = []; // сюда соберем поля в новом порядке

        $myorder = ['author', 'email', 'url', 'comment']; // нужный порядок

        foreach ($myorder as $key) {
            $new_fields[$key] = $fields[$key];
            unset($fields[$key]);
        }

        // если остались еще какие-то поля добавим их в конец
        if ($fields) {
            foreach ($fields as $key => $val) {
                $new_fields[$key] = $val;
            }
        }

        return $new_fields;
    }

    public static function get_comment_title()
    {
        $title = 'Отзывы';
        $post  = get_post();

        if (in_array($post->post_type, ['offer_reviews', 'service_reviews'])) {
            $title = $post->post_title;
        }

        if (in_array($post->post_type, ['news', 'post'])) {
            $title = 'Комментарии';
        }

        return $title;
    }

    public static function get_comment_type()
    {
        $title = 'отзыв';
        $post  = get_post();

        if (in_array($post->post_type, ['news', 'post'])) {
            $title = 'комментарий';
        }

        return $title;
    }

    public static function comment_template($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        ?>
        <div <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
            <div class="comment-text">
                <?php comment_text(); ?>
            </div>
            <div class="comment-meta">
                    <span class="comment-author"><?php echo get_comment_author_link(); ?></span>
                    <span class="line-divider">|</span>
                    <span class="comment-date"><?php echo  get_comment_date(); ?></span>
            </div>
            <div class="cz-divider"></div>
        </div>
        <?php
    }

    /**
     * @param object $post
     * @param string $needed_post_type
     * @param bool $ids
     *
     * @return array
     */
    public static function get_parent_posts($post, $needed_post_type, $ids = false)
    {

        if (!is_object($post)) {
            $post = get_post($post);
        }

        $args = array(
            'post_type' => $needed_post_type,
            'connected_type' => "{$post->post_type}_to_{$needed_post_type}",
            'connected_items' => $post->ID,
            'connected_direction' => 'from',
            'post_per_page' => -1,
        );

        if ($ids) {
            $args['fields'] = 'ids';
        }

        $results = self::get_posts($needed_post_type, $args);

        if (count($results)) {
            return $results;

        } else {
            return array();
        }
    }

    public static function check_products_posts_by_meta_exists($meta_key, $meta_value)
    {
        $parent_post_id = Creditznatok::get_parent_service_type_id();
        $post = get_post($parent_post_id);
        $post_type = $post->post_type;

        $args = array(
            'post_type' => 'offers',
            'posts_per_page' => 1,
            'connected_type' => "offers_to_{$post_type}",
            'connected_items' => $post->ID,
            'post_status' => 'publish',
            'fields' => 'ids'
        );

        $custom_meta_query = false;

        switch ($meta_key) {
            case ('razmer_zayma_from'):
                $custom_meta_query = true;
                $args['meta_query'][] = array(
                    'key' => 'razmer_zayma_max',
                    'value' => $meta_value,
                    'type' => 'numeric',
                    'compare' => '>='
                );
                break;

            case ('srok'):
                $custom_meta_query = true;
                $args['meta_query'][] = array(
                    'relation' => 'AND',
                    array(
                        'key' => 'srok_min',
                        'value' => $meta_value,
                        'type' => 'numeric',
                        'compare' => '<='
                    ),
                    array(
                        'key' => 'srok_max',
                        'value' => $meta_value,
                        'type' => 'numeric',
                        'compare' => '>='
                    )
                );
                break;
        }

        if (!$custom_meta_query) {
            $args['meta_query'][] = array(
                'key' => $meta_key,
                'value' => $meta_value,
            );
        }

        $geoip_city = self::get_geoip_city_for_query();

        if ($geoip_city) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'geoip_taxonomy',
                    'field' => 'name',
                    'terms' => $geoip_city
                )
            );
        }

        $posts = self::get_posts('offers', $args);
        $results = $posts;

        if (count($results)) {
            return true;
        } else {
            return false;
        }

    }

    public static function get_connected_bank(WP_Post $post)
    {
        $type = self::get_parent_service_type();

        if ($type == 'mikrozaimy') {
            return $post;
        } else {
            $args    = array(
                'post_type'           => 'banks',
                'connected_type'      => "{$post->post_type}_to_banks",
                'connected_items'     => $post,
                'connected_direction' => 'from',
                'post_per_page'       => -1,
            );
            $results = self::get_posts('banks', $args);

            if (count($results)) {
                return $results[0];
            }
        }

        return null;

    }

    public static function get_bank($post_id)
    {
        $type = self::get_parent_service_type();

        if ($type == 'mikrozaimy') {
            return $post_id;
        } else {
            $args = array(
                'post_type' => 'banks',
                'connected_type' => 'offers_to_banks',
                'connected_items' => $post_id,
                'connected_direction' => 'from',
                'post_per_page' => -1,
                'fields' => 'ids',
            );
            $results = self::get_posts('banks', $args);

            if (count($results)) {
                return $results[0];
            }
        }

        return null;

    }

    public static function get_bank_from_array($array)
    {
        $args = array(
            'post_type' => 'banks',
            'connected_type' => "offers_to_banks",
            'connected_items' => $array,
            'connected_direction' => 'from',
            'post_per_page' => -1,
        );

        $results = self::get_posts('banks', $args);

        $temp = array();
        foreach ($results as $result) {
            $temp[$result->ID] = $result;
        }

        $results = $temp;

        return $results;
    }

    public static function get_services_from_array($array)
    {
        $args = array(
            'post_type' => 'services',
            'connected_type' => 'offers_to_services',
            'connected_items' => $array,
            'connected_direction' => 'from',
            'post_per_page' => -1,
        );

        $results = self::get_posts('banks', $args);

        $temp = array();
        foreach ($results as $result) {
            $temp[$result->ID] = $result;
        }

        $results = $temp;

        return $results;
    }

    /**
     * @param $need string needed post type
     * @param $parent string parent post type
     * @param $array array array of ids
     *
     * @return array|mixed
     */
    public static function get_seo_groups_from_array($array, $id_menu)
    {
        $args = array(
            'post_type' => 'seo_groups',
            'connected_type' => "offers_to_seo_groups",
            'connected_items' => $array,
            'connected_direction' => 'from',
            'post__in' => $id_menu,
            'posts_per_page' => -1,
            'meta_key' => 'seosort',
            'orderby' => 'meta_value_num'
        );

        $results = self::get_posts('seo_groups', $args);

        $temp = array();
        foreach ($results as $result) {
            $temp[$result->ID] = $result;
        }

        $results = $temp;

        return $results;
    }

    public static function get_all_banks($ids_only = true)
    {
        if (self::$all_banks) {
            return self::$all_banks;
        }

        $all_posts = CreditznatokSearchResults::get_all_search_results();
        $parent_service_type = self::get_parent_service_type();

        if ($parent_service_type == 'mikrozaimy') {
            if ($ids_only) {
                $results = $all_posts;
            } else {
                $args = array(
                    'post__in' => $all_posts,
                );
                $results = self::get_posts('offers', $args);
            }
        } else {
            $results = self::get_bank_from_array($all_posts);
        }

        self::$all_banks = $results;

        return $results;
    }

    public static function get_all_services($ids_only = true)
    {
        $all_posts = CreditznatokSearchResults::get_all_search_results();

        $parent_service_type = self::get_parent_service_type();

        if ($parent_service_type == 'mikrozaimy') {
            if ($ids_only) {
                $results = $all_posts;
            } else {
                $args = array(
                    'post__in' => $all_posts,
                );
                $results = self::get_posts('offers', $args);
            }
        } else {
            $results = self::get_services_from_array($all_posts);
        }

        return $results;
    }

    public static function mtranslit($text, $im = '-')
    {
        $iso = array(
            "Є" => "YE", "І" => "I", "Ѓ" => "G", "і" => "i", "№" => "#", "є" => "ye", "ѓ" => "g",
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
            "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
            "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
            "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
            "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "X",
            "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
            "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
            "е" => "e", "ё" => "yo", "ж" => "zh",
            "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "x",
            "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
            "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya"
        );

        preg_match_all("/[а-яa-z0-9]+/iu", $text, $words);
        $add_words = array();
        foreach ($words[0] as $word) {
            $word = strtr(strtolower($word), $iso);
            $add_words[] = $word;
        }

        return implode($im, $add_words);
    }

    public static function display_tabs()
    {

        $post             = get_post();
        $post_tabs_params = get_post_meta($post->ID, 'extf_meta', true);
        
        if ( ! $post_tabs_params) {
            return;
        }

       

            $type = self::get_parent_service_type(null, true);

            $tabs_params = CreditznatokTabsUtility::$$type;

            //удаление лишних и пустых элементов из маппинга
            foreach ($tabs_params as $param_name => $param_data) {
                $values_exists = false;

                foreach ($param_data as $name => $data) {

                    switch ($data['type']) {
                        case 'checkbox':
                            $values_exists = true;
                            break;
                        case 'way2get':
                            $values_exists = true;
                            break;
                        case 'string':
                            if (isset ($post_tabs_params[$param_name][$name]) && '' != $post_tabs_params[$param_name][$name]) {
                                $values_exists = true;
                            }

                            if (isset($data['visibility']) && 'hidden' == $data['visibility']) {
                                unset ($tabs_params[$param_name][$name]);
                            }
                            break;
                        case 'select':
                            if (isset ($post_tabs_params[$param_name][$name]) && '' != $post_tabs_params[$param_name][$name]) {
                                $values_exists = true;
                            }
                            break;
                        case 'table':
                            if (isset ($post_tabs_params[$param_name][$name]) && '' != $post_tabs_params[$param_name][$name]) {
                                $values_exists = true;
                            }
                            break;
                    }
                }

                if ( ! $values_exists) {
                    unset($tabs_params[$param_name]);
                }
            }

            $tabs_count = count($tabs_params);
            $i          = $j = 0;
            ?>

            <?php if ($tabs_count): ?>
            <div class="post-tab-wrapper">
                <ul class="nav nav-tabs tabs-count-<?php echo $tabs_count; ?>" role="tablist">
                    <?php foreach ($tabs_params as $param_name => $param_data) : ?>
                        <li role="presentation" <?php echo $i == 0 ? 'class="active"' : ''; ?>>
                            <a href="#<?php echo self::mtranslit($param_name); ?>" aria-controls="<?php echo self::mtranslit($param_name); ?>" role="tab" data-toggle="tab">
                                <div class="tab-cell text-center">
                                    <span><?php echo $param_name; ?>                            </span>
                                </div>
                            </a>
                        </li>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </ul>

                <div class="tab-content">
                    <?php foreach ($tabs_params as $param_name => $param_data) :
                        $bottom_params = ''; ?>
                        <div role="tabpanel" class="tab-pane <?php echo $j == 0 ? 'active' : ''; ?>" id="<?php echo self::mtranslit($param_name); ?>">
                            <?php foreach ($param_data as $single_param_name => $single_param_data) {
                                $offer_param = isset ($post_tabs_params[$param_name][$single_param_name]) ? $post_tabs_params[$param_name][$single_param_name] : null;

                                switch ($single_param_data['type']) {
                                    case 'checkbox':
                                        if ($offer_param) {
                                            $current_value = '<i class="cz-icon cz-yes"></i>';
                                        } else {
                                            $current_value = '<i class="cz-icon cz-no"></i>';
                                        };
                                        break;
                                    case 'way2get':
                                        if ($offer_param) {
                                            $current_value = '<i class="cz-icon cz-yes"></i>';
                                        } else {
                                            $current_value = '<i class="cz-icon cz-no"></i>';
                                        };
                                        break;
                                    case 'string':
                                        if ( ! $offer_param) {
                                            continue(2);
                                        }
                                        $current_value = $offer_param;
                                        break;
                                    case 'select':
                                        if ( ! $offer_param) {
                                            continue(2);
                                        }
                                        $current_value = $offer_param;
                                        break;
                                    case 'table':
                                        $bottom_params = $offer_param;
                                        continue(2);
                                        break;
                                    default:
                                        continue(2);
                                        break;
                                }
                                ?>

                                <div class="params-row">
                                    <div class="row v-center">
                                        <div class="col-xs-6 param-name"><?php echo $single_param_name; ?></div>
                                        <div class="col-xs-6"><?php echo $current_value; ?></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if ($bottom_params) { ?>
                                <h2>Процентные ставки</h2>
                                <?php echo $bottom_params; ?>
                            <?php } ?>
                        </div>

                        <?php $j++; ?>
                    <?php endforeach; ?>
                </div>

            </div>

            <?php endif; ?>
            <?php
    }
    /**
     * Replaces the meta boxes.
     *
     * @return void
     */
    public static function switch_boxes()
    {
        if ( ! post_type_supports( $GLOBALS['post']->post_type, 'excerpt' ) )
        {
            return;
        }
        remove_meta_box(
            'postexcerpt' // ID
            ,   ''            // Screen, empty to support all post types
            ,   'normal'      // Context
        );
        add_meta_box(
            'postexcerpt2'     // Reusing just 'postexcerpt' doesn't work.
            ,   __( 'Excerpt' )    // Title
            ,   array ( __CLASS__, 'show' ) // Display function
            ,   null              // Screen, we use all screens with meta boxes.
            ,   'normal'          // Context
            ,   'core'            // Priority
        );
    }
    /**
     * Output for the meta box.
     *
     * @param  object $post
     * @return void
     */
    public static function show( $post )
    {
        ?>
        <label class="screen-reader-text" for="excerpt"><?php
            _e( 'Excerpt' )
            ?></label>
        <?php
        // We use the default name, 'excerpt', so we don’t have to care about
        // saving, other filters etc.
        wp_editor(
            self::unescape( $post->post_excerpt ),
            'excerpt',
            array (
                'textarea_rows' => 15
            ,   'media_buttons' => FALSE
            ,   'teeny'         => TRUE
            ,   'tinymce'       => TRUE
            )
        );
    }
    /**
     * The excerpt is escaped usually. This breaks the HTML editor.
     *
     * @param  string $str
     * @return string
     */
    public static function unescape( $str )
    {
        return str_replace(
            array ( '&lt;', '&gt;', '&quot;', '&amp;', '&nbsp;', '&amp;nbsp;' )
            ,   array ( '<',    '>',    '"',      '&',     ' ', ' ' )
            ,   $str
        );
    }
}