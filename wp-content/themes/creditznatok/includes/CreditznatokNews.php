<?php

$crediznatokNews = new CreditznatokNews();

class CreditznatokNews
{
    public function __construct()
    {
        add_action('init', [$this, 'register_post_types']);

        //пока не показываем на боевых версиях
        if ( ! defined('CreditznatokConfig::isTestSite') || ! CreditznatokConfig::isTestSite) {
            return;
        }
    }

    public function register_post_types()
    {

        //Тип записи Баннер
        $labels = array(
            'name'          => 'Новости',
            'singular_name' => 'Новость',
            'menu_name'     => 'Новости',
            'all_items'     => 'Все новости',
            'add_new'       => 'Добавить новую',
            'add_new_item'  => 'Добавить новую новость',
            'edit'          => 'Редактировать',
            'edit_item'     => 'Редактировать новость',
            'new_item'      => 'Новая новость',
        );

        $args = array(
            'labels'              => $labels,
            'menu_icon'           => 'dashicons-media-spreadsheet',
            'description'         => '',
            'public'              => true,
            'show_ui'             => true,
            'has_archive'         => true,
            'show_in_menu'        => true,
            'exclude_from_search' => false,
            'capability_type'     => 'post',
            'map_meta_cap'        => true,
            'hierarchical'        => false,
            'supports'            => array(
                'title',
                'editor',
                'author',
                'excerpt',
                'comments',
                'revisions',
                'thumbnail',
                'page-attributes',
            ),
        );
        register_post_type('news', $args);
    }
}