<?php


add_action('wp_ajax_cp_ajax_casepress_update_all_posts', 'ajax_casepress_update_all_posts');


function ajax_casepress_update_all_posts()
{

    $current_page  = $_REQUEST['current_page'];
    $post_count    = $_REQUEST['post_count'];
    $post_type     = $_REQUEST['post_type'];
    $post_per_page = 1;

    set_time_limit(3000);

    $posts_types = array(
        'banks'            => 'Банки',
        'bank_data'        => 'Реквизиты банков',
        'bank_reviews'     => 'Отзывы о банках',
        'service_types'    => 'Типы услуг',
        'services'         => 'Услуги банков',
        'service_reviews'  => 'Отзывы об услугах банков',
        'seo_groups'       => 'СЕО-группировки',
        'seo_groups_child' => 'СЕО-группировки дочерние посты',
        'offers'           => 'Продукты',
        'offer_reviews'    => 'Отзывы о продуктах',
    );

    $defaults = array(
        'post_type'      => $post_type,
        'post_status'    => 'any',
        'posts_per_page' => $post_per_page,
        'paged'          => $current_page,
    );

    $query = new WP_Query($defaults);
    $posts = $query->posts;

    switch ($post_type) {

        case 'bank_data' :

            $parent_posts_types = array(
                'banks'
            );
            break;

        case 'bank_reviews' :

            $parent_posts_types = array(
                'banks'
            );
            break;

        case 'services' :

            $parent_posts_types = array(
                'service_types',
                'banks',
            );
            break;

        case 'service_reviews' :

            $parent_posts_types = array(
                'services',
            );
            break;

        case 'seo_groups' :

            $parent_posts_types = array(
                'service_types',
            );
            break;

        case 'seo_groups_child' :

            $parent_posts_types = array(
                'seo_groups',
            );
            break;

        case 'offers' :

            $parent_posts_types = array(
                'services',
                'service_types',
                'banks',
                'seo_groups',
                'seo_groups_child'
            );
            break;

        case 'offer_reviews' :

            $parent_posts_types = array(
                'offers',
            );
            break;
    }

    foreach ($posts as $post) {
        update_post_title_icon($post);

        foreach ($parent_posts_types as $parent_post_type) {

            /*$child_posts = child_posts( $post, 'offers' );
            echo '$child_posts<pre>';print_r($child_posts);echo '</pre>';*/

            $parent_posts_ids = Creditznatok::old_parent_posts($parent_post_type, $post, true);

            if ( ! empty($parent_posts_ids)) {

                foreach ($parent_posts_ids as $parent_posts_id) {

                    $p2p_type = p2p_type("{$post_type}_to_{$parent_post_type}")->connect($post->ID, $parent_posts_id,
                        array(
                            'date' => current_time('mysql')
                        ));
                }
            }


        }

    }

    $start = $current_page * $post_per_page;

    if ($post_count < $start) {
        wp_send_json('all_done');
    }

    $next_page = $current_page + 1;
    $response  = array('errors' => 'none', 'current_page' => $next_page);
    wp_send_json($response);

}

function update_post_title_icon($post)
{
    $c = $post->post_content;
    $c = btw('<div class="header">', '</div', $c);
    preg_match_all("/src=\"(.*?)\"/", $c, $img);
    $img = $img[1][0];

    preg_match_all("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>|s", $post->post_content,
        $result);

    $result      = preg_split("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>[\n\s]*?<div class=\"clearfix\"></div>|s",
        $post->post_content);
    $new_content = $result[1];

    if ( ! $new_content) {
        $result      = preg_split("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>|s",
            $post->post_content);
        $new_content = $result[1];
    }
    //delete_post_meta($post->ID, 'title_icon_link');
    add_post_meta($post->ID, 'title_icon_link', $img, 1);

    $post->post_content = $new_content ? $new_content : $post->post_content;
    wp_update_post($post);

    /*echo '------------------------------------------<br>

<pre>';
    echo $post->post_title;
    var_dump($new_content);
    echo '</pre>';*/

}

function update_all_post_title_icon()
{
    $posts = Creditznatok::get_posts('service_types');

    foreach ($posts as $post) {
        $c = $post->post_content;
        $c = btw('<div class="header">', '</div', $c);
        preg_match_all("/src=\"(.*?)\"/", $c, $img);
        $img = $img[1][0];

        preg_match_all("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>|s", $post->post_content,
            $result);

        $result      = preg_split("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>[\n\s]*?<div class=\"clearfix\"></div>|s",
            $post->post_content);
        $new_content = $result[1];

        if ( ! $new_content) {
            $result      = preg_split("|<div class=\"header\">.*?</div>[\n\s]*?<div class=\"header\">.*?</div>|s",
                $post->post_content);
            $new_content = $result[1];
        }
        //delete_post_meta($post->ID, 'title_icon_link');
        add_post_meta($post->ID, 'title_icon_link', $img, 1);

        $post->post_content = $new_content ? $new_content : $post->post_content;
        wp_update_post($post);

        /*echo '------------------------------------------<br>

    <pre>';
        echo $post->post_title;
        var_dump($new_content);
        echo '</pre>';*/
    }
}

// func=между
function btw($e1, $e2, $t, $p = 1)
{
    $t = explode($e1, $t);

    if ($p == -1) {
        $p = count($t) - 1;
    }
    $t = explode($e2, $t[$p]);

    return $t[0];
}