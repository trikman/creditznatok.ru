<?php

add_action( 'admin_menu', 'add_admin_page_update_all_posts' );

function casepress_load_jquery_in_admin_area() {
	wp_enqueue_script( 'jquery' );
}

add_action( 'admin_enqueue_scripts', 'casepress_load_jquery_in_admin_area' );

function add_admin_page_update_all_posts() {
	add_menu_page( 'Обновление всех постов', 'Обновление всех постов', 'manage_options', 'update_all_posts', 'casepress_update_all_posts' );
}

function casepress_update_all_posts() {

	$all_post_counts = array();
	$per_page = 1;

	$all_posts = array(
		//'banks'            => 'Банки',
		'bank_data'        => 'Реквизиты банков',
		'bank_reviews'     => 'Отзывы о банках',
		//'service_types'    => 'Типы услуг',
		'services'         => 'Услуги банков',
		'service_reviews'  => 'Отзывы об услугах банков',
		'seo_groups'       => 'СЕО-группировки',
		'seo_groups_child' => 'СЕО-группировки дочерние посты',
		'offers'           => 'Продукты',
		'offer_reviews'    => 'Отзывы о продуктах',
	);

	foreach ( $all_posts as $post_type => $post_name ) {

		$args = array(
			'post_type'      => $post_type,
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'fields'         => 'ids',
		);

		$query = new WP_Query( $args );
		$posts_ids = $query->posts;

		$all_post_counts[ $post_type ] = count( $posts_ids );
	}

	ob_start();

	foreach ( $all_post_counts as $post_type => $post_count ) {

		?>
		<div style="border: 1px solid red; padding: 20px; max-width: 400px;margin: 20px">

			<h2>Обновление типа поста <?php echo $all_posts[ $post_type ]; ?><br>
				( <?php echo $post_type; ?>)</h2>

			<form action="" class="form-<?php echo $post_type; ?>">

				Всего страниц: <?php echo ceil( $post_count / $per_page ); ?><br>
				Всего количество постов: <?php echo $post_count; ?><br>
				Номер страницы:<br>

				<input class="current-page" type="text" value="0">
				<input class="post-count" type="hidden" value="<?php echo $post_count; ?>">
				<input class="post-type" type="hidden" value="<?php echo $post_type; ?>">
				<br>
				Текущая обрабатываемая страница:<br>
				<input class="current-handled-page" type="text" disabled="disabled" value="0">
				<br><br><br>

				<input type="button" class="cp-submit-all-posts" value="СТАРТ">
			</form>
		</div>

		<script>
			var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';

			jQuery(document).ready(function ($) {

				$('.cp-submit-all-posts').click(function () {


					if ($('#ajax-doing-all-posts').length) {
						return;
					}

					$('body').append('<input type="hidden" id="ajax-doing-all-posts">');

					var form = $(this).closest('form');
					var post_type = form.find('.post-type').val();

					cp_ajax_start_all_posts( post_type );
				});

				function cp_ajax_start_all_posts( post_type, current_page = 0) {

					var form = $('.form-'+post_type);

					if (current_page == 0) {
						current_page = form.find('.current-page').val();
					}

					var post_count = form.find('.post-count').val();

					var data = {
						action      : 'cp_ajax_casepress_update_all_posts',
						post_type   : post_type,
						current_page: current_page,
						post_count  : post_count
					};

					//console.log ( data );

					jQuery.ajax({
						type   : "POST",
						url    : ajaxurl,
						data   : data,
						success: function (response) {

							if (response.errors == 'none' && response.current_page) {
								form.find('.current-handled-page').val(response.current_page);

								cp_ajax_start_all_posts( post_type, response.current_page);

							} else if (response == 'all_done') {
								alert('all done!');
								$('#ajax-doing-all-posts').remove();
							}

						},
						error  : function () {
							alert('error!');
							$('#ajax-doing-all-posts').remove();
						}
					});

				}

			});
		</script>

		<?php

	}

	echo ob_get_clean();
}
