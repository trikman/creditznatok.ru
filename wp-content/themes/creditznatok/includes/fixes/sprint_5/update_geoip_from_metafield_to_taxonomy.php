<?php

add_action( 'admin_menu', 'add_admin_page_update_geoip' );

function casepress_load_jquery_in_admin_area() {
	wp_enqueue_script( 'jquery' );
}

add_action( 'admin_enqueue_scripts', 'casepress_load_jquery_in_admin_area' );

function add_admin_page_update_geoip() {
	add_menu_page( 'Обновление Geo-ip', 'Обновление Geo-ip', 'manage_options', 'update_geoip', 'casepress_update_geoip' );
}

function casepress_update_geoip() {
	ob_start();

	$defaults = array(
		'post_type'      => 'offers',
		'post_status'    => 'any',
		'posts_per_page' => -1,		
		'fields'         => 'ids',
	);


	/*$defaults = array(
		'post_type'      => 'services',
		'post_status'    => 'any',
		'posts_per_page' => -1,
		'fields'         => 'ids',
	);*/

	$query = new WP_Query( $defaults );
	$posts = $query->posts;

	?>
	<div style="border: 1px solid red; padding: 20px; max-width: 400px;margin: 20px">
		<h2>Обновление гео-айпи</h2>
		<form action="">
			Всего страниц: <?php echo round( count( $posts ) / 10 ); ?><br>
			Номер страницы:<br>
			<input class="current_page_offers" type="text">
			<input class="all_offers" type="hidden" value="<?php echo count( $posts ); ?>">
			<br>
			Текущая обрабатываемая страница:<br>
			<input class="current_handled_page-offers" type="text" disabled="disabled" value="0">
			<br><br><br>
			<input type="button" id="cp-submit-offers" value="СТАРТ">
		</form>
	</div>

	<script>
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

		jQuery(document).ready(function ($) {

			$('#cp-submit-offers').click(function () {
				if ($('#ajax_doing_offers').length) {
					return;
				}
				$('body').append('<input type="hidden" id="ajax_doing_offers">');
				cp_ajax_offers_start();
			});

			function cp_ajax_offers_start( current_page = 0 ) {

				if ( current_page == 0 ) {
					current_page = $('.current_page_offers').val();
				}

				var data = {
					action      : 'cp_ajax_casepress_update_geoip',
					current_page: current_page,
					all_offers  : $('.all_offers').val()
				};

				jQuery.ajax({
					type   : "POST",
					url    : ajaxurl,
					data   : data,
					success: function (response) {

						if (response.errors == 'none' && response.current_page) {
							//alert(response.current_page);
							$('.current_handled_page-offers').val(response.current_page);
							cp_ajax_offers_start(response.current_page);

						} else if (response == 'all_done') {
							alert('all done!');
							$('#ajax_doing_offers').remove();
						}

					},
					error  : function () {
						alert ('error!');
						$('#ajax_doing_offers').remove();
					}
				});
			}

		});
	</script>

	<?php
	echo ob_get_clean();
}


add_action( 'wp_ajax_cp_ajax_casepress_update_geoip', 'cp_ajax_casepress_update_geoip' );
function cp_ajax_casepress_update_geoip( ) {

	$current_page = $_REQUEST['current_page'];
	$all_offers = $_REQUEST['all_offers'];

	$post_per_page = 10;

	$start = $current_page * $post_per_page;

	if (  $all_offers < $start ) {
		wp_send_json ( 'all_done' );
	}

	set_time_limit ( 3000 );
	$defaults = array(
		'post_type'      => 'offers',
		'post_status'    => 'any',
		'posts_per_page' => $post_per_page,
		'paged'          => $current_page,
		'fields'         => 'ids',
	);

	$query = new WP_Query( $defaults );
	$posts = $query->posts;


	/*$defaults = array(
		'post_type'      => 'services',
		'post_status'    => 'any',
		'posts_per_page' => $post_per_page,
		'paged'          => $current_page,
		'fields'         => 'ids',
	);

	$query = new WP_Query( $defaults );
	$posts = $query->posts;*/

	$geoip_cities = get_terms( 'geoip_taxonomy' );
	$geo_cities = array();

	foreach ( $geoip_cities as $city_term ) {
		$geo_cities[ $city_term->name ] = $city_term->term_id;
	}

	foreach ( $posts as $post_id ) {

		update_geoip_metafield_to_taxonomy ( $post_id, $geo_cities );
	}

	$next_page    = $current_page + 1;
	$response = array( 'errors' => 'none', 'current_page' => $next_page );
	wp_send_json ($response);

}

function update_geoip_metafield_to_taxonomy ( $post_id, $geo_cities ) {

	$geoip = get_post_meta ( $post_id, 'geoip_meta', true);

	if ( $geoip ) {
		$post_terms = array();

		foreach ( $geoip as $city => $one ) {

			$post_terms[] = (int)$geo_cities[$city];
		}

		wp_set_object_terms ( $post_id, $post_terms, 'geoip_taxonomy' );
	}

}