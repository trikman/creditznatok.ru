<?php

//begin 5 спринт:

//include_once( 'fix_bank_rewiews_canonical.php' ); //фикс канонических элементов страницы отзывов о банках

//include_once( 'delete_old_meta_fields.php' );     //удаление старых мета полей после 4 спринта
//include_once( 'update_products_doc_field.php' );  //обновление мета поля doc для продуктов, приведение к одному типу и удаление ненужных
//include_once( 'update_geoip_from_metafield_to_taxonomy.php' );  //перенос значения geoip из метаполя в таксономию

//end 5 спринт

//update_products_min_max_fields();
function update_products_min_max_fields() {
	$args = array(
		'post_type'      => 'offers',
		'posts_per_page' => - 1,
		'post_status'    => 'any'
	);

	$posts = Creditznatok::get_posts( 'offers', $args );

	$array_min = array(
		'razmer_zayma_min',
		'percents_min',
		'srok_min',
		'vozrast_min',
		'time_min'
	);

	$array_max = array(
		'razmer_zayma_max',
		'percents_max',
		'srok_max',
		'vozrast_max',
		'time_max'
	);

	foreach ( $posts as $post ) {

		foreach ( $array_min as $min_key ) {
			$meta = get_post_meta( $post->ID, $min_key, true );

			if ( $meta === '' ) {
				update_post_meta( $post->ID, $min_key, 0 );
			}
		}

		foreach ( $array_max as $max_key ) {
			$meta = get_post_meta( $post->ID, $max_key, true );

			if ( $meta === '' ) {
				update_post_meta( $post->ID, $max_key, 999999999 );
			}
		}
	}
}