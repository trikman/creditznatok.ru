<?php

if ( current_user_can ('update_core')) {
	$docs = unique_doc_values();
	if ( $docs ) {
		echo '<pre>';print_r($docs); echo '</pre>';
	}
}

function unique_doc_values () {

	$result = false;

	$args = array(
		'post_type'      => 'offers',
		'post_status'    => 'any',
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $args );
	$posts = $query->posts;

	$docs = array();

	foreach ( $posts as $post ) {
		$doc = get_post_meta ( $post->ID, 'doc', true );
		$docs[] = $doc;

		switch ( $doc ) {
			case 'По 2ум документам':
				update_post_meta ( $post->ID, 'doc', 'По 2м документам' );
				break;
			case 'Паспорт РФ':
				update_post_meta ( $post->ID, 'doc', 'По паспорту' );
				break;
			case 'По паспорту':
				break;
			case 'По 2м документам':
				break;
			case 'ПТС':
				break;
			case 'Справка о доходах':
				break;
			case 'Учредительные документы':
				break;
			default:
				delete_post_meta ( $post->ID, 'doc');
		}

	}

	$result = array_unique( $docs );

	return $result;
}
