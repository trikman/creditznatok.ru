##------------------kz only-----------------------------
DELETE FROM wp_term_taxonomy
WHERE taxonomy = 'geoip_taxonomy';

DELETE
FROM `wp_term_relationships`
WHERE term_taxonomy_id NOT IN (1, 20, 7, 3978, 26);

DELETE
FROM `wp_posts`
WHERE post_type != 'service_types'
			AND post_type != 'acf'
			AND post_type != 'page'
			AND post_type NOT LIKE 'acf%';
##------------------------------------------------------


DELETE FROM wp_term_taxonomy
WHERE taxonomy LIKE 'tax_%';

DELETE FROM wp_options
WHERE option_name LIKE 'wpcc%';

DROP TABLE `wp_ak_404_log`, `wp_blc_filters`, `wp_blc_instances`, `wp_blc_links`, `wp_blc_synch`, `wp_creator_calculator`, `wp_switch`;