##Запрос для получения списка постов, имеющих какой-то текст перед шорткодом [products]
SELECT count(*)
FROM wp_posts
WHERE
	LEFT(post_content, 30) NOT LIKE '%[products]%' AND post_content LIKE '%[products]%'
	AND post_status IN ('publish', 'private');

##Запрос для выборки постов, имеющих <!--after-params--> не в начале поста
SELECT count(*)
FROM wp_posts
WHERE
	LEFT(post_content, 50) NOT LIKE '%<!--after-params-->%' AND post_content LIKE '%<!--after-params-->%'
	AND post_status IN ('publish', 'private');
