<?php

set_time_limit ( 300 );

add_action ( 'wp', 'creditznatok_delete_old_meta_fields');
function creditznatok_delete_old_meta_fields() {

	if ( ! current_user_can ('update_core')) {
		return;
	}

	$defaults = array(
		'post_type'      => 'offers',
		'post_status'    => 'any',
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $defaults );
	$posts = $query->posts;

	//echo '<pre>';print_r(count($posts));echo '</pre>';

	$metas = array(
		'razmer_zayma',
		'percents',
		'srok',
		'vozrast',
		'time',
		'currency',
	);

	foreach ( $posts as $post ) {

		foreach ( $metas as $meta ) {

			delete_post_meta( $post->ID, $meta );
		}
	}
}