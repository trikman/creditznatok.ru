<?php
set_time_limit( 300 );
//add_action ( 'shutdown', 'creditznatok_bank_rewiews_fix');

//add_action( 'shutdown', 'creditznatok_temp_fix' );

function creditznatok_temp_fix() {
	$data = array(
		/*32545,
		32548,
		32541,*/
		32551,
		32554,
		32557,
		32560,
	);

	return;

	if ( current_user_can( 'update_core' ) ) {

		echo '<br>' . 'начало обновления';

		foreach ( $data as $post_id ) {

			//delete_post_meta ( $post_id, 'tax_term_id');

			crediznatok_do_post_sync( $post_id );
			echo '<br>' . $post_id . ' обновлен';

		}

	}

	/*crediznatok_do_post_sync ( 32557 );
	crediznatok_do_post_sync ( 32554 );
	crediznatok_do_post_sync ( 32551 );
	crediznatok_do_post_sync ( 32560 );
	crediznatok_do_post_sync ( 32541 );*/
}

function creditznatok_bank_rewiews_fix() {

	if ( ! current_user_can( 'update_core' ) ) {
		return;
	}

	$defaults = array(
		'post_type'      => 'bank_reviews',
		'post_status'    => 'any',
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $defaults );
	$posts = $query->posts;

	foreach ( $posts as $post ) {
		crediznatok_do_post_sync( $post->ID );
		echo '<br>' . $post->ID . ' обновлен';
	}
}

//копия функции do_post_sync, используемая для перелинковки постов между собой, без проверки на $_SERVER['rec'], хотя для чего она вообще нужна непонятно
function crediznatok_do_post_sync( $post_id ) {
	global $structure;

	// Сначала определяем, нужен ли этот тип поста
	$post = get_post( $post_id );

	//echo '$structure<pre>';print_r($structure);echo '</pre>';
	//echo '$post<pre>';print_r($post);echo '</pre>';

	if ( isset( $structure[ $post->post_type ] ) ) {
		$taxonomy = 'tax_' . $post->post_type;

		// Затем определяем, что именно делается с постом
		if ( $post->post_status == 'auto-draft' ) {        // автодрафт при создании нового поста
			// ничего не делаем
		}

		if ( $post->post_status == 'publish' || $post->post_status == 'pending' || $post->post_status == 'private' ) {        // обновили или добавили новый (проверка внутри)

			if ( $term_id = get_post_meta( $post_id, 'tax_term_id', true ) ) {
				// Обновление

				//echo '$structure<pre>';print_r($structure);echo '</pre>';
				//echo '$term_id<pre>';print_r($term_id);echo '</pre>';

				$post->post_name = preg_replace( "/\-[0-9]$/", "", $post->post_name );
				wp_update_term( $term_id, $taxonomy, array( 'name' => $post->post_title, 'slug' => $post->post_name ) );

				// проверка
				$term = get_term( $term_id, $taxonomy );

				if ( $term->slug != $post->post_name ) {    // проблема уникальности термов, согласно костылю, запихиваем псевдоуникальный терм

					$parents  = get_all_parents( $post );
					$new_slug = "";

					foreach ( $parents as $parent ) {
						$new_slug .= "{$parent->post_name}-l-";
					}

					$new_slug .= $post->post_name;

					// пытаемся подсунуть этот терм
					wp_update_term( $term_id, $taxonomy, array( 'name' => $post->post_title, 'slug' => $new_slug ) );
					$term           = get_term( $term_id, $taxonomy );
					$_SERVER['rec'] = 1;
					if ( $term->slug != $new_slug ) {
						// нифига - ладно, присваиваем посту текущий slug терма
						wp_update_post( array( 'ID' => $post->ID, 'post_name' => $term->slug ) );
					} else {
						// получилось - пост обновляем тоже
						wp_update_post( array( 'ID' => $post->ID, 'post_name' => $new_slug ) );
					}

				}

			} else {

				// Требуется добавить метку для нового поста
				// 1. Добавляем метку

				$post->post_name = preg_replace( "/\-[0-9]$/", "", $post->post_name );

				$term = wp_insert_term( $post->post_title, $taxonomy, array( 'slug' => $post->post_name ) );

				if ( is_object( $term ) ) {
					// ошибка - такое сочетание title и slug уже есть, делаем просто метку с левым slug
					$term = wp_insert_term( $post->post_title, $taxonomy, array( 'slug' => $post->post_name . mt_rand( 0, 999999 ) ) );
				}
				$term_id = $term['term_id'];

				// проверка -- та же
				$term = get_term( $term_id, $taxonomy );

				if ( $term->slug != $post->post_name ) {    // проблема уникальности термов, согласно костылю, запихиваем псевдоуникальный терм
					$parents  = get_all_parents( $post );
					$new_slug = "";

					foreach ( $parents as $parent ) {
						$new_slug .= "{$parent->post_name}-l-";
					}
					$new_slug .= $post->post_name;

					// пытаемся подсунуть этот терм
					wp_update_term( $term_id, $taxonomy, array(
						'name' => $post->post_title,
						'slug' => $new_slug
					) );
					$term = get_term( $term_id, $taxonomy );

					$_SERVER['rec'] = 1;
					if ( $term->slug != $new_slug ) {
						// нифига - ладно, присваиваем посту текущий slug терма
						wp_update_post( array( 'ID' => $post->ID, 'post_name' => $term->slug ) );
					} else {
						// получилось - пост обновляем тоже
						wp_update_post( array( 'ID' => $post->ID, 'post_name' => $new_slug ) );
					}

				}

				// да уж на всякий уж
				$term = get_term( $term_id, $taxonomy );

				// 2. Линковка таксономия => пост
				wp_set_object_terms( $post_id, $term->slug, $taxonomy, true );

				// 3. Линковка пост => таксономия
				update_post_meta( $post_id, 'tax_term_id', $term_id );
			}
		}

		if ( $post->post_status == 'trash' ) {            // пост удаляется
			// Удаляем метку
			$term_id = get_term_by( 'name', $post->post_title, $taxonomy );
			wp_delete_term( $term_id->term_id, $taxonomy );
			delete_post_meta( $post_id, 'tax_term_id' );

		}

	}
}