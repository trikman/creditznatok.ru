<?php

class CreditznatokWidgets
{
    static private $banks_for_widget = null;

    /**
     *    Функция вывода инфовиджета
     */
    public static function bank_info()
    {
        $post = get_post();
        if ($post->post_type == 'banks') {
            $bank['bank'] = $post;
        } elseif ($post->post_type == 'service_reviews') {
            $parent_services = Creditznatok::get_parent_posts($post, 'services');
            $bank['bank']    = get_post(Creditznatok::get_connected_bank($parent_services[0]));
        } else {
            $bank['bank'] = get_post(Creditznatok::get_bank($post));
        }

        if (isset($bank['reviews'][0]) && $bank['reviews'][0]) {
            $bank['reviews'] = $bank['reviews'][0];
        }
        if (isset($bank['data'][0]) && $bank['data'][0]) {
            $bank['data'] = $bank['data'][0];
        }

        if ($bank['bank'] && ($bank['bank']->ID != $post->ID || $post->post_type == 'banks')) { ?>
            <div id="bank-info" class="cz-block-white">

                <div class="cp-bank-wrapper">
                    <?php $post_thumbnail_id = get_post_thumbnail_id($bank['bank']->ID); ?>
                    <?php $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id); ?>
                    <?php if ($post_thumbnail_url): ?>
                        <div class="cp-bank-item">
                            <img src="<?php echo $post_thumbnail_url; ?>" class="img-responsive">
                        </div>
                    <?php endif; ?>

                    <?php $post_titile = $bank['bank']->post_title; ?>
                    <?php if ($post_titile) : ?>
                        <div class="cp-bank-item">
                            <div class="cp-bank-item-title bold">
                                Название
                            </div>
                            <div class="cp-bank-item-content">
                                <?php echo $post_titile; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php /*$rating = self::_html_rating_stars($bank['reviews']->ID, 'small'); ?>
                    <?php if ($rating) : ?>
                        <div class="cp-bank-item">
                            <div class="cp-bank-item-title bold">
                                Рейтинг
                            </div>
                            <div class="cp-bank-item-content">
                                <?php echo $rating; ?>
                            </div>
                        </div>
                    <?php endif; */ ?>

                    <?php $licension = get_post_meta($bank['bank']->ID, 'licension', true); ?>
                    <?php if ($licension) : ?>
                        <div class="cp-bank-item">
                            <div class="cp-bank-item-title bold">
                                Номер лицензии
                            </div>
                            <div class="cp-bank-item-content">
                                <?php echo $licension; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php $phones = get_post_meta($bank['bank']->ID, 'phones', true); ?>
                    <?php if ($phones) : ?>
                        <div class="cp-bank-item">
                            <div class="cp-bank-item-title bold">
                                Номер телефона
                            </div>
                            <div class="cp-bank-item-content">
                                <?php echo $phones; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php $head_office = get_post_meta($bank['bank']->ID, 'head_office', true); ?>
                    <?php if ($head_office) : ?>
                        <div class="cp-bank-item">
                            <div class="cp-bank-item-title bold">
                                Адрес головного офиса
                            </div>
                            <div class="cp-bank-item-content">
                                <?php echo $head_office; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php $post_permalink = get_permalink($bank['bank']->ID); ?>
                    <?php if ($post_permalink) : ?>
                        <div class="cp-bank-item bank-links">
                            <a href="<?php echo Creditznatok::get_bank_reviews_link($bank['bank']); ?>">Отзывы</a>
                            <a href="<?php echo Creditznatok::get_bank_data_link($bank['bank']); ?>">Реквизиты</a>
                            <a href="<?php echo $post_permalink; ?>">Информация о банке</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php
        }

    }

    public function _html_rating_stars($reviews_post_id)
    {
        $rating = self::_get_bank_rating($reviews_post_id);
        //$rating = get_post_meta($reviews_post_id,'crfp-total-ratings',TRUE); // так проще, но нет кол-ва отзывов
        $r    = floor($rating['r']);
        $html = "";
        $uri  = get_template_directory_uri() . '/includes/img/icons';
        for ($i = 0; $i < $r; $i++) {
            $html .= "<img src='$uri/s1.png'>";
        }
        for ($i = 0; $i < 5 - $r; $i++) {
            $html .= "<img src='$uri/s0.png'>";
        }

        return $html . " ({$rating['c']} голосов)";
    }

    public static function _get_bank_rating($reviews_post_id)
    {

        $result = array('r' => 0, 'c' => 0);

        if ($reviews_post_id) {
            $comments = get_comments(array('post_id' => $reviews_post_id));

            $rating = 0;
            foreach ($comments as $comment) {
                $rating += get_comment_meta($comment->comment_ID, 'crfp-rating', true);
            }
            if (count($comments)) {
                $rating /= count($comments);
            }
            $result = array('r' => $rating, 'c' => count($comments));
        }

        return $result;
    }

    public static function display_all_sidebar_menu($mobile = false)
    {
        $menus      = [];
        $menu_owner = Creditznatok::get_parent_service_type_id();

        for ($i = 1; $i <= 5; $i++) {
            $menus[$i]['menu']       = get_field('menu_' . $i, $menu_owner);
            $menus[$i]['menu_name']  = get_field('menu_' . $i . '_name', $menu_owner);
            $menus[$i]['menu_state'] = get_field('menu_' . $i . '_state', $menu_owner);
        }

        ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php
            for ($i = 1; $i <= count($menus); $i++) {
                self::display_sidebar_menu($menus[$i]['menu'], $menus[$i]['menu_name'], $menus[$i]['menu_state']);
            }

            $banks      = self::get_banks_for_widget();
            $bank_state = get_field('bank_state', $menu_owner);

            if ( ! empty($banks)) {
                ?>
                <div class="widget-wrapper">
                    <div class="panel panel-default">
                        <div class="panel-heading <?php echo $bank_state ? '' : 'collapsed'; ?>" role="tab" id="headingOne" data-toggle="collapse" data-target="#bank-list" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
                            <div class="panel-title">
                                <span class="widget-title widget-<?php echo 'creditnye_karty' == Creditznatok::get_parent_service_type() ? 'card' : 'bank'; ?>">Банк</span>
                            </div>
                        </div>
                        <div id="bank-list" class="panel-collapse collapse <?php echo $bank_state ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <ul class="bank-list double-column cz-ul">
                                    <?php echo $banks; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php

    }


    /**
     *    Автоматизация виджета банков
     */
    public static function get_banks_for_widget()
    {
        $results = Creditznatok::get_all_services();
        $html    = '';

        $parent_service_type = Creditznatok::get_parent_service_type();
        if ('mikrozaimy' == $parent_service_type) {
            foreach ($results as $post_id) {
                $html .= '<li class="menu-item"><a href="' . get_permalink($post_id) . '">' . Creditznatok::alt_post_title($post_id) . '</a></li>';
            }
        } else {
            foreach ($results as $post) {
                $html .= '<li class="menu-item"><a href="' . get_permalink($post->ID) . '">' . Creditznatok::alt_post_title($post) . '</a></li>';
            }
        }

        self::$banks_for_widget = $html;

        return $html;
    }

    /**
     * Отображает блок в сайдбаре (типо виджета)
     */
    public static function display_sidebar_menu($menu, $menu_name, $menu_state)
    {
        if ($menu) {
            $id_menu = array();

            //сбор данных по элементам меню в массив
            foreach ($menu as $menu_item) {
                if (is_object($menu_item)) {
                    array_push($id_menu, $menu_item->ID);
                } else {
                    array_push($id_menu, $menu_item);

                }
            }

            $ul_menu_id    = 'menu-id-' . Creditznatok::mtranslit($menu_name);
            $li_menu_class = 'menu-item'; ?>
            <div class="widget-wrapper">
                <div class="panel panel-default">
                    <div class="panel-heading <?php echo $menu_state ? '' : 'collapsed'; ?>" role="tab" id="headingOne" data-toggle="collapse" data-target="#<?php echo $ul_menu_id; ?>" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
                        <div class="panel-title">
                            <span role="button">
                                <?php echo $menu_name; ?>
                            </span>
                        </div>
                    </div>
                    <div id="<?php echo $ul_menu_id; ?>" class="panel-collapse collapse <?php echo $menu_state ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $ul_menu_id; ?>">
                        <div class="panel-body">
                            <ul class="<?php echo $ul_menu_id; ?> cz-ul double-column">
                                <?php
                                $search_results = CreditznatokSearchResults::get_all_search_results();
                                $posts          = Creditznatok::get_seo_groups_from_array($search_results, $id_menu);

                                foreach ($posts as $post) { ?>
                                    <li class="<?php echo $li_menu_class; ?>">
                                        <a href="<?php echo get_permalink($post->ID); ?>"><?php echo Creditznatok::alt_post_title($post); ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    public static function show_teaser_block($place)
    {
        $post = get_post();

        $blocks  = get_option('blocks');
        $teasers = get_option('teasers');

        /*
        *	Определяем, где находимся и тип блока
        *	Проверяем:
        *		service_types + $check
        *			или
        *		проверка банка
         */

        /* Параметры местоположения */
        if ($post->post_type == 'offers') {
            $d     = 'h';
            $check = 'offers,services';
        } else if ($post->post_type == 'service_types' || $post->post_type == 'services' || $post->post_type == 'seo_groups') {
            $d = 'v';
            if ($post->post_type == 'service_types') {
                $check = '';
            } else if ($post->post_type == 'services') {
                $check = 'services';
            } else if ($post->post_type == 'seo_groups') {
                $check = 'seo_groups';
            }
        } else if ($post->post_type == 'banks' || $post->post_type == 'bank_data' || $post->post_type == 'bank_reviews') {
            $d     = 'v';
            $check = 'banks';
        } else {
            return "";
        }

        /* Поиск блоков, упорядочивание и GeoIP фильтрация тизеров */
        $sel_blocks = array();
        $check      = explode(',', $check);

        if ($post->post_type == 'service_types') {
            $service_types_ID = $post->ID;
        } else {
            $parents          = Creditznatok::get_parent_posts($post, 'service_types');
            $service_types_ID = $parents[0]->ID;
        }

        if ($check[0] != 'banks' && ! $service_types_ID) {
            return "";
        }

        foreach ($blocks as $id => $block) {
            if ($check[0] == 'banks') {        // банки
                if (isset($block['meta']['banks'])) {
                    $sel_blocks[] = $id;
                }
            } else {                        // остальные места


                if ( ! isset($block['meta'][$service_types_ID])) {
                    continue;
                }


                foreach ($check as $c) {
                    if ($c !== '') {
                        $parents = Creditznatok::get_parent_posts($post, $c);
                        if ( ! isset($block['meta'][$service_types_ID][$c][$parents[0]->ID])) {
                            continue;
                        };
                    }
                }

                $sel_blocks[] = $id;
            }
        }

        foreach ($sel_blocks as $id) {
            foreach ($teasers as $teaser) {
                if ($teaser['id'] !== '' && $teaser['block'] == $id) {
                    if ($teaser['order']) {
                        $order = $teaser['order'] * 1000 + mt_rand(0, 999);
                    } else {
                        $order = $id;
                    }

                    if ( ! isset($teaser['geoip'][$_COOKIE['geo']]) || ! $teaser['geoip'][$_COOKIE['geo']]) {
                        continue;
                    }
                    $teasers_sorted[$order] = $teaser;

                }
            }
            krsort($teasers_sorted);

            if (count($teasers_sorted)) {
                echo "<div class='bs-row teasers'>";

                $teasers_sorted = array_slice(array_values($teasers_sorted), 0, 3);
                foreach ($teasers_sorted as $teaser) {
                    if ($d == 'v') {
                        echo "<div class='bs-row'>";
                    }
                    self::show_teaser($teaser, $d);
                    if ($d == 'v') {
                        echo "</div>";
                    }
                }
                echo "</div>";

                return "";
            }
        }

    }

    /**
     *    Адаптивное отображение тизера, в т.ч. и для админки
     */
    public static function show_teaser($teaser, $d = 'h')
    {
        if ($d == 'h') {
            $d = 4;
        } else {
            $d = 12;
        }
        ?>
        <!--<div class="bs-col-xs-<?php echo $d; ?> bs-col-sm-<?php echo $d; ?> bs-col-md-<?php echo $d; ?>  bs-col-lg-<?php echo $d; ?> teaser">-->
        <a href="<?php echo /*get_bloginfo('wpurl').'/r.php?r='.*/
        $teaser['link']; ?>" target="_blank">
            <div class="bs-row teaser-header">
                <?php echo $teaser['header']; ?>
            </div>
            <div class="bs-row">
                <div class="bs-col-xs-6 bs-col-sm-6 bs-col-md-6  bs-col-lg-6">
                    <img src="<?php echo get_bloginfo('wpurl') . $teaser['img']; ?>">
                </div>
                <div class="bs-col-xs-6 bs-col-sm-6 bs-col-md-6  bs-col-lg-6">
                    <div style="margin-left:5px;"><?php echo str_replace("\r\n", "<BR>", $teaser['text']); ?></div>
                </div>
            </div>
            <div class="bs-row">
                <button class="order-online">Онлайн-заявка</button>
            </div>
        </a>
        <!--</div>-->
        <?php

    }

    public static function greensid2_listing()
    {
        ?>
        <script src="<?php echo get_template_directory_uri(); ?>/js/listlink.js"></script>
        <?php
    }

}