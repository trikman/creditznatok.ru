<?php

class CreditznatokSql
{
    public static function getOne($sql, $output = OBJECT)
    {
        global $wpdb;
        $results = $wpdb->get_results($sql, OBJECT);

        if (count($results)) {
            return $results[0];
        }

        return false;
    }

    public static function getArray($sql, $output = ARRAY_A)
    {
        global $wpdb;
        $results = $wpdb->get_results($sql, $output);

        if (count($results)) {
            return $results;
        }

        return false;
    }
}