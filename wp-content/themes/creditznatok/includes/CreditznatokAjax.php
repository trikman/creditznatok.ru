<?php

$creditznatokAjax = new CreditznatokAjax();

class CreditznatokAjax
{
    function __construct()
    {
        add_action('wp_ajax_cp_ajax_create_reviews_post', [$this, 'cp_ajax_create_reviews_post']);

        add_action('wp_ajax_load_cities', [$this, 'ajax_load_cities']);
        add_action('wp_ajax_nopriv_load_cities', [$this, 'ajax_load_cities']);

        add_action('wp_ajax_cz_calculator', [$this, 'ajax_cz_calculator']);
        add_action('wp_ajax_nopriv_cz_calculator', [$this, 'ajax_cz_calculator']);
    }

    function ajax_cz_calculator()
    {
        $sum     = (int)$_REQUEST['sum'];
        $percent = (int)$_REQUEST['percent'];
        $year    = (int)$_REQUEST['year'];

        if ( ! is_numeric($sum) || ! is_numeric($percent) || ! is_numeric($year)) {
            wp_send_json(['error' => 'Только числа']);

            return;
        }

        $result = $sum + ($sum * ($percent / 100)) * $year;
        wp_send_json(['success' => $result . ' ' . CreditznatokConfig::CurrencyName]);

        return;
    }

    function ajax_load_cities()
    {
        $geoip_options           = get_option('geoip_options');
        $geoip_options['cities'] = CreditznatokGeoip::geoip_options_cities_sort($geoip_options['cities']);
        $result = [];

        foreach ($geoip_options['cities'] as $city => $one) {
            $result[] = ['value' => $city, 'data' => $city];
        }

        wp_send_json($result);
    }


    function cp_ajax_create_reviews_post()
    {
        $mapping = [
            'banks'    => 'bank_reviews',
            'services' => 'service_reviews',
            'offers'   => 'offer_reviews'
        ];
        $post_ID = $_REQUEST['post_id'];
        $post    = get_post($post_ID);

        $children = Creditznatok::get_all_connected_posts('service_reviews', $post);
        if ($children) {
            $html = 'Страница отзыва уже создана! <a target="_blank" href="' . admin_url() . "post.php?post={$children[0]->ID}&action=edit" . '">' . $children[0]->post_title . '</a>';
            wp_send_json(['result' => 'success', 'response_html' => $html]);
        }

        if ($mapping[$post->post_type]) {
            $review_post_id = wp_insert_post(array(
                'post_title'     => 'Отзывы о ' . $post->post_title,
                'post_type'      => $mapping[$post->post_type],
                'post_status'    => 'publish',
                'comment_status' => 'open'
            ));

            p2p_create_connection("{$mapping[$post->post_type]}_to_{$post->post_type}", array(
                'from' => $review_post_id,
                'to'   => $post->ID,
            ));

            $review_post = get_post($review_post_id);

            $html = 'Страница отзыва успешно создана. <a target="_blank" href="' . admin_url() . "post.php?post={$review_post->ID}&action=edit" . '">' . $review_post->post_title . '</a>';

            wp_send_json(['result' => 'success', 'response_html' => $html]);
        }

        wp_send_json(['result' => 'false']);
    }
}