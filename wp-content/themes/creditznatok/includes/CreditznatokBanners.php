<?php

$creditznatokBanners = new CreditznatokBanners();

class CreditznatokBanners
{
    const DefaultBanner = 'default.php';
    const ImageSize = 'full';
    const cookieLifeTime = 30 * DAY_IN_SECONDS;

    private $banner = null;
    private $exclude_banners = null;
    private $showed = null;
    private $banner_type = null;
    private $iteration = true;

    public function __construct()
    {
        add_action('init', [$this, 'register_post_types']);

        //пока не показываем на боевых версиях
        //if ( ! defined('CreditznatokConfig::isTestSite') || ! CreditznatokConfig::isTestSite) {
        //    return;
        //}

        add_action('wp_ajax_get_banner', [$this, 'get_banner_ajax']);
        add_action('wp_ajax_nopriv_get_banner', [$this, 'get_banner_ajax']);
        //add_action('wp', [$this, 'test'], 100);
    }

    public function test()
    {
        ?>
        <div class="cz-banner-place" data-type="<?php echo $this->get_banner_type(); ?>"></div>
        <?php
    }

    public function get_banner_ajax()
    {
        $this->handle_input_data();
        $this->handle_banner();
        $this->send_response();
    }

    public function show_banner($banner_type = 'default')
    {
        if ( ! $this->banner) {
            return;
        }

        $banners_folder  = get_template_directory() . '/banners/';
        $banner_template = $banners_folder . self::DefaultBanner;

        if (file_exists($banners_folder . $banner_type . '.php')) {
            $banner_template = $banners_folder . $banner_type . '.php';
        }


        include_once($banner_template);
    }

    public function handle_input_data()
    {
        $banner_type = sanitize_text_field($_REQUEST['banner_type']);

        if ( ! $banner_type) {
            return false;
        }
        $exclude_banners = isset($_REQUEST['exclude_banners']) ? $_REQUEST['exclude_banners'] : [];
        $showed          = isset($_REQUEST['showed']) ? $_REQUEST['showed'] : [];

        $this->banner_type     = $banner_type;
        $this->exclude_banners = $exclude_banners;
        $this->showed          = $showed;
    }

    /**
     * Получает баннер для текущей страницы, обрабатывает число показов баннера для пользователя
     * @return bool
     */
    public function handle_banner()
    {
        $banner_type = $this->banner_type;

        if ( ! $banner_type) {
            return false;
        }
        $exclude_banners = isset($this->exclude_banners[$banner_type]) ? $this->exclude_banners[$banner_type] : [];
        $showed          = isset($this->showed[$banner_type]) ? $this->showed[$banner_type] : 0;

        $banner = null;

        $date  = new DateTime();
        $today = $date->format('Ymd');

        $args = [
            'post_type'      => 'banner',
            'posts_per_page' => 1,
            'post_status'    => 'publish',
            'orderby'        => 'menu_order',
            'meta_query'     => [
                'relation'   => 'AND',
                'sections'   => [
                    'key'     => 'sections',
                    'value'   => $banner_type,
                    'compare' => 'LIKE',
                ],
                'date_start' => [
                    'key'     => 'date_start',
                    'value'   => $today,
                    'compare' => '<='
                ],
                'date_end'   => [
                    'key'     => 'date_end',
                    'value'   => $today,
                    'compare' => '>='
                ]

            ],
        ];

        if ( ! empty($exclude_banners) && count($exclude_banners)) {
            $args['post__not_in'] = $exclude_banners;
        }

        $banners = Creditznatok::get_posts('banner', $args);

        if ( ! empty($banners)) {
            $showed       = $showed + 1;
            $banner       = $banners[0];
            $this->banner = $banner;

            $frequency = get_post_meta($banner->ID, 'frequency', 1);
            if ($frequency <= $showed) {
                // если число показов баннера меньше или равно (поменяли настройки) реальному кол-ву показов баннера для пользователя то исключаем его и сбрасываем число показов
                $exclude_banners[] = $banner->ID;

                $this->exclude_banners[$banner_type] = $exclude_banners;
                $this->showed[$banner_type]          = 0;
            } else {
                // иначе просто увеличиваем число показов данного баннера
                $this->showed[$banner_type] = $showed;
            }

        } else {
            if ( ! $showed && empty($exclude_banners)) {
                //баннеров вообще нет
                return false;
            } else {
                //конец цикла показа баннеров
                $this->exclude_banners[$banner_type] = [];
                $this->showed[$banner_type]          = 0;

                if ($this->iteration) {
                    $this->iteration = false;
                    $this->handle_banner();
                }
            }
        }

    }

    private function send_response()
    {
        $response['exclude_banners'] = $this->exclude_banners;
        $response['showed']          = $this->showed;
        ob_start();
        $this->show_banner($this->banner_type);
        $response['banner'] = ob_get_clean();

        wp_send_json($response);
    }

    /**
     * Сопоставление типа баннера с сохраненными значениями.
     *
     * Возможные значения в ACF:
     * kredity : кредиты
     * cards : карты
     * mikrozaimy : займы
     * vklady : вклады
     * ipoteka : ипотека
     * avtokredity : автокредиты
     * credit-dlya-business : кредиты для бизнеса
     * home : главная
     * banks-ratings : рейтинги банков
     * mfo : каталог МФО
     *
     * @return bool|mixed|string
     */
    public static function get_banner_type()
    {
        $post = get_post();

        $parent_service_type = Creditznatok::get_parent_service_type_for_url();
        if ($parent_service_type) {
            return $parent_service_type;
        }

        if (is_home() || is_front_page()) {
            return 'home';
        }

        if (is_post_type_archive('mfo') || is_page('mfo-search') || $post->post_type == 'mfo') {
            return 'mfo';
        }

        if (is_post_type_archive('banks') || is_page('banks-ratings')
            || in_array($post->post_type, ['banks', 'bank_data', 'bank_reviews'])
        ) {
            return 'banks';
        }

        if ($post->post_type == 'news') {
            return 'news';
        }

        return false;
    }

    public function register_post_types()
    {

        //Тип записи Баннер
        $labels = array(
            'name'          => 'Баннеры',
            'singular_name' => 'Баннер',
            'menu_name'     => 'Баннеры',
            'all_items'     => 'Все баннеры',
            'add_new'       => 'Добавить новый',
            'add_new_item'  => 'Добавить новый баннер',
            'edit'          => 'Редактировать',
            'edit_item'     => 'Редактировать баннер',
            'new_item'      => 'Новый баннер',
        );

        $args = array(
            'labels'              => $labels,
            'menu_icon'           => 'dashicons-format-image',
            'description'         => '',
            'public'              => false,
            'show_ui'             => true,
            'has_archive'         => false,
            'show_in_menu'        => true,
            'exclude_from_search' => true,
            'capability_type'     => 'post',
            'map_meta_cap'        => true,
            'hierarchical'        => false,
            'supports'            => array(
                'title',
                'revisions',
                'thumbnail',
                'page-attributes',
            ),
        );
        register_post_type('banner', $args);
    }

}