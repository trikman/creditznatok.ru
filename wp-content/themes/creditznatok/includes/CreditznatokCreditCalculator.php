<?php

register_widget('CreditznatokCreditCalculator');

class CreditznatokCreditCalculator extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'cz_calculator', // Base ID
            esc_html__('Кредитный калькулятор', 'creditznatok'), // Name
            array('description' => esc_html__('Кредитный калькулятор', 'creditznatok'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    { ?>
        <div class="cz-widget-wrapper widget-wrapper">
            <div id="widget-title-three" class="widget-title-home"><h2>Кредитный калькулятор</h2></div>
            <form method="POST" name="cz-calculator-form">
                <div class="bs-row cz-calculator-row">
                    <div class="bs-col-sm-12">
                        <label for="cz-calculator-sum">
                            Размер кредита (руб.)
                        </label>
                    </div>
                </div>
                <div class="bs-row">
                    <div class="bs-col-sm-12">
                        <input id="cz-calculator-sum" type="text" name="cz-calculator-sum">
                    </div>
                </div>
                <div class="bs-row cz-calculator-row">
                    <div class="bs-col-sm-12">
                        <label for="cz-calculator-percent">
                            Процент
                        </label>
                    </div>
                </div>
                <div class="bs-row">
                    <div class="bs-col-sm-12">
                        <input id="cz-calculator-percent" type="text" name="cz-calculator-percent">
                    </div>
                </div>
                <div class="bs-row cz-calculator-row">
                    <div class="bs-col-sm-12">
                        <label for="cz-calculator-year">
                            Срок кредита (лет)
                        </label>
                    </div>
                </div>
                <div class="bs-row">
                    <div class="bs-col-sm-12">
                        <input id="cz-calculator-year" type="text" name="cz-calculator-year">
                    </div>
                </div>

                <div class="bs-row cz-calculator-submit-row">
                    <div class="bs-col-sm-12">
                        <input value="Рассчитать" type="submit">
                    </div>
                </div>

                <div class="bs-row cz-calculator-submit-row">
                    <div class="bs-col-sm-12">
                        <div class="cz-calculator-result"></div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {
        $title = ! empty($instance['title']) ? $instance['title'] : esc_html__('New title', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Кредитный калькулятор',
                    'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance          = array();
        $instance['title'] = ( ! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

        return $instance;
    }

}