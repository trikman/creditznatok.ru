<?php

class CreditznatokSqlUtility
{
    public static function get_first_mfo_letters()
    {
        $sql = <<<SQL
SELECT DISTINCT UPPER(LEFT(post_title, 1)) AS letter
FROM wp_posts
WHERE post_status = 'publish' AND post_type = 'mfo'
ORDER BY letter;
SQL;

        return CreditznatokSql::getArray($sql);
    }

    public static function get_connected_child($needed_post_type, WP_Post $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.ID
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = '{$needed_post_type}'
			AND (wp_posts.post_status IN ('publish', 'private'))
			AND (
				wp_p2p.p2p_type = '{$post->post_type}_to_{$needed_post_type}'
				AND wp_posts.ID = wp_p2p.p2p_to 
				AND wp_p2p.p2p_from= '$post->ID')
ORDER BY wp_posts.post_date DESC
SQL;

        return CreditznatokSql::getOne($sql);
    }

    public static function get_connected_service_type(WP_Post $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.ID
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = 'service_types'
			AND (wp_posts.post_status IN ('publish', 'private'))
			AND (
				wp_p2p.p2p_type = '{$post->post_type}_to_service_types'
				AND wp_posts.ID = wp_p2p.p2p_to 
				AND wp_p2p.p2p_from= '$post->ID')
ORDER BY wp_posts.post_date DESC
SQL;


        return CreditznatokSql::getOne($sql);
    }

    public static function get_bank_child($needed_post_type, $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.*,
	wp_p2p.*
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = '{$needed_post_type}'
			AND (wp_posts.post_status <> 'trash' AND wp_posts.post_status <> 'auto-draft')
			AND (
				wp_p2p.p2p_type = '{$needed_post_type}_to_banks'
				AND wp_posts.ID = wp_p2p.p2p_from
				AND wp_p2p.p2p_to = '$post->ID')
ORDER BY wp_posts.post_date DESC
SQL;

        return CreditznatokSql::getOne($sql);
    }

    public static function get_parent_bank($needed_post_type, $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.*,
	wp_p2p.*
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = 'banks'
			AND ((wp_posts.post_status = 'publish'))
			AND
			(wp_p2p.p2p_type = '{$needed_post_type}_to_banks'
			 AND wp_posts.ID = wp_p2p.p2p_to
			 AND wp_p2p.p2p_from IN (
				SELECT wp_posts.ID
				FROM wp_posts
				WHERE 1 = 1 AND
							wp_posts.ID = {$post->ID}
							AND wp_posts.post_type = '{$needed_post_type}'
							AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'inactive1' OR
									 wp_posts.post_status = 'private')
				ORDER BY
					wp_posts.post_date DESC))
ORDER BY wp_posts.post_date DESC
SQL;

        return CreditznatokSql::getOne($sql);
    }

    public static function get_parent_service_type($needed_post_type, $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.*,
	wp_p2p.*
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = 'service_types'
			AND (wp_posts.post_status IN ('publish', 'inactive1', 'private'))
			AND
			(wp_p2p.p2p_type = '{$needed_post_type}_to_service_types'
			 AND wp_posts.ID = wp_p2p.p2p_to
			 AND wp_p2p.p2p_from IN (
				SELECT wp_posts.ID
				FROM wp_posts
				WHERE 1 = 1 AND
							wp_posts.ID = {$post->ID}
							AND wp_posts.post_type = '{$needed_post_type}'
							AND (wp_posts.post_status IN ('publish', 'inactive1', 'private'))
				ORDER BY
					wp_posts.post_date DESC))
ORDER BY wp_posts.post_date DESC
SQL;

        return CreditznatokSql::getOne($sql);
    }

    public static function get_connected_from_post($needed_post_type, WP_Post $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.*,
	wp_p2p.*
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = '{$needed_post_type}'
			AND (wp_posts.post_status IN ('publish', 'private'))
			AND
			(wp_p2p.p2p_type = '{$post->post_type}_to_{$needed_post_type}'
			 AND wp_posts.ID = wp_p2p.p2p_to
			 AND wp_p2p.p2p_from IN (
				SELECT wp_posts.ID
				FROM wp_posts
				WHERE 1 = 1 AND
							wp_posts.ID
							IN ({$post->ID})
							AND wp_posts.post_type = '{$post->post_type}'
							AND wp_posts.post_status IN ('publish', 'private')
				ORDER BY
					wp_posts.post_date DESC))
ORDER BY wp_posts.post_date DESC
SQL;

        return CreditznatokSql::getOne($sql);
    }

    public static function get_review_post($needed_post_type, $post)
    {
        //причина чистого SQL - зависимости posts-to-posts подгружаются позже, чем переписываются урлы
        $sql = <<< SQL
SELECT
	wp_posts.*,
	wp_p2p.*
FROM wp_posts
	INNER JOIN wp_p2p
WHERE 1 = 1
			AND wp_posts.post_type = '{$needed_post_type}'
			AND (
				(
					wp_posts.post_status <> 'trash'
					AND wp_posts.post_status <> 'auto-draft'
				)
			)
			AND (
				wp_p2p.p2p_type = '{$needed_post_type}_to_{$post->post_type}'
				AND wp_posts.ID = wp_p2p.p2p_from
				AND wp_p2p.p2p_to
						IN (

							SELECT wp_posts.ID
							FROM wp_posts
							WHERE 1 = 1
										AND wp_posts.ID
												IN ($post->ID)
										AND wp_posts.post_type = '{$post->post_type}'
										AND (
											wp_posts.post_status = 'publish'
											OR wp_posts.post_status = 'inactive1'
											OR wp_posts.post_status = 'future'
											OR wp_posts.post_status = 'draft'
											OR wp_posts.post_status = 'pending'
											OR wp_posts.post_status = 'private'
										)
							ORDER BY wp_posts.post_date DESC
						)
			)
ORDER BY wp_posts.post_date DESC
LIMIT 0, 30
SQL;

        return CreditznatokSql::getOne($sql);
    }

    /**
     * @param array   $needed_post_types
     * @param string  $needed_post_name
     * @param WP_Post $parent_post
     *
     * @return array
     */
    public static function getChildPostBySlag($needed_post_types, $needed_post_name, $parent_post)
    {

        $post_types_str        = '';
        $posts2posts_types_str = '';

        foreach ($needed_post_types as $needed_post_type) {
            $post_types_str .= " '$needed_post_type',";
            $posts2posts_types_str .= " '{$needed_post_type}_to_{$parent_post->post_type}',";
        }

        $post_types_str        = rtrim($post_types_str, ',');
        $posts2posts_types_str = rtrim($posts2posts_types_str, ',');

        $sql = <<<SQL
SELECT
  wp_posts.ID
FROM wp_posts
  INNER JOIN wp_p2p
WHERE 1 = 1
      AND wp_posts.post_type IN ($post_types_str)
      AND (wp_posts.post_status <> 'trash' AND wp_posts.post_status <> 'auto-draft')
      AND (wp_p2p.p2p_type IN ($posts2posts_types_str)
           AND wp_p2p.p2p_from IN (
              SELECT wp_posts.ID
              FROM wp_posts
              WHERE 1 = 1
                    AND wp_posts.post_name = '$needed_post_name'
            )
           AND wp_p2p.p2p_to = '$parent_post->ID'
      )
      AND wp_p2p.p2p_from = wp_posts.ID
ORDER BY wp_posts.post_date DESC
LIMIT 0, 30;
SQL;


        $result = CreditznatokSql::getOne($sql);

        return $result->ID;
    }
}