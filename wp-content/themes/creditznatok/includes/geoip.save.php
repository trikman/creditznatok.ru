<?php
if ( isset( $_GET['city'] ) ) {
	$time = time() + 60 * 60 * 24 * 30;
	$path = '/';
	$domain = $_SERVER['HTTP_HOST'];

    setrawcookie ( "geo", urldecode($_GET['city']), $time, $path, $domain  );

	header( "Location: " . $_GET['r'] );
	exit;
}
