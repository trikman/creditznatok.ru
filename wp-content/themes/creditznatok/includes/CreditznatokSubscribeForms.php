<?php

class CreditznatokSubscribeForms
{
    public static function get_subscribe_form()
    {
        $service_type_id = Creditznatok::get_parent_service_type_id();

        if ( ! isset(CreditznatokConfig::$subscribe_form_mapping[$service_type_id])) {
            return;
        }
        $form_id = CreditznatokConfig::$subscribe_form_mapping[$service_type_id];

        $content = sprintf('[yikes-mailchimp form="%s" title="%s" description="%s"]',
            $form_id,
            $form_id,
            $form_id
        ); ?>
        <div class="cz-block-white">
            <div class="cz-subscribe-wrapper">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
        <?php
    }
}