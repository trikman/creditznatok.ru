<?php

class NewRegisterStructure
{
    public static $structure = array(
        'banks' => 'Банки',
        'bank_data' => 'Реквизиты банков',
        'bank_reviews' => 'Отзывы о банках',
        'service_types' => 'Типы услуг',
        'services' => 'Услуги банков',
        'service_reviews' => 'Отзывы об услугах',
        'seo_groups' => 'SEO группировки',
        'seo_groups_child' => 'SEO группировки - дочерние посты',
        'offers' => 'Продукты',
        'offer_reviews' => 'Отзывы о продуктах',
    );

    function __construct()
    {
        $this->register_structure();
    }

    function register_structure()
    {
        $structure = self::$structure;

        // Регистрация объектов и связей
        foreach ($structure as $name => $label) {
            $this->register_structure_object($name, $label);
        }
    }

    function register_structure_object($name, $label)
    {
        $args = array(
            'labels' => array(
                'name' => $label,
            ),
            'public' => true,
            'show_ui' => true,
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'custom-fields',
                'excerpt',
                'trackbacks',
                'comments',
                'revisions',
                'page-attributes',
                'post-formats'
            )
        );


        register_post_type($name, $args);
    }
}

$creditznatok_functions = new NewRegisterStructure();