<?php

class CreditznatokWeekChangeStatus
{

    function __construct()
    {

        add_action('init', array($this, 'cp_cron_activation'));

        add_action('add_meta_boxes', array($this, 'product_add_custom_box'));

        add_action('save_post', array($this, 'save_schedule_data'));

        add_action('cp_daily_post_status_change', array($this, 'daily_post_status_change'));
    }

    function product_add_custom_box()
    {
        add_meta_box('week_schedule', 'Еженедельное планирование', array($this, 'product_meta_box_callback'), 'offers');
    }

    function product_meta_box_callback()
    {
        // Используем nonce для верификации
        wp_nonce_field(basename(__FILE__), 'product_metabox_nonce');

        global $post;

        $days_of_week = array(
            'monday' => 'Понедельник',
            'tuesday' => 'Вторник',
            'wednesday' => 'Среда',
            'thursday' => 'Четверг',
            'friday' => 'Пятница',
            'saturday' => 'Суббота',
            'sunday' => 'Воскресенье'
        );

        $scheduled_status_change = get_post_meta($post->ID, 'cp_week_scheduler', true);

        $all_post_statuses = array(
            'publish' => 'Опубликовано',
            'private' => 'Личное'
        );
        ?>
        <form id="cp-schedule-form" name="cp-schedule-form" method="post">
            <?php
            foreach ($days_of_week as $eng_day => $rus_day) {
                ?>
                <div class="cp-schedule-wrapper">
                    <div class="cp-schedule-left">
                        <label for="cp_week_scheduler[<?php echo $eng_day; ?>]"><?php echo $rus_day; ?></label>
                    </div>
                    <div class="cp-schedule-right">
                        <select name="cp_week_scheduler[<?php echo $eng_day; ?>]">
                            <option value="">-</option>
                            <?php foreach ($all_post_statuses as $eng_status => $rus_status) { ?>
                                <option <?php selected($scheduled_status_change[$eng_day], $eng_status); ?>
                                    value="<?php echo $eng_status; ?>"><?php echo $rus_status; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>

        </form>
        <style>
            .cp-schedule-wrapper {
                border: 1px solid darkgray;
                width: 300px;
                margin: 5px;
                padding: 5px
            }

            .cp-schedule-left {
                width: 150px;
                float: left;
            }

            .cp-schedule-right {
                float: right;
            }
        </style>
        <?php
    }

    function save_schedule_data($post_id)
    {

        // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
        if (!wp_verify_nonce($_POST['product_metabox_nonce'], basename(__FILE__))) {
            return $post_id;
        }

        // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // проверяем разрешено ли пользователю указывать эти данные
        if ('offers' == $_POST['post_type'] && !current_user_can('edit_page', $post_id)) {
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // Убедимся что поле установлено.
        if (!isset($_POST['cp_week_scheduler'])) {
            return;
        }

        $data = $_POST['cp_week_scheduler'];
        foreach ($data as $key => $value) {
            if (!$value) {
                unset ($data[$key]);
            }
        }

        update_post_meta($post_id, 'cp_week_scheduler', $data);
    }

    function cp_cron_activation()
    {
        //wp_clear_scheduled_hook('cp_daily_post_status_change');

        if (!wp_next_scheduled('cp_daily_post_status_change')) {

            $start_time = mktime(24, 0, 1, date('m'), date('d'), date('Y')); //следующая полночь
            wp_schedule_event($start_time, 'daily', 'cp_daily_post_status_change');
        }
    }

    function daily_post_status_change()
    {
        $day_of_week = strtolower(date('l'));

        $args = array(
            'post_type' => 'offers',
            'meta_query' => array(
                array(
                    'key' => 'cp_week_scheduler',
                    'value' => $day_of_week,
                    'compare' => 'LIKE',
                )
            ),
            'posts_per_page' => -1,
            'post_status' => 'any'
        );

        $posts = Creditznatok::get_posts('offers', $args);

        foreach ($posts as $post) {

            $schedule_data = get_post_meta($post->ID, 'cp_week_scheduler', true);

            $status = $schedule_data[$day_of_week];

            $post->post_status = $status;

            wp_update_post($post);

        }
    }
}

$creditznatok_week_change_status = new CreditznatokWeekChangeStatus();