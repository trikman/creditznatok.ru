jQuery(document).ready(function ($) {

	function isDefined(some) {
		if (typeof some !== 'undefined') {
			return true;
		}

		return false;
	}

	var ajax_url = creditznatok.ajax_url;

	var bannerPlace = $('.cz-banner-place');
	if (bannerPlace.length) {

		var showed = {};
		var czShowed = localStorage.getItem('czShowed');

		if (czShowed && czShowed.length > 0) {
			showed = JSON.parse(czShowed);
		}

		var excludeBanners = {};
		var czExcludeBanners = localStorage.getItem("czExcludeBanners");

		if (czExcludeBanners && czExcludeBanners.length > 0) {
			excludeBanners = JSON.parse(czExcludeBanners);
		}

		var bannerType = bannerPlace.data('type');
		var data = {
			action         : 'get_banner',
			showed         : showed,
			exclude_banners: excludeBanners,
			banner_type    : bannerType
		};

		jQuery.ajax({
			type   : "POST",
			url    : ajax_url,
			data   : data,
			success: function (response) {
				if ($.isEmptyObject(response)) {
					return;
				}
				if (isDefined(response.banner) && response.banner.length) {
					bannerPlace.html(response.banner);
				}

				if (isDefined(response.exclude_banners)) {
					var serialExcludeBanners = JSON.stringify(response.exclude_banners);
					localStorage.setItem("czExcludeBanners", serialExcludeBanners);
				}
				if (isDefined(response.showed)) {
					var serialShowed = JSON.stringify(response.showed);
					localStorage.setItem("czShowed", serialShowed);
				}
			}
		});
	}

	$('.cz-banner a').click(function (event) {
		event.preventDefault();
		var parent = $(this).closest('.cz-banner');
		if (!parent.length) {
			return;
		}

		var outer_url = parent.data('outer_url');
		if (outer_url.length) {
			window.open(outer_url);
		}

		var inner_url = parent.data('inner_url');
		if (inner_url.length) {
			window.location = inner_url;
		}
	});

	$('[name="cz-calculator-form"]').submit(function (event) {
		event.preventDefault();

		var ajax = $('#cz-ajax');

		if (ajax.length) {
			return;
		}

		var result = $('.cz-calculator-result');
		result.text('');

		var sum = $('[name="cz-calculator-sum"]').val().replace(/\s+/g, '');
		var percent = $('[name="cz-calculator-percent"]').val().replace(/\s+/g, '');
		var year = $('[name="cz-calculator-year"]').val().replace(/\s+/g, '');

		if (!sum.length || !percent.length || !year.length) {
			result.text('Заполните все поля!');
			return;
		}

		if (!$.isNumeric(sum) || !$.isNumeric(percent) || !$.isNumeric(year)) {
			result.text('Только цифры!');
			return;
		}

		var ajax_url = creditznatok.ajax_url;
		$('body').append('<input type="hidden" id="cz-ajax">');

		var data = {
			action : 'cz_calculator',
			sum    : sum,
			percent: percent,
			year   : year
		};

		jQuery.ajax({
			type   : "POST",
			url    : ajax_url,
			data   : data,
			success: function (response) {
				if (response.error) {
					result.text(response.error);
				} else {
					result.text(response.success)
				}
				$('#cz-ajax').remove();
			},
			error  : function () {
				result.text('Произошла ошибка, попробуйте позже.');
				$('#cz-ajax').remove();
			}
		});

	});

	$('.selectpicker').selectpicker(
			{
				noneSelectedText: 'Ничего не выбрано'
			}
	);

	$('#sorting-select').change(function () {
		$('.filters').submit();
	});

	$('.filters-form').submit(function () {
		if (typeof yaCounter26138283 !== 'undefined') {
			yaCounter26138283.reachGoal('searchProduct');
			return true;
		}

		if (typeof yaCounter40936899 !== 'undefined') {
			yaCounter40936899.reachGoal('searchProduct');
			return true;
		}

		if (typeof yaCounter42011259 !== 'undefined') {
			yaCounter42011259.reachGoal('searchProduct');
			return true;
		}
	});

	$('.credit_apply_button').click(function () {
		var url = $(this).data("url");
		window.open(url);

		if (typeof yaCounter26138283 !== 'undefined') {
			yaCounter26138283.reachGoal('goToBankButton');
			return true;
		}

		if (typeof yaCounter40936899 !== 'undefined') {
			yaCounter40936899.reachGoal('goToBankButton');
			return true;
		}

		if (typeof yaCounter42011259 !== 'undefined') {
			yaCounter42011259.reachGoal('goToBankButton');
			return true;
		}
	});

	$('.choose-cities').click(function (event) {
		if ($('#cities-loaded').length === 0) {
			load_cities(event);
			return;
		}
	});

	/*$('.dropdown-menu').on('click', function (e) {
	 if ($(this).hasClass('dropdown-menu-form')) {
	 e.stopPropagation();
	 }
	 });*/


	//TODO переделать сохранение города через wp-ajax
	function selCity(city) {
		var template_directory_uri = creditznatok.template_directory_uri;

		document.location = template_directory_uri + "/includes/geoip.save.php?city=" + city + "&r=" + document.location;
	}

	function load_cities() {
		var ajax_url = creditznatok.ajax_url;
		$('body').append('<input type="hidden" id="cities-loaded">');

		var data = {
			action: 'load_cities'
		};

		jQuery.ajax({
			type   : "POST",
			url    : ajax_url,
			data   : data,
			success: function (response) {

				if (response) {
					$('#autocomplete').autocomplete({
						lookup      : response,
						lookupLimit : 20,
						appendTo    : $('.cz-popover-wrapper .popover-suggestions'),
						onSelect    : function (suggestion) {
							console.log(suggestion)
							selCity(suggestion.data);
						},
						formatResult: function (suggestion, currentValue) {
							var currentCity = $('#current-city').data('current-city');
							if (suggestion.data == currentCity) {
								return '<span class="current-suggestion-city">' + suggestion.data + '</span>';
							}

							return '<span>' + suggestion.data + '</span>';
						}
					});
				}

			},
			error  : function () {
			}
		});

	}

	$('#sel-city').change(function (event) {
		event.preventDefault();
		selCity();
	});


	$('body').click(function (e) {

		if ($(e.target).closest('.cities-wrapper').length) {
			return false;
		}

		if ($(e.target).hasClass('cities-wrapper')) {
			return false;
		}

		var popover = $('.cz-popover-wrapper');
		if (popover.hasClass('hidden')) {

		} else {
			popover.addClass('hidden');
		}
	});

	$('#cities-select').click(function () {
		if ($('#cities-loaded').length === 0) {
			load_cities();
		}

		$('#cities-select').removeClass('focusedInput');
		$('.cz-popover-wrapper input').addClass('focusedInput').focus();

		var popover = $('.cz-popover-wrapper');
		if (popover.hasClass('hidden')) {
			popover.removeClass('hidden');
		} else {
			popover.addClass('hidden');
		}
	});

	if ($('.format-numeral').length) {
		$('.format-numeral').keyup(function (a) {
			formatNum(this);
		});

		function formatNum(_this) {
			var string = numeral(parseInt($(_this).val().replace(/ /g, ''))).format('0,0');
			var v = string.replace(/,/g, ' ');
			if (v != '0')
				$(_this).val(v);
		}

		$('.format-numeral').each(function () {
			formatNum($(this));
		});
	}

});