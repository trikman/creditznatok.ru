jQuery(document).ready(function ($) {
	var ajaxUrl = creditznatok.ajaxUrl;
	$('.cp-create-reviews-page').click(function (event) {
		event.preventDefault();
		if ($('#cp-ajax-doing').length) {
			return;
		}
		$('body').append('<input type="hidden" id="cp-ajax-doing">');

		var post_id = $(this).data('post-id');
		var wrapper = $(this).closest('.cp-create-reviews-page-wrap');
		wrapper.after('<span class="cp-spinner"><img src="/wp-admin/images/spinner.gif"></span>');

		var data = {
			action : 'cp_ajax_create_reviews_post',
			post_id: post_id
		};

		jQuery.ajax({
			type   : "POST",
			url    : ajaxUrl,
			data   : data,
			success: function (response) {
				if (response.result == 'success') {
					wrapper.html(response.response_html);
				} else if (response == 'all_done') {
					wrapper.html(response.response_html);
				}
				$('#cp-ajax-doing').remove();
				$('.cp-spinner').remove();
			},
			error  : function () {
				alert('В процессе создания отзыва возникла ошибка');
				$('#cp-ajax-doing').remove();
				$('.cp-spinner').remove();
			}
		});
	});

	currencySet();
	$("[name='extra[currency_new]']").change(function () {
		currencySet();
	});

	function currencySet() {
		var currency = $("[name='extra[currency_new]'] option:selected");
		if (!currency.length) {
			return;
		}

		var currencyText = $(".currency-text");
		if (!currencyText.length) {
			return;
		}

		var currencyValue = currency.val();

		if (!currencyValue.length) {
			currencyText.removeAttr('value');
			return;
		}

		if (currencyValue == 'rur' || currencyValue == 'uah' || currencyValue == 'kzt') {
			currencyText.val(creditznatok.currencyIn);
		} else if (currencyValue == 'usd') {
			currencyText.val('В долларах')
		} else if (currencyValue == 'eur') {
			currencyText.val('В евро')
		}
	}
});
