<?php

class CreditznatokFilterFormUtility
{
    /**
     * @var array
     * Поля и значения формы фильтрации
     */
    public static $form_fields = [

        'creditnye_karty' => [
            'razmer_zayma_from' => [
                'title'  => 'Кредитный лимит',
                'type'   => 'select',
                'values' => [
                    '   50 000 ' . CreditznatokConfig::CurrencyName => 50000,
                    '  100 000 ' . CreditznatokConfig::CurrencyName => 100000,
                    '  200 000 ' . CreditznatokConfig::CurrencyName => 200000,
                    '  300 000 ' . CreditznatokConfig::CurrencyName => 300000,
                    '  500 000 ' . CreditznatokConfig::CurrencyName => 500000,
                    '1 000 000 ' . CreditznatokConfig::CurrencyName => 1000000,
                    '2 000 000 ' . CreditznatokConfig::CurrencyName => 2000000,
                    '3 000 000 ' . CreditznatokConfig::CurrencyName => 3000000,
                ],
            ],
            'proof_of_income'   => [
                'title'  => 'Подтверждение дохода',
                'type'   => 'select',
                'values' => [
                    'Требуется'    => 'Требуется',
                    'Не требуется' => 'Не требуется',
                ],
            ],
            'banks'             => [
                'title' => 'Банки',
                'type'  => 'banks',
            ],
            'city_filter'       => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
            'cashback'          => [
                'title'  => 'Cash Back',
                'type'   => 'checkbox',
                'values' => 'cashback'
            ],
            'free_service'      => [
                'title'  => 'Бесплатное обслуживание',
                'type'   => 'checkbox',
                'values' => 'free_service',
            ],
            'without_income_verification' => [
                'title'  => 'Без подтверждения дохода',
                'type'   => 'checkbox',
                'values' => 'without_income_verification'
            ],
            'currency_new'      => [
                'title'  => 'Валюта',
                'type'   => 'multiselect',
                'values' => [
                    CreditznatokConfig::CurrencyIn => CreditznatokConfig::Currency,
                    'В долларах'                   => 'usd',
                    'В евро'                       => 'eur'
                ],
                'selected' => [
                    CreditznatokConfig::CurrencyIn,
                ],
            ],
        ],

        'kredity' => [
            'razmer_zayma'       => [
                'title' => 'Сумма (' . CreditznatokConfig::CurrencyName . ')',
                'type'  => 'text'
            ],
            'srok'               => [
                'title' => 'Срок (мес.)',
                'type'  => 'text'
            ],
            'proof_of_income'    => [
                'title'  => 'Подтверждение дохода',
                'type'   => 'select',
                'values' => [
                    'Требуется'    => 'Требуется',
                    'Не требуется' => 'Не требуется',
                ],
            ],
            'banks'              => [
                'title' => 'Банки',
                'type'  => 'banks',
            ],
            'city_filter'        => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
            'without_garantors'  => [
                'title'  => 'Без поручителей',
                'type'   => 'checkbox',
                'values' => 'without_garantors'
            ],
            'without_collateral' => [
                'title'  => 'Без залога',
                'type'   => 'checkbox',
                'values' => 'without_collateral'
            ],
            'without_income_verification' => [
                'title'  => 'Без подтверждения дохода',
                'type'   => 'checkbox',
                'values' => 'without_income_verification'
            ],
            'currency_new'       => [
                'title'  => 'Валюта',
                'type'   => 'multiselect',
                'values' => [
                    CreditznatokConfig::CurrencyIn => CreditznatokConfig::Currency,
                    'В долларах'                   => 'usd',
                    'В евро'                       => 'eur'
                ],
                'selected' => [
                    CreditznatokConfig::CurrencyIn,
                ],
            ],
        ],

        'mikrozaimy' => [
            'razmer_zayma' => [
                'title' => 'Сумма (' . CreditznatokConfig::CurrencyName . ')',
                'type'  => 'text',
            ],
            'srok'         => [
                'title' => 'Срок (дней)',
                'type'  => 'text',
            ],
            'way2get'      => [
                'title'  => 'Способ получения',
                'type'   => 'select',
                'values' => [
                    'На карту'           => 'На карту',
                    'На банковский счет' => 'На банковский счет',
                    'Через Contact'      => 'Через Contact',
                    'на Webmoney'        => 'на Webmoney',
                    'На QIWI'            => 'На QIWI',
                    'На Яндекс Деньги'   => 'На Яндекс Деньги',
                    'Лидер'              => 'Лидер',
                    'Золотая Корона'     => 'Золотая Корона',
                    'Юнистрим'           => 'Юнистрим',
                    'На дом'             => 'На дом',
                    'В офисе'            => 'В офисе',
                ],
            ],
            'banks'        => [
                'title' => 'МФО',
                'type'  => 'banks',
            ],
            'city_filter'  => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
        ],

        'ipoteka' => [
            'credit_sum'   => [
                'title' => 'Сумма (' . CreditznatokConfig::CurrencyName . ')',
                'type'  => 'text',
            ],
            'srok'         => [
                'title' => 'Срок (лет)',
                'type'  => 'text',
            ],
            'min_deposit'  => [
                'title'  => 'Минимальный взнос (%)',
                'type'   => 'select',
                'values' => [
                    '0' => 0,
                    '10' => 10,
                    '15' => 15,
                    '20' => 20,
                    '25' => 25,
                    '30' => 30,
                    '40' => 40,
                    '50' => 50,
                ],
            ],
            'banks'        => [
                'title' => 'Банк',
                'type'  => 'banks',
            ],
            'city_filter'  => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
        ],

        'avtokredity' => [
            'razmer_zayma'      => [
                'title' => 'Сумма (' . CreditznatokConfig::CurrencyName . ')',
                'type'  => 'text',
            ],
            'srok'              => [
                'title' => 'Срок (лет)',
                'type'  => 'text',
            ],
            'doc'               => [
                'title'  => 'Документы',
                'type'   => 'select',
                'values' => [
                    'По паспорту'       => 'По паспорту',
                    'По 2м документам'  => 'По 2м документам',
                    'Справка о доходах' => 'Справка о доходах',
                ],
            ],
            'min_deposit'       => [
                'title'  => 'Минимальный взнос (%)',
                'type'   => 'select',
                'values' => [
                    '0' => 0,
                    '10' => 10,
                    '15' => 15,
                    '20' => 20,
                    '25' => 25,
                    '30' => 30,
                    '40' => 40,
                    '50' => 50,
                ],
            ],
            'vehicle_type'      => [
                'title'  => 'Тип ТС',
                'type'   => 'multiselect',
                'values' => [
                    'Легковой транспорт'     => 'Легковой транспорт',
                    'Мототранспорт'          => 'Мототранспорт',
                    'Коммерческий транспорт' => 'Коммерческий транспорт',
                    'Водный транспорт'       => 'Водный транспорт',
                    'Мотоцикл'               => 'Мотоцикл',
                ],
                'selected' => [
                    'Легковой транспорт'
                ],
            ],
            'manufacturer_type' => [
                'title'  => 'Вид ТС',
                'type'   => 'multiselect',
                'values' => [
                    'Иностранный новый'         => 'Иностранный новый',
                    'Иностранный подержанный'   => 'Иностранный подержанный',
                    'Отечественный новый'       => 'Отечественный новый',
                    'Отечественный подержанный' => 'Отечественный подержанный',
                ],
            ],
            'banks'             => [
                'title' => 'Банк',
                'type'  => 'banks',
            ],
            'city_filter'       => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
            'currency_new'      => [
                'title'  => 'Валюта',
                'type'   => 'multiselect',
                'values' => [
                    CreditznatokConfig::CurrencyIn => CreditznatokConfig::Currency,
                    'В долларах'                   => 'usd',
                    'В евро'                       => 'eur',
                ],
                'selected' => [
                    CreditznatokConfig::CurrencyIn,
                ],
            ],
        ],

        'vklady' => [
            'razmer_zayma'             => [
                'title' => 'Сумма вклада',
                'type'  => 'text',
            ],
            'currency_new'             => [
                'title'  => 'Валюта',
                'type'   => 'multiselect',
                'values' => [
                    CreditznatokConfig::CurrencyIn => CreditznatokConfig::Currency,
                    'В долларах'                   => 'usd',
                    'В евро'                       => 'eur'
                ],
                'selected' => [
                    CreditznatokConfig::CurrencyIn,
                ],
            ],
            'srok'                     => [
                'title'  => 'Срок',
                'type'   => 'select',
                'values' => [
                    '1 месяц'   => 30,
                    '3 месяца'  => 60,
                    '6 месяцев' => 180,
                    '9 месяцев' => 270,
                    '1 год'     => 360,
                    '1.5 года'  => 540,
                    '3 года'    => 1080,
                    '4 года'    => 1440,
                    '5 лет'     => 1800,
                ],
            ],
            'capitalization'           => [
                'title'  => 'Капитализация',
                'type'   => 'checkbox',
                'values' => 'capitalization',
            ],
            'preferential_dissolution' => [
                'title'  => 'Льготное расторжение',
                'type'   => 'checkbox',
                'values' => 'preferential_dissolution'
            ],
            'depositing'               => [
                'title'  => 'Пополнение',
                'type'   => 'checkbox',
                'values' => 'depositing',
            ],
            'partial_withdrawal'       => [
                'title'  => 'Частичное снятие',
                'type'   => 'checkbox',
                'values' => 'partial_withdrawal',
            ],
            'banks'                    => [
                'title' => 'Банк',
                'type'  => 'banks',
            ],
            'city_filter'              => [
                'title' => 'Город',
                'type'  => 'cities',
            ],
        ],
    ];

    /**
     * @var array
     * Параметры для сортировки
     */
    public static $sorting_parameters = [
        'creditnye_karty' => [
            'acceptance'   => [
                'title' => '% одобрения',
                'order' => 'DESC'
            ],
            'percents'     => [
                'title' => '% ставке',
                'order' => 'ASC'
            ],
            'srok'         => [
                'title' => 'Льготному периоду',
                'order' => 'DESC'
            ],
            'time'         => [
                'title' => 'Времени рассмотрения',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Кредитному лимиту',
                'order' => 'ASC'
            ],
            /*'crfp-average-rating'=>[
                'title'=>'Рейтингу',
                'order'=>'DESC'
            ],*/
        ],

        'kredity' => [
            'acceptance'   => [
                'title' => '% одобрения',
                'order' => 'DESC'
            ],
            'percents'     => [
                'title' => '% ставке',
                'order' => 'ASC'
            ],
            'time'         => [
                'title' => 'Времени рассмотрения',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Сумме кредита',
                'order' => 'ASC'
            ],
            /*'crfp-average-rating'=>[
                'title'=>'Рейтингу',
                'order'=>'DESC'
            ],*/
        ],

        'mikrozaimy' => [ /// для продукта "микрозаймы"
            'acceptance'   => [
                'title' => '% одобрения',
                'order' => 'DESC'
            ],
            'percents'     => [
                'title' => '% ставке',
                'order' => 'ASC'
            ],
            'time'         => [
                'title' => 'Времени одобрения',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Размеру займа',
                'order' => 'ASC'
            ],
            /*'crfp-average-rating'=>[
                'title'=>'Рейтингу',
                'order'=>'DESC'
            ],*/
        ],

        'vklady' => [
            'percents'     => [
                'title' => '% ставке',
                'order' => 'DESC'
            ],
            'srok'         => [
                'title' => 'Времени',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Размеру',
                'order' => 'ASC'
            ],
            /*'crfp-average-rating'=>[
                'title'=>'Рейтингу',
                'order'=>'DESC'
            ],*/
        ],

        'ipoteka' => [
            'acceptance'   => [
                'title' => '% одобрения',
                'order' => 'DESC'
            ],
            'percents'     => [
                'title' => '% ставке',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Минимальному взносу',
                'order' => 'ASC'
            ],

        ],

        'avtokredity' => [
            'acceptance'   => [
                'title' => '% одобрения',
                'order' => 'DESC'
            ],
            'percents'     => [
                'title' => '% ставке',
                'order' => 'ASC'
            ],
            'razmer_zayma' => [
                'title' => 'Сумме кредита',
                'order' => 'ASC'
            ],
            'srok'         => [
                'title' => 'Сроку кредита',
                'order' => 'ASC'
            ],
        ],
    ];
}