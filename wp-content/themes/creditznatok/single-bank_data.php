<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
$post = get_post();

get_header(); ?>

<?php get_template_part('loop-header'); ?>

<?php if (have_posts()) : ?>

    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white single-banks">

                <?php while (have_posts()) :
                    the_post(); ?>
                    <?php
                    $post->post_parent = CreditznatokSqlUtility::get_parent_bank('bank_data', get_post())->ID;
                    $bank_children     = get_pages('child_of=' . $post->post_parent);
                    foreach ($bank_children as $ch) {
                        if (mb_strpos($ch->post_title, 'Реквизиты', 0, 'UTF-8') === 0) {
                            $rekvizit_id = $ch->ID;
                        }

                        if (mb_strpos($ch->post_title, 'Отзывы', 0, 'UTF-8') === 0) {
                            $otziv_id = $ch->ID;
                        }
                    }
                    ?>
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <?php $image_url = get_the_post_thumbnail_url($post->post_parent, 'full'); ?>
                            <img src="<?php echo $image_url; ?>" class="img-rounded img-responsive product-image" alt="<?php the_title(); ?>">
                        </div>
                        <div class="col-sm-8 col-xs-12">
                            <h1 class="h1-title"><?php echo get_the_title($post->post_parent); ?></h1>


                            <div class="bank-links">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li>
                                        <a href="<?php echo get_permalink($post->post_parent); ?>">О банке</a>
                                    </li>
                                    <li class="active">
                                        <a href="#">Реквизиты</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo Creditznatok::get_bank_reviews_link(get_post($post->post_parent)); ?>">Отзывы</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bank-info-wrapper">
                                <?php foreach (CreditznatokConfig::$banks_data_fields_mapping as $text => $meta_name): ?>
                                    <?php $meta_value = get_post_meta($post->post_parent, $meta_name, 1) ?>
                                    <?php if ($meta_value): ?>
                                        <div class="single-bank-info">
                                            <div class="row v-center">
                                                <div class="col-xs-6 bank-param-name"><?php echo $text; ?></div>
                                                <div class="col-xs-6 bank-param-value"><?php echo $meta_value; ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>
            </div>

            <?php get_template_part('comments-add-new'); ?>

            <?php if ($post->post_content): ?>
                <div class="cz-block-white">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php else : ?>
    <div class="row">
        <div class="col-md-8">
            <div id="content">
                <?php get_template_part('loop-no-posts'); ?>
            </div><!-- end of #content -->
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>