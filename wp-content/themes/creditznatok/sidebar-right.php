<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
?>

<?php CreditznatokWidgets::bank_info(); ?>

<div id="widgets" class="cz-block-white">

    <div class="creditznatok-widgets">
        <?php CreditznatokWidgets::display_all_sidebar_menu(); ?>
    </div>

    <?php //CreditznatokWidgets::show_teaser_block('sidebar'); ?>
</div><!-- end of #widgets -->

<?php CreditznatokSubscribeForms::get_subscribe_form(); ?>

<div class="cz-banner">
    <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
</div>

<?php responsive_widgets_after(); // after widgets container hook ?>

