<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}

get_header();

$post = get_post();
?>

<?php get_template_part('loop-header'); ?>
<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

        <div class="row">
            <div class="col-md-8">
                <div id="content">

                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php responsive_entry_top(); ?>

                        <?php if ($post->post_excerpt) : ?>
                            <div class="cz-block-white cz-list"><?php echo do_shortcode($post->post_excerpt); ?></div>
                        <?php endif; ?>

                        <?php echo CreditznatokShortcodes::product_shortcode(); ?>

                        <?php if ($post->post_content) : ?>
                            <div class="cz-block-white cz-list"><?php echo do_shortcode($post->post_content); ?></div>
                        <?php endif; ?>

                    </div>

                    <?php comments_template('', true); ?>

                </div>
            </div>
            <div class="col-md-4">
                <?php get_sidebar('right'); ?>
            </div>
        </div>
        <?php
    endwhile;

else : ?>
    <div id="content" class="cp-content grid col-700">
        <?php get_template_part('loop-no-posts'); ?>
    </div><!-- end of #content -->
<?php endif; ?>

<?php get_footer(); ?>