<?php

// Exit if accessed directly
if ( ! defined('ABSPATH')) {
    exit;
}
$post = get_post();

get_header(); ?>

<?php get_template_part('loop-header'); ?>


    <div class="row">
        <div class="col-md-8">
            <div id="content" class="cz-block-white single-banks">

                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <?php $image_url = get_the_post_thumbnail_url($post->post_parent, 'full'); ?>
                        <img src="<?php echo $image_url; ?>" class="img-rounded img-responsive product-image" alt="<?php the_title(); ?>">
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <h1 class="h1-title"><?php the_title(); ?></h1>
                        <div class="bank-info-wrapper">
                            <?php
                            $parent_post = get_post($post->post_parent);

                            $address     = get_post_meta($post->ID, 'address', true);
                            $coordinates = get_post_meta($post->ID, 'coordinates', true);
                            $metro       = get_post_meta($post->ID, 'metro', true);

                            $working_hours_weekdays_from = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_weekdays_from', true));
                            $working_hours_weekdays_to   = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_weekdays_to', true));

                            $working_hours_saturday_from = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_saturday_from', true));
                            $working_hours_saturday_to   = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_saturday_to', true));

                            $working_hours_sunday_from = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_weekdays_from', true));
                            $working_hours_sunday_to   = Creditznatok::format_mfo_work_time(get_post_meta($post->ID,
                                'working_hours_weekdays_to', true));
                            ?>

                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    ymaps.ready(init);
                                    var myMap;

                                    function init() {
                                        myMap = new ymaps.Map("map", {
                                            center: [<?php echo $coordinates['lat_long']; ?>],
                                            zoom  : 17
                                        });

                                        var mfoPlacemark = new ymaps.Placemark([<?php echo $coordinates['lat_long']; ?>], {
                                            balloonContentBody: [
                                                '<div>',
                                                '<strong>МФО «<?php echo $parent_post->post_title; ?>»</strong>',
                                                '<br/>',
                                                '<?php echo $post->post_title; ?>',
                                                '<br/>',
                                                'Адрес: <?php echo $address; ?>',
                                                '</div>'
                                            ].join('')
                                        }, {
                                            preset: 'islands#blueMoneyIcon'
                                        });

                                        myMap.geoObjects.add(mfoPlacemark);

                                    <?php if($metro['geocode']) :?>
                                        var metroPlacemark = new ymaps.Placemark([<?php echo $metro['lat_long']; ?>], {
                                            balloonContentBody: [
                                                '<div>',
                                                '<strong><?php echo $metro['geocode']->GeoObject->name; ?></strong>',
                                                '<br/>',
                                                '<?php echo $metro['geocode']->GeoObject->description; ?>',
                                                '</div>'
                                            ].join('')
                                        }, {
                                            preset: 'islands#blueRapidTransitIcon'
                                        });

                                        myMap.geoObjects.add(metroPlacemark);
                                    <?php endif; ?>
                                    }

                                });
                            </script>

                            <?php if ($address): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Адрес:</div>
                                        <div class="col-xs-6 bank-param-value"><?php echo $address; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($metro['geocode']): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Метро:</div>
                                        <div class="col-xs-6 bank-param-value"><?php echo $metro['geocode']->GeoObject->name; ?> - <?php echo $metro['geocode']->GeoObject->description; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($coordinates): ?>
                                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                                <div id="map" style="width: auto; height: 400px"></div>
                            <?php endif; ?>
                            <?php if (($working_hours_weekdays_from && $working_hours_weekdays_to) || ($working_hours_saturday_from && $working_hours_saturday_to) || ($working_hours_sunday_from && $working_hours_sunday_to)): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Время работы:</div>
                                        <div class="col-xs-6 bank-param-value"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($working_hours_weekdays_from && $working_hours_weekdays_to): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Будни:</div>
                                        <div class="col-xs-6 bank-param-value"><?php echo $working_hours_weekdays_from . ' - ' . $working_hours_weekdays_to; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($working_hours_saturday_from && $working_hours_saturday_to): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Суббота:</div>
                                        <div class="col-xs-6 bank-param-value"><?php echo $working_hours_saturday_from . ' - ' . $working_hours_saturday_to; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($working_hours_sunday_from && $working_hours_sunday_to): ?>
                                <div class="single-bank-info">
                                    <div class="row v-center">
                                        <div class="col-xs-6 bank-param-name">Воскресенье:</div>
                                        <div class="col-xs-6 bank-param-value"><?php echo $working_hours_sunday_from . ' - ' . $working_hours_sunday_to; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($post->post_content): ?>
                <div class="cz-block-white">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-4">
            <div class="cz-banner">
                <div class="cz-banner-place" data-type="<?php echo CreditznatokBanners::get_banner_type(); ?>"></div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>